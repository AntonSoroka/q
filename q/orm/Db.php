<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 26.04.2017
 * Time: 13:28
 */

namespace q\orm;



use q\Orm;
use q\orm\db\Table;
use q\orm\db\table\column\Cell;
use q\orm\db\table\column\Extend;
use q\orm\db\table\column\Join;
use q\orm\db\table\column\Primary;
use q\orm\db\table\column\Real;
use q\orm\db\table\column\Returned;
use q\orm\db\table\column\Where;
use q\orm\db\table\type\BigInt;
use q\orm\db\table\type\Blob;
use q\orm\db\table\type\Boolean;
use q\orm\db\table\type\Char;
use q\orm\db\table\type\Date;
use q\orm\db\table\type\DateTime;
use q\orm\db\table\type\Decimal;
use q\orm\db\table\type\Double;
use q\orm\db\table\type\Enum;
use q\orm\db\table\type\Integer;
use q\orm\db\table\type\LongBlob;
use q\orm\db\table\type\LongText;
use q\orm\db\table\type\MediumBlob;
use q\orm\db\table\type\MediumInt;
use q\orm\db\table\type\MediumText;
use q\orm\db\table\type\Set;
use q\orm\db\table\type\SmallInt;
use q\orm\db\table\type\Text;
use q\orm\db\table\type\Time;
use q\orm\db\table\type\TimeStamp;
use q\orm\db\table\type\TinyBlob;
use q\orm\db\table\type\TinyInt;
use q\orm\db\table\type\TinyText;
use q\orm\db\table\type\Varchar;
use q\orm\db\table\type\Year;
use q\orm\db\table\Column;
use q\sql\Connect;

class Db
{
	/**
	 * @var string Имя базы данных
	 */
	protected $stringName;

	/**
	 * @return string Имя базы данных
	 */
	final public function getName():string{
		return $this->stringName;
	}

	/**
	 * @var string Имя базы данных
	 */
	protected $stringUnescapeName;

	/**
	 * @return string Имя базы данных
	 */
	final public function getUnescapeName():string{
		return $this->stringUnescapeName;
	}

	/**
	 * @return string полное имя с названием ORM
	 */
	final public function getInfoName():string{
		return $this->objectOrm->getName() . '.' . $this->getName();
	}

	/**
	 * @var Orm Родительская ORM для базы
	 */
	protected $objectOrm;

	/**
	 * @return Orm Родительская ORM для базы
	 */
	final public function getOrm():Orm{
		return $this->objectOrm;
	}

	/**
	 * @return Connect Подключение к базе
	 */
	final public function getConnect():Connect{
		return $this->objectOrm->getConnect();
	}

	/**
	 * @var array[array]
	 */
	protected $arrayUpdateSchemeTime;

	/**
	 * Массив с данными о последних обновлениях таблицы
	 */
	protected function initLastUpdateTime(){

		// Имя базы
		$stringBaseName = \q\sql\escape($this->getUnescapeName());

		// Находим дату последнего изменения таблиц в базе
		$arrayResult = $this->getConnect()->query(
			'SELECT ' .
				'UNIX_TIMESTAMP(CREATE_TIME) AS `CREATE_TIME`, ' .
				'UNIX_TIMESTAMP(UPDATE_TIME) AS `UPDATE_TIME`, ' .
				'`TABLE_NAME` ' .
			'FROM information_schema.`TABLES` ' .
			"WHERE TABLE_SCHEMA = {$stringBaseName}"
		);

		// Данные по обновлению таблиц
		$arrayUpdateList = $arrayResult[0] ?? [];

		// Заполняем данные
		foreach($arrayUpdateList as $arrayUpdate){
			$this->arrayUpdateSchemeTime[\q\sql\escapeName($arrayUpdate['TABLE_NAME'])] = [
				'CREATE' => (int)$arrayUpdate['CREATE_TIME'],
				'UPDATE' => (int)$arrayUpdate['UPDATE_TIME'],
			];
		}
	}

	/**
	 * Время последнего изменения структуры базы
	 *
	 * @param string|null $stringTableNamePrefix
	 * @return float
	 */
	public function getLastUpdateScheme(string $stringTableNamePrefix = null):float{

		// Максимальное найденое время
		$mixedMaxTime = 0;

		// Перебираем все данные по изменениям таблиц
		foreach($this->arrayUpdateSchemeTime as $stringTableName => $arrayInfo){

			// Если нужно соответствовать префиксу и таблица не начинается с префикса
			if(null !== $stringTableNamePrefix && strpos(\q\sql\unescapeName($stringTableNamePrefix), $stringTableName) !== 0){

				// Пропускаем таблицу
				continue;
			}

			// Выбираем максимальное между прошлым максимальным и текущим элементом
			$mixedMaxTime = max($mixedMaxTime, $arrayInfo['CREATE']);
		}

		// Время не найдено
		if($mixedMaxTime === 0){

			// Возвращаем текущее время
			return time();
		}

		// Возвращаем время последнего изменения в базе
		return $mixedMaxTime;
	}

	/**
	 * Дата последнего обновления таблицы
	 *
	 * @param Table $tableObject
	 * @return int|float
	 */
	public function getLastUpdateTable(Table $tableObject){


		// Имя таблицы
		$stringTableName = $tableObject->getName();

		// Данных по этой таблице нету
		if(!isset($this->arrayUpdateSchemeTime[$stringTableName])){
			throw new \LogicException("Info from table {$stringTableName} not exist");
		}

		// TODO: В винде mysql не сохранять дату обновления данных в таблице...
		if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
			return microtime(true);
		}

		// Возвращаем максимальное значение
		return max(
			$this->arrayUpdateSchemeTime[$stringTableName]['CREATE'],
			$this->arrayUpdateSchemeTime[$stringTableName]['UPDATE']
		);
	}

	/**
	 * Обновляем статистику по таблице
	 *
	 * @param Table $tableObject
	 */
	public function setLastUpdateTime(Table $tableObject){

		// Имя таблицы
		$stringTableName = $tableObject->getName();

		// Данных по этой таблице нету
		if(!isset($this->arrayUpdateSchemeTime[$stringTableName])){
			throw new \LogicException("Info from table {$stringTableName} not exist");
		}

		// Обновляем дату последней записи без запросов
		$this->arrayUpdateSchemeTime[$stringTableName]['UPDATE'] = microtime(true);
	}


	/**
	 * Db constructor.
	 * @param string $stringName Имя базы данных
	 * @param Orm $objectOrm
	 */
	final public function __construct(string $stringName, Orm $objectOrm){

		// Записываем нормализованое имя
		$this->stringName = \q\sql\escapeName($stringName);
		$this->stringUnescapeName = \q\sql\unescapeName($stringName);

		// Записываем ORM
		$this->objectOrm = $objectOrm;

		// Добавляем себя в ORM
		$this->objectOrm->writeDb($this);

		// Даты последних обновлений структуры таблицы
		$this->initLastUpdateTime();
	}


	/**
	 * @var Table[] Таблицы в этой базе
	 */
	protected $arrayTable;

	/**
	 * Создание новой таблицы
	 *
	 * @param string $stringName Имя таблицы
	 * @return Table Новая таблица
	 */
	final public function createTable(string $stringName):Table{
		// Создаём и возвращаем таблицу
		return new Table($stringName, $this);
	}

	/**
	 * Запись таблицы в коллекцию
	 *
	 * @param Table $objectTable Таблица которую нужно добавить в коллекцию
	 */
	final public function writeTable(Table $objectTable){

		// Таблица ссылается не на эту базу
		if($objectTable->getDb() !== $this){
			throw new \LogicException("Table {$objectTable->getName()} Db not is {$this->getInfoName()}");
		}

		// Таблица с таким именем уже существует
		if(isset($this->arrayTable[$objectTable->getName()])){
			throw new \OverflowException("Table {$objectTable->getName()} already exists in {$this->getInfoName()}");
		}

		// Добавляем таблицу в коллекцию
		$this->arrayTable[$objectTable->getName()] = $objectTable;
	}

	/**
	 * Получить таблицу из базы по её имени
	 *
	 * @param string $stringTableName Имя таблицы
	 * @return Table Искомая таблица
	 */
	final public function getTable(string $stringTableName):Table{

		// Экранируем имя таблицы
		$stringTableName = \q\sql\escapeName($stringTableName);

		// Таблица не найдена
		if(!isset($this->arrayTable[$stringTableName])){
			throw new \OutOfBoundsException("Table {$stringTableName} not exists in {$this->getInfoName()}");
		}

		// Возвращаем таблицу
		return $this->arrayTable[$stringTableName];
	}


	/**
	 * @return Table[]
	 */
	final public function getAllTable():array{
		return $this->arrayTable;
	}


	final public function getColumn(string $stringColumnName):Column{

		// Извлекаем имя таблицы и имя ячейки
		list($stringTableName, $stringColumnName) = \array_map(function($stringName){
				return \q\sql\escapeName($stringName);
			},
			explode('.', $stringColumnName, 2)
		);

		// Возвращаем колонку из таблицы
		return $this->getTable($stringTableName)->getColumn($stringColumnName);
	}

	/**
	 * @param string $stringReturnedName
	 * @return Returned
	 */
	final public function getColumnReturned(string $stringReturnedName):Returned{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Returned)){
			throw new \OutOfBoundsException("Returned column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Real
	 */
	final public function getColumnReal(string $stringReturnedName):Real{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Real)){
			throw new \OutOfBoundsException("Returned column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Extend
	 */
	final public function getColumnExtend(string $stringReturnedName):Extend{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Extend)){
			throw new \OutOfBoundsException("Extends column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Primary
	 */
	final public function getColumnPrimary(string $stringReturnedName):Primary{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Primary)){
			throw new \OutOfBoundsException("Primary column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Cell
	 */
	final public function getColumnCell(string $stringReturnedName):Cell{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Cell)){
			throw new \OutOfBoundsException("Cell column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Join
	 */
	final public function getColumnJoin(string $stringReturnedName):Join{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Join)){
			throw new \OutOfBoundsException("Cell column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Where
	 */
	final public function getColumnWhere(string $stringReturnedName):Where{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Where)){
			throw new \OutOfBoundsException("Cell column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * Получить данные по таблицам и полям из текущей базы
	 * @return array
	 */
	final public function getDescribeDb():array{

		// Все таблицы из текущей базы данных
		$arrayTableList = $this->getConnect()->query("SHOW TABLES IN {$this->getName()}; USE {$this->getName()};")[0];

		// Все поля из всех таблиц в текущей базе данных
		$arrayTableListColumnList = $this->getConnect()->query(\q\arr\implode('', $arrayTableList, function($arrayTable){
			$stringTableName = current($arrayTable);
			return "DESCRIBE {$stringTableName};";
		}));

		// Сюда поместим данные о таблицах и полях
		$arrayResult = [];

		// Перебираем данные
		foreach($arrayTableList ?? [] as $intIndex => $arrayTable){
			$arrayResult[current($arrayTable)] = $arrayTableListColumnList[$intIndex];
		}

		// Отдаём данные
		return $arrayResult;
	}

	/**
	 * Автоматическое создание структуры базы из файла
	 *
	 * @param string $stringInitFileName Имя файла в который будет записываться код для генерации структуры таблицы
	 * @param string $stringPrefixTableName Префикс имён таблиц из базы на которые нужно генерировать структуру
	 */
	final public function generateInitializationFile(string $stringInitFileName, string $stringPrefixTableName = ''):void{

		// Файла инициализации базы нету
		if(!file_exists($stringInitFileName)){

			// Устанавливаем дату обновления в 0
			$intLastTablesUpdateTime = 0;
		}

		// Файл инициализации уже существует
		else{

			// Время последнего изменения файла
			$intLastTablesUpdateTime = (int)filemtime($stringInitFileName);
		}

		// База менялась позже последнего изменения файла
		if($this->getLastUpdateScheme() > $intLastTablesUpdateTime){

			// Генерируем код
			$stringCodeGenerateDb = $this->generateStructure('$ormObject', $stringPrefixTableName);

			// Добавляем обёртку для нашей ситуации
			$stringCodeGenerateDb = (
				"<?php return (function(\$ormName){\n" .
					"\$ormObject = \\q\\Orm::getInstance(\$ormName);\n" .
					$stringCodeGenerateDb . "\n" .
				'});'
			);

			// Записываем код в файл
			file_put_contents($stringInitFileName, $stringCodeGenerateDb);
		}

		// Подключаем файл инициирующий базу данных
		$functionCreateOrm = include $stringInitFileName;

		// Вернулась не функция
		if(!is_callable($functionCreateOrm)){

			// Удаляем файл
			unlink($stringInitFileName);

			// Вызываем функцию повторно
			$this->generateInitializationFile($stringInitFileName, $stringPrefixTableName);

			// Завершаем выполнение
			return;
		}

		// Выполняем функцию генерирующую структуру
		$functionCreateOrm($this->getOrm()->getName());
	}

	/**
	 * Генерирует схему всех таблиц из базы
	 *
	 * @param string $stringOrmVariableName Имя переменной в которой будет переданна ОРМ
	 * @param string $stringPrefixTableName Префикс имен таблиц которые нужно включить в вывод
	 * @param bool $boolTextCreateDb Указывает что нужно включить в схему создание базы в ОРМ
	 * @return string Код инициализации базы
	 */
	final public function generateStructure(
		string $stringOrmVariableName = '$ormObject',
		string $stringPrefixTableName = '',
		bool $boolTextCreateDb = false
	):string{

		$stringDbName = \q\sql\escape($this->getUnescapeName());

		// Экранируем префикс
		$stringPrefixTableName = preg_replace('#(_|%)#usi', '\\\\$1', \q\sql\decode($stringPrefixTableName));

		// Все данные по базе
		$arrayResult = $this->getConnect()->query(

			// Ячейки
			'SELECT ' .
				'cell.TABLE_NAME, ' .
				'cell.COLUMN_NAME, ' .
				'cell.COLUMN_DEFAULT, ' .
				'cell.IS_NULLABLE, ' .
				'cell.DATA_TYPE, ' .
				'cell.CHARACTER_MAXIMUM_LENGTH, ' .
				'cell.COLUMN_KEY, ' .
				'cell.EXTRA, ' .
				'cell.COLUMN_TYPE ' .
			'FROM information_schema.`COLUMNS` AS cell ' .
				//'INNER JOIN information_schema.`TABLES` AS tables ON tables.TABLE_NAME = column.TABLE_NAME AND tables.TABLE_NAME' .
			'WHERE ' .
				"cell.TABLE_SCHEMA = {$stringDbName} AND " .
				"cell.TABLE_NAME LIKE '{$stringPrefixTableName}%' " .
			'ORDER BY cell.TABLE_NAME; ' .

			// Ссылки
			'SELECT ' .
				'cell.TABLE_SCHEMA, ' .
				'cell.TABLE_NAME, ' .
				'cell.COLUMN_NAME, ' .
				'cell.REFERENCED_TABLE_SCHEMA, ' .
				'cell.REFERENCED_TABLE_NAME, ' .
				'cell.REFERENCED_COLUMN_NAME ' .
			'FROM information_schema.KEY_COLUMN_USAGE AS cell ' .
			'WHERE ' .
				"cell.TABLE_SCHEMA = {$stringDbName} AND " .
				"cell.TABLE_NAME LIKE '{$stringPrefixTableName}%' AND " .
				"cell.REFERENCED_TABLE_NAME LIKE '{$stringPrefixTableName}%' AND " .
				'cell.REFERENCED_TABLE_SCHEMA IS NOT NULL ' .

			'UNION ' .

			'SELECT ' .
				'cell.REFERENCED_TABLE_SCHEMA AS TABLE_SCHEMA, ' .
				'cell.REFERENCED_TABLE_NAME AS TABLE_NAME, ' .
				'cell.REFERENCED_COLUMN_NAME AS COLUMN_NAME, ' .
				'cell.TABLE_SCHEMA AS REFERENCED_TABLE_SCHEMA, ' .
				'cell.TABLE_NAME AS REFERENCED_TABLE_NAME, ' .
				'cell.COLUMN_NAME AS REFERENCED_COLUMN_NAME ' .
			'FROM information_schema.KEY_COLUMN_USAGE AS cell ' .
			'WHERE ' .
				"cell.TABLE_SCHEMA = {$stringDbName} AND " .
				"cell.TABLE_NAME LIKE '{$stringPrefixTableName}%' AND " .
				"cell.REFERENCED_TABLE_NAME LIKE '{$stringPrefixTableName}%' AND " .
				'cell.REFERENCED_TABLE_SCHEMA IS NOT NULL ' .

			'ORDER BY TABLE_NAME; ' .
		'');

		// Делим результат
		list($arrayCellList, $arrayCellJoinList) = $arrayResult;

		// Таблиц нет
		if(count($arrayCellList) === 0){
			return '';
		}

		// Массив для данных о таблицах
		$arrayTableList = array_fill_keys(array_column($arrayCellList, 'TABLE_NAME'), [
			'cell' => [],
			'join' => [],
			'primary' => [],
		]);

		// Перебираем ячейки
		foreach($arrayCellList ?? [] as $arrayCell){
			$arrayTableList[$arrayCell['TABLE_NAME']]['cell'][$arrayCell['COLUMN_NAME']] = $arrayCell;
			if($arrayCell['COLUMN_KEY'] === 'PRI'){
				$arrayTableList[$arrayCell['TABLE_NAME']]['primary'][$arrayCell['COLUMN_NAME']] = $arrayCell;
			}
		}
		// Перебираем ссылки
		foreach($arrayCellJoinList ?? [] as $arrayCell){
			$arrayTableList[$arrayCell['TABLE_NAME']]['join'][] = $arrayCell;
		}

		// Имя базы для создания одноимённой переменной
		$stringVariableDbName = '$db_' . preg_replace('#[^\w_]+#', '_', $this->getUnescapeName());

		// Массив в который будем складывать строки
		$arrayReturn = [];


		// Если в вывод нужно записать код создания таблицы
		if($boolTextCreateDb){

			// Код для создания схемы базы
			$arrayReturn[] = "{$stringVariableDbName} = {$stringOrmVariableName}->createDb('{$this->getUnescapeName()}');";
		}

		// Иначе нужно взять базу из ОРМ
		else{

			// Код для извлечения базы из ОРМ
			$arrayReturn[] = "{$stringVariableDbName} = {$stringOrmVariableName}->getDb('{$this->getUnescapeName()}');";
		}

		// Перебираем таблицы
		foreach($arrayTableList as $stringTableName => $arrayTableData){

			$stringVariableTableName = '$table_' . preg_replace('#[^\d\w_]+#', '_', $stringTableName);
			
			// Добавляем отступы
			$arrayReturn[] = '';
			$arrayReturn[] = '';
			$arrayReturn[] = "// Table: {$stringTableName}";

			// Код для создания схемы таблицы
			$arrayReturn[] = "{$stringVariableTableName} = {$stringVariableDbName}->createTable('{$stringTableName}');";

			// Перебираем ячейки
			foreach($arrayTableData['cell'] ?? [] as $arrayCell){

				// Метод для создания ячейки
				$stringCreateCellMethod = $arrayCell['COLUMN_KEY'] === 'PRI'
					? 'createColumnPrimary'
					: 'createColumnCell'
				;

				// Является ли число беззнаковым
				$boolUnsigned = preg_match('#unsigned#', $arrayCell['COLUMN_TYPE']);
				$stringUnsigned = $boolUnsigned ? 'true' : 'false';

				// Наборы значений для ENUM и SET
				if(in_array($arrayCell['DATA_TYPE'], ['set', 'enum'])){
					// Код для массива возможных значений
					$stringVariants = preg_replace('#^(enum|set)\((.*)\)$#', '$2', $arrayCell['COLUMN_TYPE']);
					$stringVariants = '[' . $stringVariants . ']';
				}else{
					// Код для пустого массива возможных значений
					$stringVariants = '[]';
				}

				// Подбираем тип для SQL ячеки из наших php классов
				switch($arrayCell['DATA_TYPE']){
					case 'varchar':    $stringClassType = Varchar::class . "({$arrayCell['CHARACTER_MAXIMUM_LENGTH']})"; break;
					case 'text':       $stringClassType = Text::class . '()'; break;
					case 'blob':       $stringClassType = Blob::class . '()'; break;
					case 'tinytext':   $stringClassType = TinyText::class . '()'; break;
					case 'tinyblob':   $stringClassType = TinyBlob::class . '()'; break;
					case 'mediumtext': $stringClassType = MediumText::class . '()'; break;
					case 'mediumblob': $stringClassType = MediumBlob::class . '()'; break;
					case 'longtext':   $stringClassType = LongText::class . '()'; break;
					case 'longblob':   $stringClassType = LongBlob::class . '()'; break;
					case 'int':        $stringClassType = Integer::class . "({$stringUnsigned})"; break;
					case 'smallint':   $stringClassType = SmallInt::class . "({$stringUnsigned})"; break;
					case 'mediumint':  $stringClassType = MediumInt::class . "({$stringUnsigned})"; break;
					case 'bigint':     $stringClassType = BigInt::class . "({$stringUnsigned})"; break;
					case 'set':        $stringClassType = Set::class . "({$stringVariants})"; break;
					case 'enum':       $stringClassType = Enum::class . "({$stringVariants})"; break;
					case 'char':       $stringClassType = Char::class . "({$arrayCell['CHARACTER_MAXIMUM_LENGTH']})"; break;
					case 'date':       $stringClassType = Date::class . '()'; break;
					case 'datetime':   $stringClassType = DateTime::class . '()'; break;
					case 'time':       $stringClassType = Time::class . '()'; break;
					case 'year':       $stringClassType = Year::class . '()'; break;
					case 'timestamp':  $stringClassType = TimeStamp::class . '()'; break;
					case 'decimal':    $stringClassType = Decimal::class . '()'; break;
					case 'double':     $stringClassType = Double::class . '()'; break;
					case 'float':      $stringClassType = \q\orm\db\table\type\Real::class . '()'; break;
					case 'tinyint':
						// Если длинна поля 1 и не может быть отрицательным, то считаемт что это Boolean
						if($arrayCell['COLUMN_TYPE'] === 'tinyint(1) unsigned'){
							$stringClassType = Boolean::class . '()';
						}
						// Иначе TinyInt
						else{
							$stringClassType = TinyInt::class . "({$stringUnsigned})";
						}
						break;


					// Если тип не найден то генерируем исключение
					default: throw new \LogicException("Unknown type {$arrayCell['DATA_TYPE']}");
				}

				// Код для создания ячейки
				$arrayReturn[] = (
					"{$stringVariableTableName}->{$stringCreateCellMethod}(" .
						"'{$arrayCell['COLUMN_NAME']}', " .
						"new {$stringClassType}" .
					')' .
					($arrayCell['IS_NULLABLE'] === 'YES' ? '->acceptNull()' : '') .
					($arrayCell['EXTRA'] === 'auto_increment' ? '->thisAutoIncrement()' : '') .

					';'
				);
			}

			// Если есть связанные с этой таблицей ячейки
			if(is_array($arrayTableData['join']) && count($arrayTableData['join']) > 0){

				$arrayReturn[] = "{$stringVariableTableName}->on('init.join', function(" . Table::class . ' $table, ' . self::class . ' $db){';

				// Перебираем ссылки на другие таблицы
				foreach($arrayTableData['join'] as $arrayCell){

					// Ссылатся на ячейку из другой таблицы
					if($arrayCell['TABLE_SCHEMA'] !== $arrayCell['REFERENCED_TABLE_SCHEMA']){

						$arrayReturn[] = (
							"\t// " .
							"{$arrayCell['TABLE_SCHEMA']}.{$arrayCell['TABLE_NAME']}.{$arrayCell['COLUMN_NAME']}" .
							' -> ' .
							"{$arrayCell['REFERENCED_TABLE_SCHEMA']}.{$arrayCell['REFERENCED_TABLE_NAME']}.{$arrayCell['REFERENCED_COLUMN_NAME']}"
						);

						continue;
					}

					// Таблица на которую ссылаются
					$arrayTableTarget = $arrayTableList[$arrayCell['REFERENCED_TABLE_NAME']];

					// Метод для создания колонки (по умолчанию 1 ко многим)
					$stringMethodName = 'createColumnJoin';

					if(
						// Если есть список ячеек
						isset($arrayTableTarget['cell']) &&

						// Если ячейка является первичным ключом
						($arrayTableTarget['cell'][$arrayCell['REFERENCED_COLUMN_NAME']]['COLUMN_KEY'] === 'PRI') &&

						// И первичная ячейка только одна
						count($arrayTableTarget['primary']) === 1
					){
						// То будем использовать метод для
						$stringMethodName = 'createColumnJoinOne';
					}


					// Код ссылки на ячейку из текущей базы
					$arrayReturn[] = (
						"\t\$table->{$stringMethodName}(" .
							//"'{$arrayCell['REFERENCED_TABLE_NAME']}', " .
							"'{$arrayCell['COLUMN_NAME']}__{$arrayCell['REFERENCED_TABLE_NAME']}__{$arrayCell['REFERENCED_COLUMN_NAME']}', " .
							"'{$arrayCell['COLUMN_NAME']}', " .
							"\$db->getColumnReturned('{$arrayCell['REFERENCED_TABLE_NAME']}.{$arrayCell['REFERENCED_COLUMN_NAME']}')" .
						');'
					);
				}

				$arrayReturn[] = '});';

			}

		}

		// Добавляем одну пустую строку в конце что-бы не слиплось с другими базами
		$arrayReturn[] = "\n\n";

		// Соедмняем строки переводом строки, возвращаем результат
		return \implode("\n", $arrayReturn);
	}
}





















