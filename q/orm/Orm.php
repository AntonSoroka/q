<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 26.04.2017
 * Time: 13:14
 */

namespace q;


use q\orm\Db;
use q\orm\db\Table;
use q\sql\Connect;

/**
 * Class Orm
 * @package q
 */
class Orm
{
	/**
	 * @var Connect подключение к базе данных
	 */
	protected $objectConnect;

	/**
	 * @return Connect Защищённый объект для выполнения запросов
	 */
	final public function getConnect():Connect{
		return $this->objectConnect;
	}


	/**
	 * @var string Имя ORM
	 */
	protected $stringName;

	/**
	 * @return string Имя этой ORM
	 */
	final public function getName():string{
		return $this->stringName;
	}

	/**
	 * @return string Имя этой ORM
	 */
	final public function getNameQuotes():string{
		return '\'' . preg_replace('#\'#usi', '\\\'', $this->stringName) . '\'';
	}

	/**
	 * @var Orm[]
	 */
	static protected $arrayInstance = [];

	/**
	 * Экранирует спецсимволы в имени
	 *
	 * @param string $stringName
	 * @return string
	 */
	protected static function clearName(string $stringName):string{
		return $stringName;
	}

	/**
	 * Orm constructor.
	 * @param Connect $objectConnect подключение к базе
	 * @param string $stringName Имя этой ORM
	 */
	final public function __construct(Connect $objectConnect, string $stringName){

		$stringName = static::clearName($stringName);

		// Проверяем была ли создана Orm с таким же именем прежде
		if(isset(static::$arrayInstance[$stringName])){

			// Такая Orm уже есть, генерируем исключение
			throw new \LogicException("Orm '{$stringName}' already exist");
		}

		// Запоминаем созданную Orm
		static::$arrayInstance[$stringName] = $this;

		$this->objectConnect = $objectConnect;
		$this->stringName = $stringName;

	}

	/**
	 * Возвращаем ранее созданную Orm по её имени
	 * @param Connect $objectConnect
	 * @param string $stringName
	 * @return Orm
	 */
	final public static function getInstance(string $stringName, Connect $objectConnect = null):Orm{

		$stringName = static::clearName($stringName);

		// Проверяем была ли создана Orm с таким именем
		if(!isset(static::$arrayInstance[$stringName])){

			// Такая Orm не существует
			throw new \LogicException("Orm '{$stringName}' not exist");
		}

		// Получаем старую ORM
		$orm = &static::$arrayInstance[$stringName];

		// Подключение к базе не совпадают
		if($objectConnect !== null && $orm->getConnect() !== $objectConnect){

			// Генерируем исключение
			throw new \LogicException("Connect from Orm '{$stringName}' not equal of in Connect");
		}

		// Возвращаем
		return $orm;
	}

	/**
	 * @param string $stringName
	 * @return bool
	 */
	final public static function issetInstance(string $stringName):bool{
		return isset(static::$arrayInstance[static::clearName($stringName)]);
	}


	/**
	 * @var Db[]
	 */
	protected $arrayDb = [];

	/**
	 * Создание новой базы
	 *
	 * @param string $stringName Имя новой базы данных
	 * @return Db Добавленная база
	 */
	final public function createDb(string $stringName):Db{
		// Создаём и возвращаем базу данных
		return new Db($stringName, $this);
	}

	/**
	 * Запись новой базы в коллекцию
	 *
	 * @param Db $objectDb База данных
	 */
	final public function writeDb(Db $objectDb){

		// База ссылается не на эту ORM
		if($objectDb->getOrm() !== $this){
			throw new \LogicException("Db {$objectDb->getName()} orm not is {$this->getName()}");
		}

		// База с таким именем уже существует
		if(isset($this->arrayDb[$objectDb->getName()])){
			throw new \OverflowException("Db {$objectDb->getName()} already exists in {$this->getName()}");
		}

		// Добавляем базу в текущую Orm
		$this->arrayDb[$objectDb->getName()] = $objectDb;
	}

	/**
	 * Возвратить базу данных по её имени
	 *
	 * @param string $stringDbName Имя базы данных
	 * @return Db Искомая база данных
	 */
	final public function getDb(string $stringDbName):Db{

		// Экранируем имя
		$stringDbName = \q\sql\escapeName($stringDbName);

		// База не найдена
		if(!isset($this->arrayDb[$stringDbName])){
			throw new \OutOfBoundsException("Db {$stringDbName} not exists in {$this->getName()}");
		}

		// Возвращаем базу
		return $this->arrayDb[$stringDbName];
	}

	/**
	 * @var bool Указывает что все связи между таблицами уже построены
	 */
	protected $boolInitJoin = false;

	/**
	 * @return bool Узнать были инициированы связи между таблицами или нет
	 */
	final public function alreadyInit():bool{
		return $this->boolInitJoin;
	}

	/**
	 * Инициирует настройку связей между таблицами
	 */
	final public function triggerInitJoin(){

		// Проверяем не инициировались ли ячейки раньше
		if($this->boolInitJoin){
			return;
		}

		// Перебираем все базы
		foreach($this->arrayDb as $dbObject){

			// Пребираем все таблицы
			foreach($dbObject->getAllTable() as $tableObject){

				// Вызываем фукции инициализации связей текущей таблицы
				$tableObject->trigger('init.join', ...[$tableObject, $dbObject, $this]);
			}
		}

		// Указываем что ранее инициировали проверку
		$this->boolInitJoin = true;
	}

	/**
	 * Возвращает таблицу из базы
	 *
	 * @param string $stringTableFullName полное имя таблицы например test_base.test_table
	 * @return Table
	 */
	final public function getTable(string $stringTableFullName):Table{

		// Извлекаем имя базы и имя таблицы
		list($stringDbName, $stringTableName) = \array_map(function($stringName){
			return \q\sql\escapeName($stringName);
		},
			explode('.', $stringTableFullName, 2)
		);

		// Возвращаем колонку из таблицы
		return $this->getDb($stringDbName)->getTable($stringTableName);
	}


}