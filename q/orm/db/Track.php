<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 15.05.2017
 * Time: 12:40
 */

namespace q\orm\db;


use q\orm\db\table\column\Join;
use q\orm\db\table\column\JoinOne;

class Track
{

	/**
	 * @var Join|null Ячейка которой связана этот трэки с родительским
	 */
	protected $joinObject;

	/**
	 * @var Table|null Таблица из которой берутся записи
	 */
	protected $tableObject;

	/**
	 * @var Track|null
	 */
	protected $trackParent;

	/**
	 * @var Track[] Дочерние трэки
	 */
	protected $arrayChildrenTrack =[];

	/**
	 * @var int Индекс текущего трека
	 */
	protected $intIndex;

	/**
	 * @return int
	 */
	public function getIndex():int{
		return $this->intIndex;
	}

	/**
	 * @var int Счётчик треков
	 */
	protected $intIndexTrack;

	/**
	 * Track constructor.
	 * @param Table $tableObject
	 * @param Join|null $joinObject
	 * @param int $intIndex
	 */
	protected function __construct(Table $tableObject = null, Join $joinObject = null, int &$intIndex){
		$this->intIndex = $intIndex;
		$this->intIndexTrack = &$intIndex;
		$this->joinObject = $joinObject;
		$this->tableObject = $tableObject;
	}

	/**
	 * Создаёт корневой трекинг
	 * @return Track
	 */
	public static function createTrack():Track{

		// У корня индекс обязательно равен -1
		$intIndex = -1;

		// Создаём корневой трек и возвращаем его
		return new Track(null, null, $intIndex);
	}

	/**
	 * Добавляет новый трек к уже существующему
	 * @param Table|null $tableObject
	 * @param Join|null $joinObject Если это запрос ничего не возвращает, то приходит null
	 * @return Track
	 */
	public function add(Table $tableObject = null, Join $joinObject = null):Track{

		// Увеличиваем индекс трекинга
		$this->intIndexTrack++;

		// Создаём новый трек
		$trackObject = new Track($tableObject, $joinObject, $this->intIndexTrack);

		// Добавляем трек к списку дочерних треков
		$this->arrayChildrenTrack[] = $trackObject;

		// Записываем себя в родители к созданному треку
		$trackObject->trackParent = $this;

		// Возвращаем новый трек
		return $trackObject;
	}


	/**
	 * Переопределяем функцию что-бы она возвращала пустую строку
	 * @return string
	 */
	public function __toString(){
		return '/*' . $this->intIndex . '*/';
	}


	/**
	 * Собираем двуменрый массив в многомерный
	 *
	 * @param array $arrayResult
	 * @return array
	 */
	public function joinResult(array $arrayResult):array{



		// Перебираем и строим результат для всех дочерних элементов
		foreach($this->arrayChildrenTrack as $trackObject){

			// Пропускаем пустышки
			if(null === $trackObject->joinObject){
				continue;
			}

			// Соединяем дочернюю ветку
			$arrayChildrenResult = $trackObject->joinResult($arrayResult);

			// Имена ячеек
			$stringColumnTarget = $trackObject->joinObject->getTarget()->getNameUnescape();
			$stringColumnLink = $trackObject->joinObject->getLink()->getNameUnescape();
			$stringColumnJoin = $trackObject->joinObject->getNameUnescape();

			// Это связь один к отдному
			if($trackObject->joinObject instanceof JoinOne){

				// Получаем уникальные записи по ID
				$arrayChildrenResult = array_column($arrayChildrenResult, null, $stringColumnTarget);

				// Данные для заполнения связей
				$arrayRowOnceList = &$arrayResult[$this->intIndex] ?? [];

				// Записываем их в результат
				foreach($arrayRowOnceList as &$arrayRowResult){
					$arrayRowResult[$stringColumnJoin] = $arrayChildrenResult[$arrayRowResult[$stringColumnLink]] ?? null;
				}
			}

			// Это связь один ко многим
			else{

				// Массив ключи которого равны ссылающемуся полю, а значения это пустые массивы
				$arrayMapResult = array_fill_keys(array_column($arrayResult[$this->intIndex], $stringColumnLink), []);

				// Данные для добавления в связи
				$arrayRowManyList = &$arrayResult[$this->intIndex] ?? [];

				// Находим записи в которые нужно добавить объекты
				foreach($arrayRowManyList as $intIndex => &$arrayRowResult){

					// Инициируем пустые массивы для записи в них значений в будующем
					$arrayRowResult[$stringColumnJoin] = [];

					// Добавляем индекс текущей записи в список
					$arrayMapResult[$arrayRowResult[$stringColumnLink]][] = $intIndex;
				}

				// Перебираем записи на которые ссылаются
				foreach($arrayChildrenResult as $arrayRowJoin){

					// Ключи к которым нужно добавить запись
					$arrayIndexList = &$arrayMapResult[$arrayRowJoin[$stringColumnTarget]] ?? [];

					// Перебираем ключи результатирующего массива, к значениям которых нужно добавить текущуюю запись
					foreach($arrayIndexList as $intIndex){

						// Добавляем запись в результат
						$arrayResult[$this->intIndex][$intIndex][$stringColumnJoin][] = $arrayRowJoin;
					}
				}
			}
		}

		// Объект для передачи данных между событиями
		$crossCondition = new CrossCondition();

		// Вызываем событие перед перебором строк
		$this->tableObject->trigger('get.before.rows', ...[$crossCondition, &$arrayResult[$this->intIndex], $this->tableObject]);

		// Массив ячеек которые нужны в результатирующем наборе
		$arrayReturned = $this->tableObject->getAllColumnReturned();

		if(!is_array($arrayResult[$this->intIndex])){
			$arrayRowList = [];
		}else{
			$arrayRowList = &$arrayResult[$this->intIndex];
		}

		// Перебираем строки из результата
		foreach($arrayRowList as &$arrayRow){

			// Вызываем событие перед обработкой строки
			$this->tableObject->trigger('get.before.row', ...[$crossCondition, &$arrayRow, $this->tableObject]);

			// Перебираем ячейки и обрабатываем их соглассно типу
			foreach($arrayReturned as $returnedObject){

				// Имя ячейки
				$stringName = $returnedObject->getNameUnescape();

				// Ячейку не запросили в результатирующий набор
				if(!isset($arrayRow[$stringName])){
					continue;
				}

				$arrayRow[$stringName] = $returnedObject
					->getType()
					->convertOut($arrayRow[$stringName])
				;
			}

			// Вызываем событие после обработки строки
			$this->tableObject->trigger('get.after.row', ...[$crossCondition, &$arrayRow, $this->tableObject]);
		}

		// Вызываем событие после обработки всех строк
		$this->tableObject->trigger('get.after.rows', ...[$crossCondition, &$arrayResult[$this->intIndex], $this->tableObject]);

		// Возвращаем результат
		return $arrayResult[$this->intIndex];
	}

	/**
	 * Проверяет вы вызовет ли добавление трека с данной ячейкой петли запросов
	 * @param Join $joinObject
	 * @return bool
	 * @throws \LogicException
	 */
	public function isWhile(Join $joinObject):bool{

		// Это первая ветвь, так что петель быть не может
		if(null === $this->joinObject){
			return false;
		}

		// Это логическая ошибка
		if($this->joinObject->getTarget()->getTable() !== $joinObject->getTable()){
			throw new \LogicException('Bad link. Join target table not equal new join link table.');
		}

		// Таблица котороя добавится в треки
		$tableObject = $joinObject->getTarget()->getTable();

		// Таблица на вершине текущей ветви
		$trackCurrent = $this;

		while(true){

			// Проверяем таблицы на которые ссылаются Join ячейки
			if($trackCurrent->joinObject->getTable() === $tableObject){
				return true;
			}

			// Это последний элемент в списке
			if(null === $trackCurrent->trackParent->joinObject){
				break;
			}

			// Поднимаемся вверх по дереыу
			$trackCurrent = $trackCurrent->trackParent;
		}

		return false;
	}
}







