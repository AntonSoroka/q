<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19.06.2017
 * Time: 11:55
 */

namespace q\orm\db;

/**
 * Класс нужен для хранения произвольных данных которые нужно передать между событиями
 * во время удаления/создания/изменения/запроса
 *
 * @package q\orm\db
 */
class CrossCondition{

	private $_ = [];

	public function __get($name){
		if(!isset($this->_[$name])){
			throw new \LogicException("Property $name not set");
		}
		return $this->_[$name];
	}
	public function __set($name, $value){
		$this->_[$name] = $value;
	}
	public function __unset($name){
		if(isset($this->_[$name])){
			unset($this->_[$name]);
		}
	}
	public function __isset($name):bool{
		return isset($this->_[$name]);
	}
}