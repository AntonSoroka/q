<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 26.04.2017
 * Time: 14:24
 */

namespace q\orm\db;


use q\Event;
use q\Orm;
use q\orm\Db;
use q\orm\db\table\column\Cell;
use q\orm\db\table\column\Extend;
use q\orm\db\table\column\Join;
use q\orm\db\table\column\JoinOne;
use q\orm\db\table\column\Primary;
use q\orm\db\table\column\Real;
use q\orm\db\table\column\Returned;
use q\orm\db\table\column\Where;
use q\orm\db\table\Column;
use q\orm\db\table\query\Delete;
use q\orm\db\table\query\Select;
use q\orm\db\table\Type;
use q\sql\Connect;
use q\sql\connect\Info;

class Table extends Event
{
	/**
	 * @var string Имя таблицы
	 */
	protected $stringName;

	/**
	 * @return string Имя таблицы
	 */
	final public function getName():string{
		return $this->stringName;
	}

	/**
	 * @var string
	 */
	protected $stringFullName;

	/**
	 * @return string Полное имя таблицы с именем базы
	 */
	final public function getFullName():string{
		return $this->stringFullName;
	}

	/**
	 * @return string Информационное имя с названием ORM
	 */
	final public function getInfoName():string{
		return $this->objectDb->getInfoName() . '.' . $this->getName();
	}

	/**
	 * @var Db База данных
	 */
	protected $objectDb;

	/**
	 * @return Db База данных в которой находится эта таблица
	 */
	final public function getDb():Db{
		return $this->objectDb;
	}

	/**
	 * @return Orm в которой находится база
	 */
	final public function getOrm():Orm{
		return $this->getDb()->getOrm();
	}

	/**
	 * @return Connect Подключение к SQL серверу
	 */
	final public function getConnect():Connect{
		return $this->getOrm()->getConnect();
	}

	/**
	 * Table constructor.
	 * @param string $stringName Имя таблицы
	 * @param Db $objectDb Родительская база данных
	 */
	public function __construct(string $stringName, Db $objectDb){

		parent::__construct();

		// Записываем родительскую базу
		$this->objectDb = $objectDb;

		// Проверяем и записываем имя таблицы
		$this->stringName = \q\sql\escapeName($stringName);
		$this->stringFullName = $this->objectDb->getName() . '.' . $this->stringName;

		// Записываем себя в базу
		$this->objectDb->writeTable($this);
	}

	/**
	 * @var Column[] Ячейки этой таблицы
	 */
	private $arrayColumn = [];

	/**
	 * Создать новую ячейку
	 *
	 * @param string $stringName Имя новой ячейки
	 * @param Type $objectType
	 * @return Cell Новая ячейка
	 */
	final public function createColumnCell(string $stringName, Type $objectType):Cell{
		// Создаём и возвращаем ячейку
		return new Cell($stringName, $objectType, $this);
	}

	/**
	 * Создать новую ячейку первичного ключа
	 *
	 * @param string $stringName Имя новой ячейки
	 * @param Type $objectType
	 * @return Primary Новая ячейка
	 */
	final public function createColumnPrimary(string $stringName, Type $objectType):Primary{
		// Создаём и возвращаем ячейку
		return new Primary($stringName, $objectType, $this);
	}

	/**
	 * Создать новую расширенную ячейку
	 *
	 * @param string $stringName Имя новой ячейки
	 * @param Type $objectType
	 * @param callable $functionGetExpression
	 * @return Extend Новая ячейка
	 */
	final public function createColumnExtend(string $stringName, Type $objectType, callable $functionGetExpression):Extend{
		// Создаём и возвращаем ячейку
		return new Extend($stringName, $objectType, $functionGetExpression, $this);
	}

	/**
	 * Создать новую ячейку для проверки условий
	 *
	 * @param string $stringName Имя новой ячейки
	 * @param callable $functionGetExpression
	 * @return Where Новая ячейка
	 */
	final public function createColumnWhere(string $stringName, callable $functionGetExpression):Where{
		// Создаём и возвращаем ячейку
		return new Where($stringName, $functionGetExpression, $this);
	}

	/**
	 * Создать новую ячейку соеденяющую ячейку текущей таблицы с внешней ячейкой
	 *
	 * @param string $stringName Имя новой ячейки
	 * @param string $stringColumnName Имя ячейки из текущей таблицы
	 * @param Returned $objectReturned Ячейка из другой таблицы
	 * @return Join Новая ячейка
	 */
	final public function createColumnJoin(string $stringName, string $stringColumnName, Returned $objectReturned):Join{
		// Создаём и возвращаем ячейку
		return new Join($stringName, $this, $stringColumnName, $objectReturned);
	}

	/**
	 * Создать новую ячейку соеденяющую ячейку текущей таблицы с внешней ячейкой
	 *
	 * @param string $stringName Имя новой ячейки
	 * @param string $stringColumnName Имя ячейки из текущей таблицы
	 * @param Returned $objectReturned Ячейка из другой таблицы
	 * @return Join Новая ячейка
	 */
	final public function createColumnJoinOne(string $stringName, string $stringColumnName, Returned $objectReturned):Join{
		// Создаём и возвращаем ячейку
		return new JoinOne($stringName, $this, $stringColumnName, $objectReturned);
	}



	/**
	 * Записать ячейку в коллекцию этой таблицы
	 *
	 * @param Column $objectCell Ячейка которую нужно записать в коллекцию
	 */
	final public function writeColumn(Column $objectCell){

		// Ячейка ссылается не на ту таблицу
		if($objectCell->getTable() !== $this){
			throw new \LogicException("Cell {$objectCell->getName()} Table not is {$this->getInfoName()}");
		}

		// Ячейка с таким именем уже существует
		if(isset($this->arrayColumn[$objectCell->getName()])){
			throw new \OverflowException("Cell {$objectCell->getName()} already exists in {$this->getInfoName()}");
		}

		// Добавляем ячейку в коллекцию
		$this->arrayColumn[$objectCell->getName()] = $objectCell;
	}

	final public function getColumn(string $stringColumnName):Column{

		// Экранируем имя
		$stringColumnName = \q\sql\escapeName($stringColumnName);

		// Такой ячейки нет в таблице
		if(!isset($this->arrayColumn[$stringColumnName])){
			throw new \OutOfBoundsException("Column {$stringColumnName} not exists in {$this->getInfoName()}");
		}

		// Возвращаем колонку
		return $this->arrayColumn[$stringColumnName];
	}
	
	/**
	 * @param string $stringReturnedName
	 * @return Returned
	 */
	final public function getColumnReturned(string $stringReturnedName):Returned{
		$objectColumn = $this->getColumn($stringReturnedName);
		
		if(!($objectColumn instanceof Returned)){
			throw new \OutOfBoundsException("Returned column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}
		
		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Real
	 */
	final public function getColumnReal(string $stringReturnedName):Real{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Real)){
			throw new \OutOfBoundsException("Returned column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Extend
	 */
	final public function getColumnExtend(string $stringReturnedName):Extend{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Extend)){
			throw new \OutOfBoundsException("Extends column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Primary
	 */
	final public function getColumnPrimary(string $stringReturnedName):Primary{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Primary)){
			throw new \OutOfBoundsException("Primary column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Cell
	 */
	final public function getColumnCell(string $stringReturnedName):Cell{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Cell)){
			throw new \OutOfBoundsException("Cell column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Join
	 */
	final public function getColumnJoin(string $stringReturnedName):Join{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Join)){
			throw new \OutOfBoundsException("Cell column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}

	/**
	 * @param string $stringReturnedName
	 * @return Where
	 */
	final public function getColumnWhere(string $stringReturnedName):Where{
		$objectColumn = $this->getColumn($stringReturnedName);

		if(!($objectColumn instanceof Where)){
			throw new \OutOfBoundsException("Cell column {$stringReturnedName} not exists in {$this->getInfoName()}");
		}

		return $objectColumn;
	}
	
	/**
	 * @return Column[]
	 */
	final public function getAllColumn():array{
		return $this->arrayColumn;
	}

	/**
	 * Отдать колонки определённого класса
	 *
	 * @param string|null $stringColumnClass
	 * @param array $arrayCell
	 * @return Column[]
	 */
	private function getColumnInClass(string $stringColumnClass = null, array &$arrayCell = null):array{

		$arrayColumn = [];

		foreach($this->arrayColumn as $columnObject){
			// Если массив передан
			if($arrayCell !== null){

				// И ячейки нет в массиве
				if(!in_array($columnObject->getNameUnescape(), $arrayCell, false)){

					// То отдавать её не нужно
					continue;
				}
			}

			// Класс указан и колонка не является его экземпляром
			if(
				$stringColumnClass !== null &&
				!($columnObject instanceof $stringColumnClass)
			){

				// Пропускаем эту колонку
				continue;
			}

			// Эту колонку нужно будет отдать
			$arrayColumn[] = $columnObject;
		}

		return $arrayColumn;
	}

	/**
	 * @param array|null $arrayCell
	 * @return Returned[]
	 */
	final public function getAllColumnReturned(array $arrayCell = null):array{
		return $this->getColumnInClass(Returned::class, $arrayCell);
	}

	/**
	 * @param array|null $arrayCell
	 * @return Join[]
	 */
	final public function getAllColumnJoin(array $arrayCell = null):array{
		return $this->getColumnInClass(Join::class, $arrayCell);
	}

	/**
	 * @param array|null $arrayCell
	 * @return Join[]
	 */
	final public function getAllColumnJoinLink(array $arrayCell = null):array{

		$arrayJoin = $this->getColumnInClass(Join::class, $arrayCell);

		foreach($arrayJoin as $intIndex => $joinObject){
			$arrayJoin[$intIndex] = $joinObject->getLink();
		}

		return $arrayJoin;
	}

	/**
	 * @param array|null $arrayCell
	 * @return Join[]
	 */
	final public function getAllColumnJoinTarget(array $arrayCell = null):array{

		$arrayJoin = $this->getColumnInClass(Join::class, $arrayCell);

		foreach($arrayJoin as $intIndex => $joinObject){
			$arrayJoin[$intIndex] = $joinObject->getTarget();
		}

		return $arrayJoin;
	}



	/**
	 * @return Primary[]
	 */
	final public function getAllColumnPrimary():array{
		return $this->getColumnInClass(Primary::class, $arrayCell);
	}

	/**
	 * @return Cell[]
	 */
	final public function getAllColumnCell():array{
		return $this->getColumnInClass(Cell::class, $arrayCell);
	}


	/**
	 * @return Extend[]
	 */
	final public function getAllColumnExtend():array{
		return $this->getColumnInClass(Extend::class, $arrayCell);
	}

	/**
	 * @return Real[]
	 */
	final public function getAllColumnReal():array{
		return $this->getColumnInClass(Real::class, $arrayCell);
	}

	/**
	 * @return Where[]
	 */
	final public function getAllColumnWhere():array{
		return $this->getColumnInClass(Where::class, $arrayCell);
	}


	/**
	 * @param array $arrayOption Параметры запроса LIMIT, OFFSET, ORDER, DEPTH
	 *     'where' string[cell]|Clause[cell]|string[cell][] - Условия
	 *     'hawing' string[cell]|Clause[cell]|string[cell][] - Условия
	 *     'order' string[]   Сортировка результата в формате (cell = ASC, +cell = ASC, -cell = DESC)
	 *     'limit' int        Ограничение количества строк
	 *     'offset' int       Нечало выборки
	 *     'cell' array       Массив колонок которые нужно вернуть
	 *     'depthDefault' int Глубина просмотра связанных таблиц по умолчанию для этого запроса, по умолчанию 0
	 *     'depth' int[cell]  Глубина просмотра в направлении определённой ячейки с внешним ключём
	 *     'cache' float      Время на которое закешируется результат, по умолчанию(0.0) не кешируется
	 *     'found' bool       Посчитать общее количество найденых строк если бы недыло лимита
	 * @return array Результат запроса
	 */
	final public function get(array $arrayOption = []):array{

		// Инициируем и проверяем таблицы
		$this->getOrm()->triggerInitJoin();

		// Переменная для записи данных которые необходимо передать между событиями
		$crossCondition = new CrossCondition();

		// Инициируем событие проверки опций пользователем
		$this->trigger('get.option', ...[$crossCondition, &$arrayOption]);

		// Имя для временной таблицы
		$stringTempTableName = $this->getTempName($this->getFullName());

		// Глубина просмотра
		$arrayDepth = $this->getOptionDepth($arrayOption, $arrayOption['depthDefault'] ?? 0);

		// Указывает что просматривать внутренние связи не нужно
		switch (count($arrayDepth)){
			case 0: $boolZeroDepth = true; break;
			case 1: $boolZeroDepth = current($arrayDepth) === 0; break;
			default: $boolZeroDepth = \max(...array_values($arrayDepth)) === 0;
		}

		// Объект для отслеживания полученных данных
		$track = Track::createTrack();

		// Ячейки которые надо вернуть
		$arrayCell = isset($arrayOption['cell']) && count($arrayOption['cell']) > 0 ? $arrayOption['cell'] : null;

		// Дата последнего обновления любой из таблиц учавствующих в запросе
		$floatLastUpdateData = $this->getDb()->getLastUpdateTable($this);

		// Указывает что нужно посчитать общее количество результатов без лимита
		$boolFound = isset($arrayOption['found']) && $arrayOption['found'];

		// Запрос для вывода ноужного количества записей из таблицы
		if($boolZeroDepth){
			$trackRoot = $track->add($this);
			$stringQuery = (
				'SELECT ' . ($boolFound ? 'SQL_CALC_FOUND_ROWS ' : '') .
					static::implodeColumn($this->getAllColumnReturned($arrayCell)) . ' ' .
				'FROM ' . $this->getFullName() . ' ' .
				$this->getOptionWhere($arrayOption) .
				//$this->getOptionGroupBy() . // Поля для группировки
				$this->getOptionWhere($arrayOption, true) .
				$this->getOptionOrder($arrayOption) .
				$this->getOptionLimit($arrayOption) .

				';' .

				($boolFound ?
					($trackCountRows = $track->add($this)) .
					'SELECT FOUND_ROWS() AS count;'
				: '')
			);
		}

		// Запрос записей с временной таблицей
		else{
			$stringQuery = (

				// Удаляем временную таблицу если она есть
				$track->add() .
				"DROP TEMPORARY TABLE IF EXISTS {$stringTempTableName}; " .

				// Запрашиваем первичные ключи, внешнии ключи и вычисляемые ячейки во временную таблицу
				$track->add() .
				"CREATE TEMPORARY TABLE {$stringTempTableName} (" .
					// Поле с ключом для временной таблицы
					"{$stringTempTableName} INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY" .
				') ENGINE=MEMORY ' .
				'SELECT ' . ($boolFound ? 'SQL_CALC_FOUND_ROWS ' : '') .
				static::implodeColumn(array_merge(
					$this->getAllColumnPrimary(),
					$this->getAllColumnExtend(),
					$this->getAllColumnJoinLink()
				)) .
				' ' .
				"FROM {$this->getFullName()} " .
				$this->getOptionWhere($arrayOption) . // Все условия,
				//$this->getOptionGroupBy() . // Поля для группировки
				$this->getOptionWhere($arrayOption, true) . // Все условия, после группировки
				$this->getOptionOrder($arrayOption) . // сортировку результатов
				$this->getOptionLimit($arrayOption) . // и лимиты - нужно применить именно на этот запрос
				'; ' .

				($boolFound ?
					($trackCountRows = $track->add($this)) .
					'SELECT FOUND_ROWS() AS count;'
				: '') .

				// Запрашиваем все данные из таблицы
				($trackRoot = $track->add($this)) .
				'SELECT ' . static::implodeColumn(
					$this->getAllColumnReturned($arrayCell),
					array_merge(
						$this->getAllColumnJoinLink(),
						$this->getAllColumnExtend(),
						$this->getAllColumnPrimary()
					),
					$stringTempTableName
				) .
				' ' .
				"FROM {$this->getFullName()} " .
					"INNER JOIN {$stringTempTableName} USING ({$this->getJoinReturnedName($this->getAllColumnPrimary())}) " .
				"ORDER BY {$stringTempTableName}.{$stringTempTableName} ASC; " .

				// Все прилинкованые таблицы
				\q\arr\implode('', $this->getAllColumnJoin(),
					function(Join $joinObject) use ($arrayDepth, $stringTempTableName, $trackRoot, $arrayCell, &$floatLastUpdateData){
						return $joinObject->getTarget()->getTable()->getJoin(
							$stringTempTableName,
							$trackRoot,
							$joinObject,
							$arrayDepth[$joinObject->getNameUnescape()],
							$floatLastUpdateData,
							$arrayCell[$joinObject->getNameUnescape()] ?? null
						);
					}
				) .

				// Удаляем временную таблицу
				$track->add() .
				"DROP TEMPORARY TABLE IF EXISTS {$stringTempTableName}; "
			);
		}

		// Кеш не нужен
		if(!isset($arrayOption['cache']) || isset($arrayOption['cache']) <= 0){

			// Запрашиваем данные
			$arrayResult = $this->getConnect()->query($stringQuery);
		}

		// Нужно закешировать
		else{

			// Время кеширования
			$floatCacheTime = $arrayOption['cache'];

			// Удаляем не влияющие на сапрос данные
			unset($arrayOption['cache']);

			// Ключь для сохранения кеша
			$stringKey = \md5(serialize($arrayOption));

			// Запрашиваем данные
			$arrayResult = $this->getConnect()->query($stringQuery, $floatCacheTime, $floatLastUpdateData, $stringKey);
		}

		// Засекаем время
		$floatTimeStartJoin = microtime(true);

		// Строим дерево результатирующего набора
		$arrayJoinResult = $trackRoot->joinResult($arrayResult);

		// Количество записей в запросе если бы небыло ограничений
		$this->intFoundRows = $boolFound && isset($trackCountRows)
			? (int)$arrayResult[$trackCountRows->getIndex()][0]['count']
			: null
		;

		// Время окончания "склеивания" результатов в один древовидный массив
		$floatTimeStopJoin = microtime(true);

		// Информация о запросе
		$this->info = $this->getConnect()->getInfoList()->getInfo();

		// Общее время затраченое на склеивание
		$this->floatJoinTime = (float)round($floatTimeStopJoin - $floatTimeStartJoin, 4);

		// Время потраченое на запрос к базе
		$this->floatQueryTime = (float)round($this->info->getTime(), 4);

		// Инициируем событие проверки опций пользователем
		$this->trigger('get.after.select', ...[$crossCondition, $arrayJoinResult]);

		// Возвращаем дерево результатов
		return $arrayJoinResult;
	}

	protected $intFoundRows = null;
	protected $floatQueryTime = 0;
	protected $floatJoinTime = 0;
	protected $info = null;

	/**
	 * @return null|int Общее количество записей в наборе без лимита (нужно в настройках запроса указат параметр 'found' => true)
	 */
	public function getFoundRows(){
		return $this->intFoundRows;
	}

	/**
	 * @return float Время за которое выполнился последний запрос
	 */
	public function getQueryTime():float{
		return $this->floatQueryTime;
	}

	/**
	 * @return float Время за которое результат склеился в один массив
	 */
	public function getJoinTime():float{
		return $this->floatJoinTime;
	}

	/**
	 * @return Info Информация о запросе
	 */
	public function getInfo():Info{
		return $this->info;
	}

	/**
	 * Генерация цепочки подзапросов для текущей таблицы
	 *
	 * @param string $stringTempTableName Временная таблица с которой нужно связать текущую
	 * @param Track $trackObject Трек отслеживающий дерево запросов
	 * @param Join $joinObject ячейка котороая соединяет 2 таблицы
	 * @param int $intDepth Глубина построения подзапросов в текущей ветке
	 * @param float $floatLastTableUpdate Дата обновления таблицы
	 * @param array $arrayCell Ячейки которые должны быть возвращены
	 * @return string Запросы для получения этой ветки из базы
	 */
	final protected function getJoin(
		string $stringTempTableName,
		Track $trackObject,
		Join $joinObject,
		int $intDepth,
		float &$floatLastTableUpdate,
		array $arrayCell = null
	):string{

		// Если глубина равна нулю, то запрашивать ячейку не нужно
		if($intDepth <= 0){
			return '';
		}

		// Уменьшаем глубину просмотра для дочерних таблиц
		$intDepth--;

		// Не запрашиваем эту таблицу, если в результате получится петля
		if($trackObject->isWhile($joinObject)){
			return '';
		}

		// Глубины просмотра для каждой Join ячейке в этой таблице
		$arrayDepth = $this->getOptionDepth([], $intDepth);

		// Указывает что просматривать внутренние связи не нужно
		$boolZeroDepth = count($arrayDepth) === 0 or \max($arrayDepth) <= 0;

		$floatLastTableUpdate = max($floatLastTableUpdate, $this->getDb()->getLastUpdateTable($this));

		// Забираем записи прямиком из таблицы
		if($boolZeroDepth){
			$stringQuery = (
				($trackRoot = $trackObject->add($this, $joinObject)) .
				'SELECT ' . static::implodeColumn(
					$this->getAllColumnReturned($arrayCell),
					[$joinObject->getTarget()],
					$stringTempTableName
				) . ' ' .
				"FROM {$this->getFullName()} " .
					"INNER JOIN {$stringTempTableName} USING ({$this->getJoinReturnedName([$joinObject->getTarget()])}) " .
				$this->getGroupPrimary() .
				$this->getOptionOrder([]) . '; '
			);
		}

		// Создаём временную таблицу и забираем записи из неё
		else{
			// Имя для новой временной таблицы
			$stringTempTableNameCurrent = $this->getTempName($this->getFullName());

			// Запрос
			$stringQuery = (
				// Удаляем временную таблицу если она есть
				$trackObject->add() .
				"DROP TEMPORARY TABLE IF EXISTS {$stringTempTableNameCurrent}; " .

				// Запрашиваем нужные ячейки во временную таблицу
				$trackObject->add() .
				"CREATE TEMPORARY TABLE {$stringTempTableNameCurrent} (" .
					// Поле с ключом для временной таблицы
					"{$stringTempTableNameCurrent} INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY" .
				') ENGINE=MEMORY ' .
				'SELECT ' . static::implodeColumn(
					array_merge(
						$this->getAllColumnPrimary(),
						$this->getAllColumnExtend(),
						$this->getAllColumnJoinLink()
					),
					[$joinObject->getTarget()],
					$stringTempTableName
				) .
				' ' .
				"FROM {$this->getFullName()} " .
					"INNER JOIN {$stringTempTableName} USING ({$this->getJoinReturnedName([$joinObject->getTarget()])}) " .
				$this->getGroupPrimary() .
				$this->getOptionOrder([]) .
				'; ' .

				($trackRoot = $trackObject->add($this, $joinObject)) .
				'SELECT ' . static::implodeColumn(
					$this->getAllColumnReturned($arrayCell),
					array_merge(
						$this->getAllColumnPrimary(),
						$this->getAllColumnExtend(),
						$this->getAllColumnJoinLink(),
						[$joinObject->getTarget()]
					),
					$stringTempTableNameCurrent
				) . ' ' .
				"FROM {$this->getFullName()} " .
					"INNER JOIN {$stringTempTableNameCurrent} USING ({$this->getJoinReturnedName($this->getAllColumnPrimary())})" .
				"ORDER BY {$stringTempTableNameCurrent}.{$stringTempTableNameCurrent} ASC; " .

				// Все прилинкованые таблицы
				\q\arr\implode('', $this->getAllColumnJoin(),
					function(Join $joinObject) use ($intDepth, $stringTempTableNameCurrent, $trackRoot, $arrayCell, &$floatLastTableUpdate){
						return $joinObject->getTarget()->getTable()->getJoin(
							$stringTempTableNameCurrent,
							$trackRoot,
							$joinObject,
							$intDepth,
							$floatLastTableUpdate,
							$arrayCell[$joinObject->getNameUnescape()] ?? null
						);
					}
				) .

				// Удаляем временную таблицу
				$trackObject->add() .
				"DROP TEMPORARY TABLE IF EXISTS {$stringTempTableNameCurrent}; "
			);
		}

		return $stringQuery;
	}

	/**
	 * @var array имена временных таблиц использованных в течении сессиии
	 */
	protected static $arrayTempTable = [];

	/**
	 * Генерирует уникальное имя в рамках выполнения этого скрипта
	 * @param array ...$arrayArguments Аргументы с которыми генерируется запрос
	 * @return string Уникальное имя
	 */
	public function getTempName(...$arrayArguments):string
	{

		// Добавляем полное название текущей таблицы к аргументам
		$arrayArguments[] = $this->getInfoName();

		// Создаём уникальный ключ для полученых параметров
		$stringKey = serialize($arrayArguments);
		$stringKey = md5($stringKey);

		// Первый вызов
		if(!isset(static::$arrayTempTable[$stringKey])){
			static::$arrayTempTable[$stringKey] = [0, $stringKey];
		}

		// Последующие вызовы
		else{
			static::$arrayTempTable[$stringKey] = [
				static::$arrayTempTable[$stringKey][0] + 1,
				\md5(static::$arrayTempTable[$stringKey][0] + static::$arrayTempTable[$stringKey][0])
			];

			$stringKey = static::$arrayTempTable[$stringKey][1];
		}

		// Возвращаем ключ
		return \q\sql\escapeName($stringKey);
	}

	/**
	 * Глубина просмотра связанных таблиц
	 *
	 * @param array $arrayOption
	 * @param int $intDepth
	 * @return int[]
	 */
	protected function getOptionDepth(array $arrayOption, int $intDepth):array{

		// В этот массив будем записывать проверенные данные
		$arrayDepthIn = [];

		// Параметры глубины просмотра
		$arrayDepth = $arrayOption['depth'] ?? [];

		// Проверяем и валидируем пришедшие данные
		foreach ($arrayDepth as $stringCellName => $intDepthCurrent) {

			// Получаем ячейку
			$objectCell = $this->getColumnJoin($stringCellName);

			// Записываем глубину просмотра для этой ветки
			$arrayDepthIn[$objectCell->getNameUnescape()] = (int)$intDepthCurrent;
		}

		// Запишем данные по всем столбцам в которых есть ссылки
		$arrayDepthDefault = [];

		// Найдём все ячейки
		foreach ($this->getAllColumnJoin() as $objectCell) {

			// Записываем глубину по умолчанию
			$arrayDepthDefault[$objectCell->getNameUnescape()] = $intDepth;
		}

		// Совместим 2 массива, так что-бы значение по умолчанию осталось только там где нету специального значения
		return array_merge($arrayDepthDefault, $arrayDepthIn);
	}

	/**
	 * @param Returned[] $arrayReturned Поля которые нужно забрать из базы
	 * @param Returned[] $arrayReturnedTemp Поля которые нужно мернуть из временной таблицы
	 * @param string|null $stringTempTableName Имя временной таблицы
	 * @return string
	 */
	final protected static function implodeColumn(array $arrayReturned, array $arrayReturnedTemp = [], string $stringTempTableName = null):string{

		// Ячейки которые нужно запросить

		/** @var String[] $arraySelectCell */
		$arraySelectCell = [];

		// Добавляем все Returned поля
		foreach($arrayReturned as $returnedObject){
			$arraySelectCell[$returnedObject->getFullName()] = $returnedObject->getAs();
		}

		// Если есть временная таблица, то берём часть полей из неё
		if($stringTempTableName !== null){
			foreach($arrayReturnedTemp as $returnedObject){
				// Эти поля не
				if(!isset($arraySelectCell[$returnedObject->getFullName()])){
					continue;
				}

				$arraySelectCell[$returnedObject->getFullName()] = $returnedObject->getAs($stringTempTableName);
			}
		}

		// Соединяем ячейки, возвращаем результат
		return \implode(', ', $arraySelectCell);
	}

	/**
	 * (WHERE|HAVING) Соединение нескольких условий в часть SQL запроса нескольких условий
	 *
	 * @param array $arrayOption Условия
	 * @param bool $boolIsHaving
	 * @return string Часть запроса с переданными условиями
	 */
	final protected function getOptionWhere(array $arrayOption, bool $boolIsHaving = false):string{

		// Массив условий
		$arrayWhereString = [];

		$stringKey = $boolIsHaving ? 'having' : 'where';

		// Перебираем условия
		foreach($arrayOption[$stringKey] ?? [] as $columnName => $mixedWhere){
			$arrayWhereString[] = $this->getColumnReal($columnName)->getWhere($mixedWhere);
		}

		if(count($arrayWhereString) === 0){
			return '';
		}

		return strtoupper($stringKey) . ' ' . \implode(' AND ', $arrayWhereString) . ' ';
	}

	/**
	 * (ORDER BY) Соединение пользовательских сортировок и сортировок по умолчанию в часть SQL запроса
	 *
	 * @param $arrayOption
	 * @return string
	 * @throws \OverflowException
	 */
	final protected function getOptionOrder($arrayOption):string{

		// Массив колонок по которым нужно отсортировать результат
		$arrayOrderString = [];

		foreach($arrayOption['order'] ?? [] as $stringColumnName){

			// Направление сортировки
			$boolSortDirection = !(($stringColumnName[0] ?? '') === '-');

			// Убераем знаки '+' и '-' указывающие на направление сортировки
			$stringColumnName = preg_replace('#^[+-]#', '', $stringColumnName);

			// Получаем ячейку
			$returnedObject = $this->getColumnReturned($stringColumnName);

			// Возможная ошибка нужно выдать исключение
			if(isset($arrayOrderString[$returnedObject->getFullName()])){
				throw new \OverflowException("Column {$returnedObject->getFullName()} repeat order");
			}

			// Записываем колонку
			$arrayOrderString[$returnedObject->getFullName()] =
				$returnedObject->getExpression() . ' ' . ($boolSortDirection ? 'ASC' : 'DESC')
			;
		}

		// Нужно добавить Primary колонки чтобы сортировка всегда была однозначной
		foreach($this->arrayColumn as $columnObject){

			// Пропускаем не Primary колонки
			if(!($columnObject instanceof Primary)){
				continue;
			}

			// Эта по этой ячейки уже сортируют результат
			if(isset($arrayOrderString[$columnObject->getFullName()])){
				continue;
			}

			// Добавляем сортировку в конец списка
			$arrayOrderString[$columnObject->getFullName()] = $columnObject->getExpression() . ' ASC';
		}

		if(count($arrayOrderString) === 0){
			return '';
		}

		return 'ORDER BY ' . \implode(', ', $arrayOrderString) . ' ';

	}/** @noinspection ReturnTypeCanBeDeclaredInspection */

	/**
	 * (LIMIT) Построение части запроса для ограничения количество строк в результате
	 *
	 * @param array $arrayOption
	 * @return string
	 * @throws \RangeException
	 */
	final protected function getOptionLimit(array $arrayOption = []):string {

		// Лимит указан
		if(isset($arrayOption['limit'])){
			// Не является числом или меньше единицы
			if(!is_numeric($arrayOption['limit']) || $arrayOption['limit'] < 1){
				throw new \RangeException('Limit expected number greater than or equal to one');
			}
		}

		// Начало выборки указано
		if(isset($arrayOption['offset'])){
			// Не является числом или меньше нуля
			if(!is_numeric($arrayOption['offset']) || $arrayOption['offset'] < 0){
				throw new \RangeException('Limit expected number greater than or equal to one');
			}
		}

		if(isset($arrayOption['limit'], $arrayOption['offset'])){
			return 'LIMIT ' . $arrayOption['offset'] . ', ' . $arrayOption['limit'] . ' ';
		}

		if(isset($arrayOption['limit'])){
			return 'LIMIT ' . $arrayOption['limit'] . ' ';
		}

		if(isset($arrayOption['offset'])){
			return 'LIMIT ' . $arrayOption['offset'] . ', 18446744073709551615 ';
 		}

 		return '';

	}

	/**
	 * Формирует колонки для группировки по ключевым полям таблицы
	 * @return string
	 */
	final protected function getOptionGroupBy():string {

		$arrayGroupByString = [];

		// Нужно добавить Primary колонки чтобы сортировка всегда была однозначной
		foreach($this->getAllColumnPrimary() as $columnObject){

			// Добавляем сортировку в конец списка
			$arrayGroupByString[$columnObject->getFullName()] = $columnObject->getExpression();
		}

		if(count($arrayGroupByString) === 0){
			return '';
		}

		return 'GROUP BY ' . \implode(', ', $arrayGroupByString) . ' ';
	}

	/**
	 * Соединить имена переданных ячеек
	 * @param Returned[] $arrayReturned
	 * @return string
	 */
	final protected function getJoinReturnedName(array $arrayReturned):string{

		$arrayCell = [];
		foreach($arrayReturned as $returnedObject){
			$stringName = $returnedObject->getName();
			$arrayCell[$stringName] = $stringName;
		}

		return \implode(', ', $arrayCell);
	}

	/**
	 * Генерирует часть запроса для группировки результата по первичным ключам
	 * @return string
	 */
	final protected function  getGroupPrimary():string{
		$arrayCell = [];
		foreach($this->getAllColumnPrimary() as $primaryObject){
			$arrayCell[] = $primaryObject->getFullName() . ' ASC';
		}

		if(count($arrayCell) === 0){
			return '';
		}

		return 'GROUP BY ' . \implode(', ', $arrayCell) . ' ';
	}


	/**
	 * Обновление или создание записи в базе данных
	 *
	 * @param array $arrayData Новые данне для строки
	 * @return array новая/обновлённая запись
	 */
	final public function set(array $arrayData):array{

		// Инициируем и проверяем таблицы
		$this->getOrm()->triggerInitJoin();

		// Данные из ячеек с первичными ключами
		$arrayPrimaryData = [];

		// Указывает количество неуказанных ячеек с первичным ключём
		$intCountEmptyPrimaryCell = 0;

		// Указывает что есть поле с автоинкриментом
		$primaryAutoIncrement = false;

		// Перебираем первичные ключи
		foreach($this->getAllColumnPrimary() as $primaryObject){

			// Проверяем не помечено ли поле как autoincrement
			if($primaryObject->isAutoIncrement()){
				$primaryAutoIncrement = $primaryObject;
			}

			// Ячейка есть в списке
			if(isset($arrayData[$primaryObject->getNameUnescape()])){

				// Запоминаем её
				$arrayPrimaryData[$primaryObject->getNameUnescape()] = $primaryObject
					->getType()->convertInString($arrayData[$primaryObject->getNameUnescape()])
				;
			}

			// Ячейки нету
			else{
				$intCountEmptyPrimaryCell++;
			}
		}

		// Данные ячейки которые сейчас в базе, по умолчанию равно null (т.е. их нет в базе)
		$arrayDataOld = null;

		// Если пустых ячеек нет, то возможно необходимо обновить ячейку
		if($intCountEmptyPrimaryCell === 0 && count($arrayPrimaryData) > 0){
			$arrayTempDataOld = $this->get([
				'where' => $arrayPrimaryData,
				'limit' => 1
			]);

			// Данные нашлись в базе
			if(count($arrayTempDataOld) > 0){
				$arrayDataOld = $arrayTempDataOld[0];
			}

			unset($arrayTempDataOld);
		}

		// Переменная для записи данных для передачи между событиями
		$crossCondition = new CrossCondition();


		// Событие перед вставкой/обновлением
		$this->trigger('set.before', ...[$crossCondition, &$arrayData, $arrayDataOld, $this]);

		// Нужно вставить новую строку
		if($arrayDataOld === null){
			// Событие перед вставкой
			$this->trigger('set.before.insert', ...[&$arrayData, $this]);

			// Нормализованные данные
			$arrayDataNormalize = $this->normalizeRow($arrayData);

			// Вставляем новую запись
			$arrayResult = $this->getConnect()->query(
				'INSERT INTO ' . $this->getFullName() . ' (' .
					implode(', ', array_keys($arrayDataNormalize)) .
				') VALUES (' .
					implode(', ', $arrayDataNormalize) .
				')'
			);

			// Обновляем статистику по таблице
			$this->getDb()->setLastUpdateTime($this);

			// Есть первичный ключ с автоинкриментом, запрашиваем по нему
			if($primaryAutoIncrement){
				$arrayTempNewData = $this->get([
					'where' => [$primaryAutoIncrement->getNameUnescape() => $arrayResult[0]],
					'limit' => 1,
				]);
			}

			// Нет ключа с автоинкриментом запрашиваем по пришедшим первичным ключам
			else{

				// Нет ключевых полей или есть но не все указаны
				if($intCountEmptyPrimaryCell === 0 || count($arrayPrimaryData) === 0){
					$arrayTempNewData = $this->get(['where' => $arrayData, 'limit' => 1]);
				}

				// Есть все ключевые поля
				else{
					$arrayTempNewData = $this->get(['where' => $arrayPrimaryData, 'limit' => 1]);
				}
			}


			// Новая запись по какой-то причине не найдена
			if(count($arrayTempNewData) === 0){
				throw new \LogicException("New row not insert in {$this->getFullName()}");
			}

			// Новая запись
			$arrayNewData = $arrayTempNewData[0];

			unset($arrayTempNewData);

			// Событие после вставки
			$this->trigger('set.after.insert', ...[$arrayData, $arrayNewData, $this]);
		}

		// Нужно обновить уже существующую строку
		else{
			// Событие перед обновлением
			$this->trigger('set.before.update', ...[$crossCondition, &$arrayData, $arrayDataOld, $this]);

			// Нормализованные данные
			$arrayDataNormalize = $this->normalizeRow($arrayData);

			// Вставляем новую запись
			$this->getConnect()->query(
				"UPDATE {$this->getFullName()} " .
				'SET ' .
				\q\arr\implode(', ', count($arrayDataNormalize) === 0 ? $arrayPrimaryData : $arrayDataNormalize, function($mixedValue, $mixedCell){
					return $mixedCell . '=' . $mixedValue;
				}) . ' ' .
				'WHERE ' .
				\q\arr\implode(' AND ', $arrayPrimaryData, function($mixedValue, $mixedCell){
					return $mixedCell . '=' . $mixedValue;
				})
			);

			// Обновляем статистику по таблице
			$this->getDb()->setLastUpdateTime($this);

			// Запрашиваем обновлённую запись по пришедшим первичным ключам
			$arrayTempNewData = $this->get(['where' => $arrayPrimaryData]);

			// Обновлённая запись по какой-то причине не найдена
			if(count($arrayTempNewData) === 0){
				throw new \LogicException("Row not update in {$this->getFullName()}");
			}

			// Обновлённая запись
			$arrayNewData = $arrayTempNewData[0];

			// Событие перед обновления
			$this->trigger('set.after.update', ...[$crossCondition, $arrayData, $arrayDataOld, $arrayNewData, $this]);
		}

		// Событие после вставки/обновления
		$this->trigger('set.after', ...[$crossCondition, $arrayData, $arrayDataOld, $arrayNewData, $this]);

		// Возвращаем новую запись
		return $arrayNewData;
	}

	/**
	 * Удаление записей из базы данных
	 *
	 * @param $arrayOption array[] Опции запроса строк из базы
	 * @throws \RuntimeException
	 */
	final public function remove(array $arrayOption){

		// Объякт для хранения данных и передачи их между событиями
		$crossCondition = new CrossCondition();

		// Вызываем событие перед началом удаления для возможной подмены условий
		$this->trigger('remove.option', ...[$crossCondition, &$arrayOption, $this]);

		// Нельзя брать закешированный запрос из базы в случае удаления
		$arrayOption['cache'] = 0.0;

		// Строки из базы
		$arrayRowList = $this->get($arrayOption);


		// Вызываем событие перед началом удаления для обработки строк
		$this->trigger('remove.before', ...[$crossCondition, &$arrayRowList, $this]);

		// Перебираем строки
		foreach($arrayRowList as $arrayRow){



			// Вызываем событие перед началом удаления строки
			$this->trigger('remove.before.row', ...[$crossCondition, &$arrayRow, $this]);

			// Данные столбцов с первичными ключами
			$arrayPrimaryData = [];

			// Извлекаем данные
			foreach($this->getAllColumnPrimary() as $primaryObject){
				$stringName = $primaryObject->getNameUnescape();
				$arrayPrimaryData[$stringName] = $arrayRow[$stringName];
			}

			// Нормализуем данные для запросов
			$arrayDataNormalize = $this->normalizeRow($arrayPrimaryData);

			$this->getConnect()->query(
				'DELETE FROM ' . $this->getFullName() . ' WHERE  ' .
				\q\arr\implode(' AND ', $arrayDataNormalize, function($mixedValue, string $stringColumnName){
					return "$stringColumnName = $mixedValue";
				})
			);

			// Вызываем событие после удаления строки
			$this->trigger('remove.after.row', ...[$crossCondition, $arrayRow, $this]);
		}

		// Вызываем событие после удаления всех строки
		$this->trigger('remove.after', ...[$crossCondition, $arrayRowList, $this]);
	}

	/**
	 * Нормализация и проверка данных согласно их типу
	 *
	 * @param array $arrayData Массив данных
	 * @return array
	 */
	final public function normalizeRow(array $arrayData):array{

		// Нормализованные данные
		$arrayDataNormalize = [];

		// Перебираем ячейки которые нужно вернуть
		foreach($this->getAllColumnReal() as $realObject){

			// Пропускаем те что не находятся в массиве
			if(!isset($arrayData[$realObject->getNameUnescape()])){
				continue;
			}

			// Проверяем входящее значение и записываем в массив
			$arrayDataNormalize[$realObject->getName()] = $realObject
				->getType()->convertInString($arrayData[$realObject->getNameUnescape()])
			;
		}

		// Возвращаем проверенные ячейки
		return $arrayDataNormalize;
	}


	// События

	// remove

	/**
	 * Выполняется перед запросом строк для последующего удаления
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayOption
	 * @param Table $table
	 */
	final public function eventRemoveOption(CrossCondition $crossCondition, array &$arrayOption, Table $table){}

	/**
	 * Выполняется перед удалением для всего набора строк
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRowList
	 * @param Table $table
	 */
	final public function eventRemoveBefore(CrossCondition $crossCondition, array &$arrayRowList, Table $table){}

	/**
	 * Выполняется перед удалением каждой строки
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRow
	 * @param Table $table
	 */
	final public function eventRemoveBeforeRow(CrossCondition $crossCondition, array &$arrayRow, Table $table){}

	/**
	 * Выполняется после удаления каждой строки
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRow
	 * @param Table $table
	 */
	final public function eventRemoveAfterRow(CrossCondition $crossCondition, array $arrayRow, Table $table){}

	/**
	 * Выполняется после удаления всех строк
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRowList
	 * @param Table $table
	 */
	final public function eventRemoveAfter(CrossCondition $crossCondition, array $arrayRowList, Table $table){}


	// init

	/**
	 * Вызывается после инициализации всех таблиц
	 * @param Table $table Текущая таблица
	 * @param Db $db
	 * @param Orm $orm
	 */
	final public function eventInitJoin(Table $table, Db $db, Orm $orm){}


	// get

	/**
	 * Выполняется перед выполнением запроса для проверки и корректировки условий запроса
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayOption
	 * @param Table $table
	 */
	final public function eventGetOption(CrossCondition $crossCondition, array &$arrayOption, Table $table){}

	/**
	 * Выполняется перед тем как выполнить запрос к таблице
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRowList Опции запроса
	 * @param Table $table
	 */
	final public function eventGetBeforeRows(CrossCondition $crossCondition, array &$arrayRowList, Table $table){}

	/**
	 * Выполняется перед тем как выполнить запрос к таблице
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRow
	 * @param Table $table
	 * @internal param array $arrayRowList Опции запроса
	 */
	final public function eventGetBeforeRow(CrossCondition $crossCondition, array &$arrayRow, Table $table){}

	/**
	 * Выполняется перед тем как выполнить запрос к таблице
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRow
	 * @param Table $table
	 * @internal param array $arrayRowList Опции запроса
	 */
	final public function eventGetAfterRow(CrossCondition $crossCondition, array &$arrayRow, Table $table){}

	/**
	 * Выполняется перед тем как выполнить запрос к таблице
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayRowList Опции запроса
	 * @param Table $table
	 */
	final public function eventGetAfterRows(CrossCondition $crossCondition, array &$arrayRowList, Table $table){}



	/**
	 * Выполняется перед выполнением запроса
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayOption
	 * @param Table $table
	 */
	final public function eventGetBeforeSelect(CrossCondition $crossCondition, array &$arrayOption, Table $table){}



	// set

	/**
	 * Выполняется перед вставкой/обновлением записи
	 *
	 * @param CrossCondition $crossCondition
	 * @param array &$arrayData Данные от пользователя
	 * @param array|null $arrayDataOld Старые данные если происходит обновление
	 * @param Table $table Текущая таблица
	 */
	final public function eventSetBefore(CrossCondition $crossCondition, array &$arrayData, array $arrayDataOld = null, Table $table){}

	/**
	 * Выполняется перед вставкой записи
	 *
	 * @param CrossCondition $crossCondition
	 * @param array &$arrayData Данные от пользователя
	 * @param Table $table Текущая таблица
	 */
	final public function eventSetBeforeInsert(CrossCondition $crossCondition, array &$arrayData, Table $table){}

	/**
	 * Выполняется перед вставкой записи
	 *
	 * @param CrossCondition $crossCondition
	 * @param array &$arrayData Данные от пользователя
	 * @param array $arrayDataOld Старые данные если происходит обновление
	 * @param Table $table Текущая таблица
	 */
	final public function eventSetBeforeUpdate(CrossCondition $crossCondition, array &$arrayData, array $arrayDataOld, Table $table){}

	/**
	 * Выполняется после вставки записи
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayData Данные от пользователя
	 * @param array $arrayDataNew Новые данные
	 * @param Table $table Текущая таблица
	 */
	final public function eventSetAfterInsert(CrossCondition $crossCondition, array $arrayData, array $arrayDataNew, Table $table){}

	/**
	 * Выполняется после обновления записи
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayData Данные от пользователя
	 * @param array $arrayDataOld Старые данные если происходит обновление
	 * @param array $arrayDataNew Новые данные
	 * @param Table $table Текущая таблица
	 */
	final public function eventSetAfterUpdate(CrossCondition $crossCondition, array $arrayData, array $arrayDataOld, array $arrayDataNew, Table $table){}

	/**
	 * Выполняется после вставки/обновления записи
	 *
	 * @param CrossCondition $crossCondition
	 * @param array $arrayData Данные от пользователя
	 * @param array|null $arrayDataOld Старые данные если происходит обновление
	 * @param array $arrayDataNew Новые данные
	 * @param Table $table Текущая таблица
	 */
	final public function eventSetAfter(CrossCondition $crossCondition, array $arrayData, array $arrayDataOld = null, array $arrayDataNew, Table $table){}


	/**
	 * Конструктор запросов для получения выборки
	 * @return Select
	 */
	final public function select():Select{
		return new Select($this);
	}

	/**
	 * Конструктор запросов для удаления выборки
	 *
	 * @return Delete
	 */
	final public function delete():Delete{
		return new Delete($this);
	}
}