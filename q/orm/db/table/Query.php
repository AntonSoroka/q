<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 14.11.2017
 * Time: 11:19
 */

namespace q\orm\db\table;



use q\orm\db\Table;

abstract class Query{

	/**
	 * @var Table
	 */
	protected $table;

	/**
	 * @var array
	 */
	protected $arrayParams;

	public function __construct(Table $table){
		$this->table = $table;
		$this->arrayParams = [
			'where' => [],
			'having' => [],
			'order' => [],
			'depth' => [],
			'cell' => [],
		];
	}

	/**
	 * Выполнить запрос
	 */
	abstract public function query();



	/**
	 * Добавить условие
	 *
	 * @param string $stringCell
	 * @param string|int|float|array|bool|null|Clause $mixedValue
	 * @return $this
	 */
	public function where(string $stringCell, $mixedValue):self{
		$this->arrayParams['where'][$stringCell] = $mixedValue;
		return $this;
	}

	/**
	 * Добавить условие
	 *
	 * @param string $stringCell
	 * @param string|int|float|array|bool|null|Clause $mixedValue
	 * @return $this
	 */
	public function having(string $stringCell, $mixedValue):self{
		$this->arrayParams['having'][$stringCell] = $mixedValue;
		return $this;
	}

	/**
	 * Добавить несколько условий
	 *
	 * @param array[string]string|int|float|array|bool|null|Clause $arrayWhereList
	 * @return $this
	 */
	public function wheres(array $arrayWhereList):self{
		foreach($arrayWhereList as $stringCell => $mixedValue){
			$this->arrayParams['where'][$stringCell] = $mixedValue;
		}
		return $this;
	}

	/**
	 * Добавить несколько условий
	 *
	 * @param array[string]string|int|float|array|bool|null|Clause $arrayWhereList
	 * @return $this
	 */
	public function havings(array $arrayHavingList):self{
		foreach($arrayHavingList as $stringCell => $mixedValue){
			$this->arrayParams['having'][$stringCell] = $mixedValue;
		}
		return $this;
	}

	/**
	 * Установить лимит записей и начало выборки
	 *
	 * @param int $intLimit Лимит возвращаемых значений
	 * @param int $intOffset Начало выборки
	 * @return $this
	 */
	public function limit(int $intLimit, int $intOffset = null):self{
		$this->arrayParams['limit'] = $intLimit;

		if($intOffset !== null){
			$this->arrayParams['offset'] = $intOffset;
		}

		return $this;
	}

	/**
	 * Начало выборки
	 *
	 * @param int $intOffset
	 * @return $this
	 */
	public function offset(int $intOffset):self{
		$this->arrayParams['offset'] = $intOffset;
		return $this;
	}

	/**
	 * Добавить сортировку
	 *
	 * @param string $stringCell
	 * @param bool $boolAsc
	 * @return $this
	 */
	public function order(string $stringCell, bool $boolAsc = true):self{
		$this->arrayParams['order'][] = ($boolAsc ? '+' : '-') . $stringCell;
		return $this;
	}

	/**
	 * Добавить несколько сортировок
	 *
	 * @param array $arrayOrderList
	 * @return $this
	 */
	public function orders(array $arrayOrderList):self{
		foreach($arrayOrderList as $stringCell => $boolAsc){
			$this->arrayParams['order'][] = ($boolAsc ? '+' : '-') . $stringCell;
		}
		return $this;
	}

	/**
	 * Добавить ячейку которая должна быть в результатирующем наборе
	 *
	 * @param string $stringCell
	 * @return $this
	 */
	public function cell(string $stringCell):self{
		$this->arrayParams['cell'][] = $stringCell;
		return $this;
	}

	/**
	 * Добавить несколько ячеек которые должны быть в результатирующем наборе
	 *
	 * @param array $arrayString
	 * @return $this
	 */
	public function cells(array $arrayString):self{
		foreach($arrayString as $stringCell){
			$this->arrayParams['cell'] = $stringCell;
		}
		return $this;
	}

	/**
	 * Глубина запросов ко всем связанным таблицам
	 *
	 * @param int $intDepth
	 * @return $this
	 */
	public function depthDefault(int $intDepth):self{
		$this->arrayParams['depthDefault'] = $intDepth;
		return $this;
	}

	/**
	 * Глубина запроса только к одной из связанных таблиц
	 *
	 * @param string $stringCell
	 * @param int $intDepth
	 * @return $this
	 */
	public function depth(string $stringCell, int $intDepth):self{
		$this->arrayParams['depth'][$stringCell] = $intDepth;
		return $this;
	}

	/**
	 * Глубина запроса к нескольким связанным таблицам
	 *
	 * @param array $arrayDepthList
	 * @return $this
	 */
	public function depths(array $arrayDepthList){
		foreach($arrayDepthList as $stringCell => $intDepth){
			$this->arrayParams['depth'][$stringCell] = $intDepth;
		}
		return $this;
	}

	/**
	 * Запомнить общее количество строк в результатирующем наболе без limit и offset
	 * @return $this
	 */
	public function found():self{
		$this->arrayParams['found'] = true;
		return $this;
	}


}