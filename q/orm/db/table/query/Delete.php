<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 14.11.2017
 * Time: 14:25
 */

namespace q\orm\db\table\query;


use q\orm\db\table\Query;

class Delete extends Query{

	/**
	 * Удаляет полученую выборку из базы
	 */
	public function query():void{
		$this->table->remove($this->arrayParams);
	}
}