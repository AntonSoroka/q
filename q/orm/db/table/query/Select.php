<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 14.11.2017
 * Time: 14:28
 */

namespace q\orm\db\table\query;


use q\orm\db\table\Query;

class Select extends Query{

	/**
	 * Выполняет запрос и возвращает выборку
	 */
	public function query():array{
		return $this->table->get($this->arrayParams);
	}

	/**
	 * Выполняет запрос к таблице и возвращает только первый вариант
	 * @return array|null
	 */
	public function first(){
		$this->limit(1);
		$arrayResultList = $this->query();
		return $arrayResultList[0] ?? null;
	}

	/**
	 * Время на которое данный запрос будет кешироваться
	 *
	 * @param float $floatTime
	 * @return $this
	 */
	public function cache(float $floatTime):self{
		$this->arrayParams['cache'] = $floatTime;
		return $this;
	}
}