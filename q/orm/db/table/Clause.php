<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.04.2017
 * Time: 8:19
 */

namespace q\orm\db\table;

/**
 * Class Clause - выражение для вычисления ячеёки
 * @package q\qrm\db\table
 */
class Clause
{
	/**
	 * @var string Условие строкой
	 */
	protected $stringWhere;

	/**
	 * @var string Эта подстрока из условия будет заменена на ячейку
	 */
	protected $stringReplaceToValue;

	/**
	 * ManualWhere constructor.
	 * @param string $stringWhere Строка условия
	 * @param string $stringReplaceToValue Эта подстрока из условия будет заменена на ячейку
	 */
	public function __construct(string $stringWhere, string $stringReplaceToValue = '{{value}}'){
		$this->stringReplaceToValue = $stringReplaceToValue;
		$this->stringWhere = $stringWhere;
	}

	// Подстановка значения в условие
	public function getWhere(string $stringValue){
		return str_replace($this->stringReplaceToValue, '(' . $stringValue . ')', $this->stringWhere);
	}
}