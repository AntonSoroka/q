<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 26.04.2017
 * Time: 15:24
 */

namespace q\orm\db\table;


use q\Orm;
use q\orm\Db;
use q\orm\db\Table;
use function q\sql\escapeName;

abstract class Column
{
	/**
	 * @var string Имя ячейки
	 */
	protected $stringName;

	/**
	 * @return string Имя ячейки
	 */
	public function getName():string{
		return $this->stringName;
	}

	/**
	 * @param string|null $stringAlias
	 * @return string Имя ячейки
	 */
	public function getNameAlias(string $stringAlias = null):string{
		if($stringAlias === null){
			return $this->getTable()->getName() . '.' . $this->stringName;
		}

		return escapeName($stringAlias) . '.' . $this->stringName;

	}

	/**
	 * @var string Имя ячейки
	 */
	protected $stringNameUnescape;

	/**
	 * @return string Имя ячейки
	 */
	public function getNameUnescape():string{
		return $this->stringNameUnescape;
	}

	/**
	 * @return string Полный адрес ячейки и названием ORM
	 */
	final public function getInfoName():string{
		return $this->objectTable->getInfoName() . '.' . $this->getName();
	}


	/**
	 * @var Table Таблица в которой находится ячейка
	 */
	protected $objectTable;

	/**
	 * @return Table Таблица к которой прикреплена ячейка
	 */
	final public function getTable():Table{
		return $this->objectTable;
	}

	/**
	 * @return Db База данных в которой хранится ячейка
	 */
	final public function getDb():Db{
		return $this->objectTable->getDb();
	}

	/**
	 * @return Orm Родительская ORM этой ячейки
	 */
	final public function getOrm():Orm{
		return $this->objectTable->getOrm();
	}

	/**
	 * Cell constructor.
	 * @param string $stringName
	 * @param Table $objectTable
	 */
	public function __construct(string $stringName, Table $objectTable){

		// Экранируем и записываем имя
		$this->stringName = \q\sql\escapeName($stringName);

		// Убираем экранирование и записываем имя без него
		$this->stringNameUnescape = \q\sql\unescapeName($stringName);

		// Записываем таблицу к которой пренадлежит ячейка
		$this->objectTable = $objectTable;

		// Записываем себя в ячейку
		$this->objectTable->writeColumn($this);
	}
}