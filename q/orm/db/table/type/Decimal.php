<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 15:04
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class Decimal extends Type
{

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue)
	{
		// Проверяем пришедшее значение
		$floatResult = filter_var($mixedValue,FILTER_VALIDATE_FLOAT, ['options' => ['default' => false]]);

		// Если это явно не дробное
		if($floatResult === false){
			static::errorType($mixedValue, 'decimal');
		}

		// Возвращаем в точности то что пришло от пользователя, php может отрезать
		// часть числа, т.к. ему не зватает точности
		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}