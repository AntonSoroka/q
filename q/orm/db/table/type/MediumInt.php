<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.04.2017
 * Time: 13:19
 */

namespace q\orm\db\table\type;


class MediumInt extends Integer{

	protected $mixedMax = 8388607;
	protected $mixedMin = -8388608;
	protected $mixedMaxUnsigned = 16777215;
}