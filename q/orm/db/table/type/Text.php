<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 16:56
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class Text extends Type
{
	/**
	 * @var int Максимальное количество символов которое можно сохранить
	 */
	protected $intMaxCharLength = 65535;


	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \LengthException
	 */
	protected function parseIn($mixedValue)
	{
		// Длинна поля больше допустимого значения
		if(mb_strlen($mixedValue) > $this->intMaxCharLength){
			static::errorType(mb_substr($mixedValue, 0, 10) . '...(' . mb_strlen($mixedValue) . ')', 'max length: ' . $this->intMaxCharLength);
		}

		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}