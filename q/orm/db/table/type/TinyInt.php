<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.04.2017
 * Time: 12:51
 */

namespace q\orm\db\table\type;


class TinyInt extends Integer
{
	protected $mixedMax = 127;
	protected $mixedMin = -128;
	protected $mixedMaxUnsigned = 255;
}