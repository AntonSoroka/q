<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.2017
 * Time: 0:19
 */

namespace q\orm\db\table\type;


use q\orm\db\table\column\Returned;
use q\orm\db\table\Type;

class Integer extends Type{

	/**
	 * @var int Максимальная длинна значения в символах
	 */
	protected $intMaxLength;

	/**
	 * @var bool Указывает что значение беззнаковое
	 */
	protected $boolUnsigned = false;

	/**
	 * @var int|string Максимальное значение
	 */
	protected $mixedMax = 2147483647;

	/**
	 * @var int|string Минимальное значение
	 */
	protected $mixedMin = -2147483648;

	/**
	 * @var int|string Максимальное беззнаковое число
	 */
	protected $mixedMaxUnsigned = 4294967295;

	/**
	 * @var bool Указывает что данные будут передаваться как строки
	 */
	protected $boolIsString = false;

	/**
	 * @return bool Является ли значение беззнаковым
	 */
	public function isUnsigned():bool{
		return $this->boolUnsigned;
	}

	/**
	 * @return int Максимальная длинна значения
	 */
	public function getLength():int{
		return $this->intMaxLength;
	}


	/**
	 * Integer constructor.
	 * @param bool $boolUnsigned
	 * @param Returned|null $objectReturned
	 */
	public function __construct(
		bool $boolUnsigned = false,
		Returned $objectReturned = null
	){
		parent::__construct($objectReturned);
		$this->boolUnsigned = $boolUnsigned;

		// Считаем максмальную длинну поля
		if($this->boolUnsigned){
			$this->intMaxLength = strlen((string)$this->mixedMaxUnsigned);
		}else{
			$this->intMaxLength = strlen((string)$this->mixedMin);
		}

		// Числа выходят за пределы доступные php и их нужно передавать как строки
		if(
			is_string($this->mixedMaxUnsigned) ||
			is_string($this->mixedMax) ||
			is_string($this->mixedMin)
		){
			$this->boolIsString = true;
		}

	}


	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue){

		// Если пришло не число то бросаем исключение
		if(!is_numeric($mixedValue)){
			static::errorType($mixedValue, 'numeric');
		}

		// Если число выходит за приделы, то возвращаем как есть
		if($this->boolIsString) {
			return $mixedValue;
		}

		// Давпазон значений
		$intMin = $this->boolUnsigned ? 0 : $this->mixedMin;
		$intMax = $this->boolUnsigned ? $this->mixedMaxUnsigned : $this->mixedMax;

		// Проверяем число
		$mixedValue = filter_var($mixedValue, FILTER_VALIDATE_INT, [
			'options' => [
				'default' => false,
				'min_range' => $intMin,
				'max_range' => $intMax,
			],
			'flags' =>
				FILTER_FLAG_ALLOW_OCTAL |
				FILTER_FLAG_ALLOW_HEX
			,
		]);

		// Число не прошло валидацию значит оно выходит за пределы
		if($mixedValue === false){
			static::errorType($mixedValue, $intMin . ' - ' . $intMax);
		}

		// возвращаем проверенное число
		return $mixedValue;
	}

	 /**
	  * Преобразовать значение ячейки для вывода значения пользователю
	  *
	  * @param $mixedValue
	  * @return mixed Распарсеное значение
	  */
	 protected function parseOut(string $mixedValue){
		 return $mixedValue;
	 }
 }