<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 10:33
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class Year extends Type
{

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseIn($mixedValue)
	{
		$mixedValue = (string)$mixedValue;

		// Может равнятся 0000
		if(preg_match('#^0{1,4}$#', $mixedValue)){
			return '0000';
		}

		// Может состоять из четырёх цифр
		if(preg_match('#^\d\d\d\d$#', $mixedValue)){

			// Год цифрой
			$intValue = (int)$mixedValue;

			// Должно входит в интервал ижду 1901 и 2155 годом
			if($intValue >= 1901 && $intValue <= 2155){
				return (string)$intValue;
			}
		}
		/*
		// Это тоже работает для Mysql, но лучше этот вариант не использовать
		else if(preg_match('#^\d\d$#', $mixedValue)){

			// Год цифрой
			$intValue = (int)$mixedValue;

			if($intValue < 70){
				return '20' . $intValue;
			}else{
				return '19' . $intValue;
			}
		}
		*/
		static::errorType($mixedValue, '1901-2155 OR 0000');
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}