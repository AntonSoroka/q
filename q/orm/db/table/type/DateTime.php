<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 15:21
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class DateTime extends Type
{

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue)
	{
		if(is_int($mixedValue) && $mixedValue > 0){
			$mixedValue = date('Y-m-d H:i:s', time($mixedValue));
		}
		else if(!preg_match('#^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$#usi', $mixedValue)){
			static::errorType($mixedValue, 'Y-m-d H:i:s or int');
		}

		// Всё в порядке
		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}