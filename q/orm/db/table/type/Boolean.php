<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 14:49
 */

namespace q\orm\db\table\type;



class Boolean extends Integer
{
	protected $mixedMax = 1;
	protected $mixedMin = 0;
	protected $mixedMaxUnsigned = 1;

	/**
	 * Boolean constructor.
	 * @param null $objectReturned
	 */
	public function __construct($objectReturned = null)
	{
		parent::__construct(false, $objectReturned);
	}

	/**
	 * @param $mixedValue
	 * @return mixed
	 * @throws \TypeError
	 */
	public function parseIn($mixedValue)
	{
		// Проверяем входящее значение
		$boolValue = filter_var($mixedValue, FILTER_VALIDATE_BOOLEAN, [
			'flags' => FILTER_NULL_ON_FAILURE,
		]);

		// Пло явно не булевое значение
		if(null === $boolValue){
			static::errorType($mixedValue, 'boolean');
		}

		// Возвращаем проверенное значение
		return $boolValue;
	}

	/**
	 * @param string $mixedValue
	 * @return mixed
	 */
	public function parseOut(string $mixedValue)
	{
		return filter_var($mixedValue, FILTER_VALIDATE_BOOLEAN);
	}
}