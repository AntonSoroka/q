<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 9:33
 */

namespace q\orm\db\table\type;


class LongText extends Text
{
	protected $intMaxCharLength = 4294967295;
}