<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 10:45
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class Time extends Type
{

	protected $intMinValue = -3020399;
	protected $intMaxValue = 3020399;

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue)
	{
		// Переводим в строку
		$stringTime = (string)$mixedValue;

		// Явно не верный тип
		if(!preg_match('#^-?((((\d?\d?\d:)?\d)?\d:)?\d)?\d$#', $mixedValue)){
			static::errorType($mixedValue, 'time regular: #^-?((((\d?\d?\d:)?\d)?\d:)?\d)?\d$#');
		}

		// Начальные значения
		$intHours = 0;
		$intMinutes = 0;
		$intSeconds = 0;

		// Проверяем является ли время отрицательным
		$boolNegative = $stringTime[0] === '-';

		// Убираем ведущие нули
		$stringTime = preg_replace('#(-|:|^)0+#', '$1', $stringTime);

		// Убираем минус с начала строки
		$stringTime = preg_replace('#^-#', '', $stringTime);

		// Часы:Минуты:Секунды
		if(preg_match('#^\d+:\d+:\d+$#', $stringTime)){
			sscanf($stringTime, '%d:%d:%d', $intHours, $intMinutes, $intSeconds);
		}

		// Минуты:Секунды
		else if(preg_match('#^\d+:\d+$#', $stringTime)){
			sscanf($stringTime, '%d:%d', $intMinutes, $intSeconds);
		}

		// Секунды
		else if(preg_match('#^\d+$#', $stringTime)){
			sscanf($stringTime, '%d', $intSeconds);
		}

		// Считаем количество секунд
		$intSeconds = ($boolNegative ? -1 : 1) * ($intHours * 3600 + $intMinutes * 60 + $intSeconds);

		// Меньше чем можно
		if($intSeconds < $this->intMinValue){
			static::errorType($mixedValue . ' (' .$intSeconds . ' sec)', 'min: ' . $this->intMinValue);
			throw new \LengthException("Min value is {$this->intMinValue} seconds you value is {$intSeconds} seconds. {$mixedValue}");
		}

		// Меньше чем можно
		if($intSeconds > $this->intMaxValue){
			static::errorType($mixedValue . ' (' .$intSeconds . ' sec)', 'max: ' . $this->intMaxValue);
		}

		return $intSeconds;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}