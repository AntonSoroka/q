<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 9:31
 */

namespace q\orm\db\table\type;


class TinyText extends Text
{
	protected $intMaxCharLength = 255;
}