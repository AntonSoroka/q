<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.04.2017
 * Time: 15:20
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class Real extends Type
{
	
	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue){
		
		// Это явно не число
		if(!is_numeric($mixedValue)){
			static::errorType($mixedValue, 'numeric');
		}

		// Всё в порядке это походе на число
		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param string $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue){
		return $mixedValue;
	}
}