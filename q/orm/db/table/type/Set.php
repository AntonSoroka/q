<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 9:55
 */

namespace q\orm\db\table\type;


class Set extends Enum
{
	/**
	 * @param $mixedValue
	 * @return mixed
	 * @throws \UnexpectedValueException
	 */
	protected function parseIn($mixedValue)
	{
		// Значения должны быть в массиве
		if(!is_array($mixedValue)){
			$mixedValue = explode(',', $mixedValue);
		}

		// Проверяем все значения
		foreach($mixedValue as $mixedKey => $stringValue){

			if($stringValue === ''){
				unset($mixedValue[$mixedKey]);
				continue;
			}

			if(!in_array($stringValue, $this->arrayVariants, true)){
				static::errorType($stringValue, implode(', ', $this->arrayVariants));
			}
		}

		return \implode(',', $mixedValue);
	}

	protected function parseOut(string $mixedValue)
	{
		return explode(',',$mixedValue);
	}
}