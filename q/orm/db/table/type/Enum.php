<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 9:34
 */

namespace q\orm\db\table\type;



use q\orm\db\table\Type;

class Enum extends Type
{

	/**
	 * @var int Максимальное количество вариантов
	 */
	protected $intMaxVariants = 65535;

	/**
	 * @var string[] Варианты значений этого поля
	 */
	protected $arrayVariants = [];

	public function getVariants():array{
		return $this->arrayVariants;
	}

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseIn($mixedValue)
	{
		// Enum всегда может содержать пустое значение
		if($mixedValue === ''){
			return '';
		}
		
		// Enum всегда может содержать значение null
		if($mixedValue === null){
			return null;
		}
		
		// Это поле не может принимать данное значение
		if(!in_array($mixedValue, $this->arrayVariants, false)){
			static::errorType($mixedValue, implode(', ', $this->arrayVariants));
		}

		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}

	/**
	 * Enum constructor.
	 * @param array $arrayVariants Список значений
	 * @param null $objectReturned
	 * @throws \LogicException
	 */
	public function __construct(array $arrayVariants, $objectReturned = null)
	{
		parent::__construct($objectReturned);

		$intCountVariant = count($arrayVariants);

		// Вариантов больше чем разрешено типом enum
		if($intCountVariant > $this->intMaxVariants){
			throw new \LengthException("Max variants for enum cell is {$this->intMaxVariants}");
		}

		// Преобразовываем все элементы в строки
		$this->arrayVariants = array_map(
			function($stringVariant):string{
				return $stringVariant;
			},
			$arrayVariants
		);


		// Есть повтаряющиеся значения
		if($intCountVariant !== count(array_unique($this->arrayVariants))){

			// Находим повторения
			$arrayRepeat = array_count_values($this->arrayVariants);
			$arrayRepeat = array_diff($arrayRepeat, [1]);
			$arrayRepeat = array_keys($arrayRepeat);
			$stringRepeat = implode(', ', $arrayRepeat);

			// Генерируем исключение
			throw new \LogicException("The variant are repeated: {$stringRepeat}");
		}
	}
}