<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.04.2017
 * Time: 15:07
 */

namespace q\orm\db\table\type;



class BigInt extends Integer{
	protected $mixedMax = '9223372036854775807';
	protected $mixedMin = '-9223372036854775808';
	protected $mixedMaxUnsigned = '18446744073709551615';
}