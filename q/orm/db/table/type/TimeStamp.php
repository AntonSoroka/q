<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 15:25
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class TimeStamp extends Type
{

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue)
	{
		// Пришло как число
		if(is_int($mixedValue) && $mixedValue >= 0){
			return date('Y-m-d H:i:s', $mixedValue);
		}

		// Пытаемся распознать
		$intValue = strtotime($mixedValue);

		// Произошла ошибка
		if($intValue === false){
			static::errorType($mixedValue, 'timestamp or Y-m-d H:i:s');
		}

		// Возвращаем метку времени
		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}