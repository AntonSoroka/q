<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.04.2017
 * Time: 12:53
 */

namespace q\orm\db\table\type;


class SmallInt extends Integer
{
	protected $mixedMax = 32767;
	protected $mixedMin = -32768;
	protected $mixedMaxUnsigned = 65535;
}