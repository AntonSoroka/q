<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.04.2017
 * Time: 14:58
 */

namespace q\orm\db\table\type;


use q\orm\db\table\column\Returned;
use q\orm\db\table\Type;

class Varchar extends Type
{
	/**
	 * @var int Длинна поля
	 */
	protected $intLength;

	/**
	 * @var int Максимальная длинна поля
	 */
	private $intMaxLength = 65535;
	
	
	/**
	 * Varchar constructor.
	 *
	 * @param Returned $objectReturned
	 * @param int $intLength
	 * @param bool|null $boolIndex
	 */
	public function __construct(int $intLength = 255, bool $boolIndex = null, Returned $objectReturned = null){

		// Длинна поля больше чем допускает MySql
		if($intLength > $this->intMaxLength){
			throw new \LengthException("VARCHAR max length {$this->intMaxLength}");
		}

		// Длинна поля меньше единицы, это не допустимо
		if($intLength < 1){
			throw new \LengthException('Less than one');
		}

		parent::__construct($objectReturned);

		// Записываем длинну значения
		$this->intLength = $intLength;
	}

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseIn($mixedValue):string{

		// Преобразуем значение
		$stringValue = (string)$mixedValue;

		// Длинна строки привышает максимально возможную
		if(mb_strlen($stringValue) > $this->intMaxLength){
			static::errorType(mb_substr($mixedValue, 0 ,10) . '...(' . mb_strlen($stringValue) . ')', 'max: ' . $this->intMaxLength);
		}

		return $stringValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue){
		return $mixedValue;
	}
}