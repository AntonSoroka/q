<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 9:32
 */

namespace q\orm\db\table\type;


class MediumText extends Text
{
	protected $intMaxCharLength = 16777215;
}