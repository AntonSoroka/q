<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 15:11
 */

namespace q\orm\db\table\type;


use q\orm\db\table\Type;

class Date extends Type
{

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 * @throws \TypeError
	 */
	protected function parseIn($mixedValue)
	{
		
		if(!preg_match('#^\d\d\d\d-\d\d-\d\d$#usi', $mixedValue)){
			throw new \TypeError("Expected date yyyy-mm-dd, '{$mixedValue}'");
		}

		// Всё в порядке
		return $mixedValue;
	}

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	protected function parseOut(string $mixedValue)
	{
		return $mixedValue;
	}
}