<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.04.2017
 * Time: 13:30
 */

namespace q\orm\db\table;


use q\orm\db\table\column\Cell;
use q\orm\db\table\column\Returned;

abstract class Type
{

	/**
	 * @var Returned
	 */
	protected $objectReturned;

	/**
	 * Преобразование значения для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed
	 * @throws \LogicException
	 */
	final public function convertIn($mixedValue){

		// Это не физическая ячейка из таблицы
		if(!($this->objectReturned instanceof Cell)){
			throw new \LogicException(
				"Only table cells can take a value for writing in {$this->objectReturned->getFullName()}"
			);
		}

		// Пришло null и его можно вернуть
		if(null === $mixedValue and $this->objectReturned->canBeNull()){
			return null;
		}

		// Возвращаем значение не изменяя его
		return $this->parseIn($mixedValue);
	}

	final public function convertInString($mixedValue):string{
		return \q\sql\escape($this->convertIn($mixedValue));
	}

	/**
	 * Парсит значенеие из строки
	 *
	 * @param $mixedValue
	 * @return mixed
	 */
	final public function convertOut($mixedValue){

		// Пришло null и его можно вернуть
		if(null === $mixedValue and $this->objectReturned->canBeNull()){
			return null;
		}

		// Возвращаем значение не изменяя его
		return $this->parseIn($mixedValue);
	}

	/**
	 * Преобразовать значение в нужный вид для записи в базу
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	abstract protected function parseIn($mixedValue);

	/**
	 * Преобразовать значение ячейки для вывода значения пользователю
	 *
	 * @param $mixedValue
	 * @return mixed Распарсеное значение
	 */
	abstract protected function parseOut(string $mixedValue);
	
	/**
	 * Type constructor.
	 *
	 * @param Returned|null $objectReturned
	 */
	public function __construct(Returned $objectReturned = null){
		if(null !== $objectReturned){
			$this->writeReturned($objectReturned);
		}
	}
	
	/**
	 * Устанавливает ячейку сопаставленную с данным типом
	 *
	 * @param Returned $objectReturned
	 */
	final public function writeReturned(Returned $objectReturned){
		
		// Ячейка уже записана
		if(isset($this->objectReturned)){
			throw new \OverflowException('Returned column already set');
		}

		// Записываем ячейку
		$this->objectReturned = $objectReturned;

		// Записываем свой тип в ячейку
		$objectReturned->writeType($this);

	}
	
	/**
	 * @return Returned
	 */
	final public function getReturned():Returned{
		return $this->objectReturned;
	}

	/**
	 * @param mixed $mixedValue
	 * @param string|null $stringExpected
	 */
	public function errorType($mixedValue, string $stringExpected = null):void{

		if(is_object($mixedValue)){
			$mixedValue = gettype($mixedValue);
		}else if(is_array($mixedValue)){
			$mixedValue = print_r($mixedValue, true);
		}else{
			$mixedValue = \q\sql\escape($mixedValue);
		}

		throw new \LogicException(
			'Error type value in. ' .
			'Cell name: ' . $this->getReturned()->getFullName() . '. ' .
			($stringExpected === null ? '' : 'Expected: \'' . $stringExpected .  '\'. ') .
			'Class: ' . static::class . ', value: ' . $mixedValue . '.'
		);
	}

}