<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 26.04.2017
 * Time: 16:08
 */

namespace q\orm\db\table\column;


use q\Orm;
use q\orm\Db;
use q\orm\db\Table;
use q\orm\db\table\Type;

/**
 * Class Extend
 * Этот тип ячеек сделан для того что-бы включить в результатирующий набор вычисляемые значения
 * т.е. значения этих ячеек не хранятся в таблицах а вычисляются на ходу
 *
 * @package q\orm\db\table\column
 */
class Extend extends Returned
{
	/**
	 * @var callable Выражение для вычисления ячейки
	 */
	protected $functionGetExpression;

	/**
	 * @var array Параметры для функции $functionGetExpression
	 */
	protected $arrayParameters = [];
	
	/**
	 * @var string Вычисленное выражение
	 */
	protected $stringExpression = null;
	
	/**
	 * @return string
	 */
	public function getExpression(): string{
		
		// Вычисляем выражение
		if($this->stringExpression === null){
			$this->stringExpression = '(' . ($this->functionGetExpression)(...$this->arrayParameters) . ')';
		}
		
		// Возвращаем выражение
		return $this->stringExpression;
	}

	/**
	 * Extend constructor.
	 *
	 * @param string $stringName
	 * @param Type $typeObject
	 * @param callable $functionGetExpression
	 * @param Table $objectTable
	 * @throws \ReflectionException
	 * @throws \LogicException
	 */
	public function __construct(string $stringName, Type $typeObject, callable $functionGetExpression, Table $objectTable)
	{
		parent::__construct($stringName, $typeObject, $objectTable);
		
		// Разбираем функцию для проверки
		$reflectionFunction = new \ReflectionFunction($functionGetExpression);
		$reflectionParameters = $reflectionFunction->getParameters();
		
		// Она должна возвращать строку
		if((string)$reflectionFunction->getReturnType() !== 'string'){
			throw new \LogicException('Function generates an expression does not return a string');
		}

		/** @var \ReflectionClass[] $arrayReflectionClass */
		$arrayReflectionClass = [
			new \ReflectionClass(Orm::class),
			new \ReflectionClass(Db::class),
			new \ReflectionClass(Table::class),
		];

		// Параметры для подстановки к типам из $arrayReflectionClass
		$arrayParameters = [
			$this->getOrm(),
			$this->getDb(),
			$this->getTable(),
		];

		// Проверяем типы аргументов
		foreach($reflectionParameters as $reflectionParameter){

			// Разрешол ли данный тип переменной
			$boolIsPermittedType = false;

			// Сверяем со списком разрешонных типов
			foreach ($arrayReflectionClass as $intIndexClass => $reflectionClass){
				if(
					// Если является этим классои
					$reflectionParameter->getClass()->getName() === $reflectionClass->getName() ||
					// Или наследуется от него
					in_array($reflectionClass->getName(), class_parents($reflectionParameter->getClass()->getName()))
				){

					// Указываем что текущий параметр разрешон
					$boolIsPermittedType = true;

					// Добавляем параметр
					$this->arrayParameters[]  = $arrayParameters[$intIndexClass];

					// Прекращаем поиск
					break;
				}
			}

			// Тип параметра не разрешён
			if(!$boolIsPermittedType){
				throw new \LogicException(
					"Type {$reflectionParameter->getType()} not permitted. " .
					'Use ' . Orm::class . ', ' . Db::class . ' or ' . Table::class . '.'
				);
			}
		}
		
		// Записываем функцию
		$this->functionGetExpression = $functionGetExpression;
	}

	/**
	 * @return bool Всегда может принимать значение null
	 */
	public function canBeNull():bool{
		return true;
	}
}