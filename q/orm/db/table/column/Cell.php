<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.04.2017
 * Time: 16:35
 */

namespace q\orm\db\table\column;


/**
 * Class Normal
 * Обычная ячейка хранящаяся в таблицы
 *
 * @package q\orm\db\table\column
 */
class Cell extends Returned
{
	/**
	 * @var bool Ячейка может принимать значение null
	 */
	protected $boolAcceptNull = false;

	/**
	 * @return bool Может ли ячейка возвращать тип null
	 */
	public function canBeNull():bool{
		return $this->boolAcceptNull;
	}

	/**
	 * Заставляет ячейку принимать значение null
	 *
	 * @return static
	 */
	public function acceptNull(){
		$this->boolAcceptNull = true;
		return $this;
	}

	/**
	 * @var mixed значение по умолчанию для данного поля
	 */
	protected $mixedDefault;

	/**
	 * Устанавливает значение по умолчанию для данной ячейки
	 *
	 * @param $mixedValue
	 * @return static
	 */
	public function setDefault($mixedValue){
		$this->mixedDefault = $mixedValue;
		return $this;
	}

	/**
	 * Возвращает значение по умолчанию для данной ячейки
	 *
	 * @return mixed
	 */
	public function getDefault(){
		return $this->mixedDefault;
	}

	/**
	 * @var string Описание ячейки в таблице
	 */
	protected $stringDescription;

	/**
	 * Возвращает описание ячейки
	 * @return string
	 */
	public function getDescription(): string{
		return $this->stringDescription;
	}

	/**
	 * Устанавливаем описание ячейки
	 *
	 * @param string $stringDescription
	 * @return static
	 */
	public function setDescription(string $stringDescription){
		$this->stringDescription = $stringDescription;
		return $this;
	}
	
	/**
	 * Получить строковое выражение для извлечения значения ячейки
	 *
	 * @return string
	 */
	public function getExpression():string{
		return $this->objectTable->getFullName() . '.' . $this->getName();
	}
}