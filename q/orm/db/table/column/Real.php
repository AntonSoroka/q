<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.04.2017
 * Time: 21:38
 */

namespace q\orm\db\table\column;


use q\orm\db\table\Clause;
use q\orm\db\table\Column;
use q\orm\db\table\Type;

abstract class Real extends Column{
	
	/**
	 * Получить строковое выражение для извлечения значения ячейки
	 *
	 * @return string
	 */
	abstract public function getExpression():string;
	
	
	/**
	 * Возвращает выражение для генерации условия
	 *
	 * @param $mixedValue
	 * @return string
	 */
	public function getWhere($mixedValue):string{
		return static::where($mixedValue, $this->getExpression());
	}

	public static function where($mixedValue, $stringExpression):string{

		// Массив значений
		if(is_array($mixedValue)){

			// Сбрасываем ключи
			$arrayValue = array_values($mixedValue);

			// Удаляем дубликаты
			$arrayValue = array_unique($arrayValue);

			// Указывает что в массиве есть значение null
			$boolNullExist = false;

			// Проверяем есть ли значение null
			if(in_array(null, $arrayValue, true)){

				// Удаляем значение null из массива
				array_splice($arrayValue, array_search(null, $arrayValue, true), 1);

				// Меняем флаг наличия значения null
				$boolNullExist = true;
			}

			// Части условия
			$arrayParts = [];

			// Ячейка может быть равна NULL
			if($boolNullExist){
				$arrayParts[] = $stringExpression . ' IS NULL';
			}

			// Вариантов больше чем ноль
			if(count($mixedValue) !== 0){
				$arrayParts[] = (
					$stringExpression . ' IN (' .
					\q\arr\implode(',', $arrayValue, '\q\sql\escape') .
					')'
				);
			}

			// Вариантов нету
			switch(count($arrayParts)){
				case 0: return \q\sql\escape(FALSE);
				case 1: return $arrayParts[0];
				default: return '(' . \implode(' OR ', $arrayParts) . ')';
			}
		}

		else if($mixedValue instanceof Clause){
			return $mixedValue->getWhere($stringExpression);
		}

		else if(null === $mixedValue){
			return $stringExpression . ' IS NULL';
		}

		return $stringExpression . '=' . \q\sql\escape($mixedValue);
	}
	
	/**
	 * @var Type Тип ячейки
	 */
	protected $objectType;

	/**
	 * @return Type
	 */
	public function getType():Type{
		return $this->objectType;
	}
	

}
