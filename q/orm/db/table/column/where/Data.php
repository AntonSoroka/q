<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 02.11.2017
 * Time: 16:11
 */

namespace q\orm\db\table\column\where;


class Data{

	protected $mixedData;

	public function __construct($mixedData){
		$this->mixedData = $mixedData;
	}

	public function getData(){
		return $this->mixedData;
	}
}