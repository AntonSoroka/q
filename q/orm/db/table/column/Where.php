<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.04.2017
 * Time: 8:27
 */

namespace q\orm\db\table\column;


use q\Orm;
use q\orm\Db;
use q\orm\db\Table;
use q\orm\db\table\column\where\Data;

/**
 * Class Where
 * Данный тип ячеек нужен для выполнения каких либо сложных условий для запроса
 * возможно с тяжёлыми подзапросами или для таких условий сравниваемые значения
 * которых не должны попасть в результатирующий набор
 *
 * @package q\orm\db\table\column
 */
class Where extends Real
{

	/**
	 * @var callable Функция для генерации условия
	 */
	protected $functionGetWhere;

	/**
	 * @var array Параметры для функции $functionGetExpression
	 */
	protected $arrayParameters;
	
	/**
	 * @var string Часть запроса для генерации условия
	 */
	protected $stringWhere;
	
	/**
	 * @return string Выражение для вычисления значения этого условия
	 */
	public function getExpression(): string{
		
		// Вычисляем выражение
		if($this->stringWhere === null){
			$this->stringWhere = '(' . ($this->functionGetWhere)(...$this->arrayParameters) . ')';
		}
		
		// Возвращаем выражение
		return $this->stringWhere;
	}

	public function getWhere($mixedValue): string{

		// Ключ по которому хранятся данные
		static $intDataKey = null;

		// Ключь ещё не искался
		if(null === $intDataKey){

			// По умолчанию считаем что данные не нужно передавать
			$intDataKey = false;

			// Находим параметр для передачи данных
			foreach($this->arrayParameters as $intIndex => $mixedParam){

				// Параметр данных найден
				if($mixedParam instanceof Data){

					// Запоминаем индекс
					$intDataKey = $intIndex;

					// Перестаём искать
					break;
				}
			}
		}

		// Данные нужно передать в функцию
		if($intDataKey !== false){

			// Берём стандартные параметры
			$arrayParameters = $this->arrayParameters;

			// Заменяем параметр с данными на сами данные
			$arrayParameters[$intDataKey] = new Data($mixedValue);

			// Выполняем калбек с параметроми
			return ($this->functionGetWhere)(...$arrayParameters);
		}

		// Данные не нужно передавать выполняем значения по умолчанию
		return parent::getWhere($mixedValue);

	}


	/**
	 * Where constructor.
	 *
	 * @param string $stringName
	 * @param callable $functionGetWhere
	 * @param Table $tableObject
	 * @throws \ReflectionException
	 * @throws \LogicException
	 */
	public function __construct(string $stringName, callable $functionGetWhere, Table $tableObject){
		parent::__construct($stringName, $tableObject);
		$this->functionGetWhere = $functionGetWhere;
		
		// Разбираем функцию для проверки
		$reflectionFunction = new \ReflectionFunction($functionGetWhere);
		$reflectionParameters = $reflectionFunction->getParameters();
		
		// Она должна возвращать строку
		if((string)$reflectionFunction->getReturnType() !== 'string'){
			throw new \LogicException('Function generates an expression does not return a string');
		}

		/** @var \ReflectionClass[] $arrayReflectionClass */
		$arrayReflectionClass = [
			new \ReflectionClass(Orm::class),
			new \ReflectionClass(Db::class),
			new \ReflectionClass(Table::class),
			new \ReflectionClass(Data::class),
		];

		// Параметры для подстановки к типам из $arrayReflectionClass
		$arrayParameters = [
			$this->getOrm(),
			$this->getTable(),
			$this->getDb(),
			new Data(null),
		];

		// Проверяем типы аргументов
		foreach($reflectionParameters as $reflectionParameter){

			// Разрешол ли данный тип переменной
			$boolIsPermittedType = false;

			// Сверяем со списком разрешонных типов
			foreach ($arrayReflectionClass as $intIndexClass => $reflectionClass){
				if(
					// Если является этим классои
					$reflectionParameter->getClass()->getName() === $reflectionClass->getName() ||
					// Или наследуется от него
					in_array($reflectionClass->getName(), class_parents($reflectionParameter->getClass()->getName()), true)
				){

					// Указываем что текущий параметр разрешон
					$boolIsPermittedType = true;

					// Добавляем параметр
					$this->arrayParameters[]  = $arrayParameters[$intIndexClass];

					// Прекращаем поиск
					break;
				}
			}

			// Тип параметра не разрешон
			if(!$boolIsPermittedType){
				throw new \LogicException("Type {$reflectionParameter->getType()} not permitted. Use " . Orm::class .
					' or ' . Table::class);
			}
		}

		// Записываем функцию
		$this->functionGetWhere = $functionGetWhere;
	}
}