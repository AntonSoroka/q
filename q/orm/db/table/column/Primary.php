<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.05.2017
 * Time: 12:06
 */

namespace q\orm\db\table\column;


class Primary extends Cell
{
	/**
	 * @var bool Указывает что это автоинкрементное поле
	 */
	protected $boolAutoIncrement = false;

	/**
	 * Указать является ли поле полем с автоинкрементом
	 *
	 * @return static
	 */
	public function thisAutoIncrement(){

		// Если ячейка назначается ячейкой с автоматически увеличивающемся значением то она должна:
		// 1) Быть единственной подобной ячейкой
		// 2) Иметь тип Integer

		// В таблице не должно быть двух ячеек с автоинкрементным значением
		foreach($this->getTable()->getAllColumn() as $objectCell){
			if($objectCell instanceof self and $objectCell->isAutoIncrement()){
				throw new \LogicException('You can only have one auto-increment per table');
			}
		}

		// TODO: Проверить тип ячейки, должна иметь тип int


		$this->boolAutoIncrement = true;
		return $this;
	}

	/**
	 * Узнать является ли это поле полем с автоинкрементом
	 *
	 * @return bool
	 */
	public function isAutoIncrement():bool{
		return $this->boolAutoIncrement;
	}
}