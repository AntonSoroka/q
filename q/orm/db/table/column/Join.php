<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.04.2017
 * Time: 8:41
 */

namespace q\orm\db\table\column;


use q\orm\db\Table;
use q\orm\db\table\Column;

/**
 * Class Join
 * Это не настоящая ячейка она указывает на связь какой либо ячейки из текущей таблицы
 * на ячейку другой таблицы а может даже этой. В имя этой ячейки помещается массив значений
 * из таблицы связанной ячейки
 *
 * @package q\orm\db\table\column
 */
class Join extends Column
{
	/**
	 * @var Returned Ячейка из этой таблицы
	 */
	protected $returnedLink;

	/**
	 * @return Returned ячейка этой таблицы
	 */
	public function getLink():Returned{
		return $this->returnedLink;
	}

	/**
	 * @var Returned Ячейка на которую ссылается эта ссылка
	 */
	protected $returnedTarget;

	/**
	 * @return Returned Ячейка на которую ссылаются
	 */
	public function getTarget():Returned{
		return $this->returnedTarget;
	}

	/**
	 * Join constructor.
	 *
	 * @param string $stringName Имя колонки
	 * @param Table $objectTable К какой таблице принадлежит колонка
	 * @param string $stringCellLinkName
	 * @param Returned $returnedTarget
	 */
	public function __construct($stringName, Table $objectTable, string $stringCellLinkName, Returned $returnedTarget){

		// Родительский конструктор
		parent::__construct($stringName, $objectTable);

		// Записываем ячейку текущей таблицы
		$this->returnedLink = $this->getTable()->getColumnReturned($stringCellLinkName);

		// Записываем ячейку на которую ссылаемся
		$this->returnedTarget = $returnedTarget;
	}
}