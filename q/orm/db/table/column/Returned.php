<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.04.2017
 * Time: 22:19
 */

namespace q\orm\db\table\column;

use q\orm\db\Table;
use q\orm\db\table\Type;

/**
 * Class Returned
 * Колонки которые попадают в результатирующий набор из целевой таблицы
 *
 * @package q\orm\db\table\column
 */
abstract class Returned extends Real{
	
	/**
	 * Возвращает строку генерирующую значение и альяс для неё
	 *
	 * @param string|null $stringTempTable
	 * @return string
	 */
	protected $stringExpression = null;
	protected $stringLastTempTable = null;
	protected $stringLastExpression = null;

	public function getAs(string $stringTempTable = null):string{

		if(null === $stringTempTable){
			if($this->stringExpression === null){
				$this->stringExpression = $this->getExpression() . ' AS ' . $this->getName();
			}

			return $this->stringExpression;
		}

		if($this->stringLastTempTable === $stringTempTable){
			return $this->stringLastExpression;
		}

		$this->stringLastTempTable = $stringTempTable;
		$this->stringLastExpression = \q\sql\escapeName($stringTempTable) . '.' . $this->getName() . ' AS ' . $this->getName();

		return $this->stringLastExpression;

	}

	/**
	 * @var null
	 */
	protected $stringFullName = null;
	
	/**
	 * Возвращает полное имя ячейки с именем базы и таблицы
	 *
	 * @return string Полное имя ячейки с названием таблицы и базы
	 */
	final public function getFullName():string{

		if($this->stringFullName === null){
			$this->stringFullName = $this->objectTable->getFullName() . '.' . $this->getName();
		}

		return $this->stringFullName;
	}
	
	/**
	 * Returned constructor.
	 *
	 * @param string $stringName
	 * @param Type $typeObject
	 * @param Table $tableObject
	 */
	public function __construct($stringName, Type $typeObject, Table $tableObject){
		parent::__construct($stringName, $tableObject);
		$this->setType($typeObject);
	}
	
	/**
	 * Установить тип возвращаемой колонки
	 *
	 * @param Type $typeObject
	 * @return static
	 */
	public function setType(Type $typeObject){
		$typeObject->writeReturned($this);
		return $this;
	}

	/**
	 * @param Type $objectType
	 * @throws \OverflowException
	 */
	public function writeType(Type $objectType){
		
		// Тип уже объявлен
		if(null !== $this->objectType){
			throw new \OverflowException('Returned column type already set');
		}
		
		// Записываем тип
		$this->objectType = $objectType;
	}

	abstract public function canBeNull():bool;
}