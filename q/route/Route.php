<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 15.03.2017
 * Time: 12:41
 */

namespace q\utils;


/**
 * Роутинг вызовов по регулярным выражениям
 *
 * @package q\utils
 */
class Route
{


	/**
	 * @var array Двумерный массив. Первое измерение методы, второе регулярные выражения, значение это калбек функции
	 */
	private $arrayPath = [];

	/**
	 * Добавление нового регулярного выражения для поиска решения для пути и калбека для его выполнения
	 *
	 * @param string|array $mixedMethod Методы/метод
	 * @param string $stringRegularPath Регулярное выражение для поиска
	 * @param callable $functionHandle Обработчик
	 */
	public function add($mixedMethod, string $stringRegularPath, callable $functionHandle)
	{
		// Если методы это массив то добавляем обработчик для каждого метода
		if(is_array($mixedMethod)){
			foreach($mixedMethod as $stringMethod){
				$this->add($stringMethod, $stringRegularPath, $functionHandle);
			}
			return;
		}

		// Добавляем обработчик для пути
		$stringMethod = mb_strtoupper((string)$mixedMethod);
		$this->arrayPath[$stringMethod][$stringRegularPath] = $functionHandle;

	}


	public $lastError;

	/**
	 * Поиск обработчика для заданного метода и пути
	 *
	 * @param string $stringMethod Метод
	 * @param string $stringPath Путь
	 * @return null|mixed Возвращает null решение не найдено или результат выполнения функции
	 * @throws \Exception
	 */
	public function find(string $stringMethod, string $stringPath){

		// Все буквы названия метода в верхний регистр
		$stringMethod = mb_strtoupper($stringMethod);

		// Если метода нет возвращаем null
		if(!isset($this->arrayPath[$stringMethod])){
			$this->lastError = 2;
			return null;
		}

		// Ищем метод
		foreach($this->arrayPath[$stringMethod] as $regularPath => $functionHandle){

			// Найденые в строке переменные
			$arrayMatch = [];

			// Пытаемся сопоставить текущий путь с зарегистрированным
			$boolMatch = preg_match($regularPath, $stringPath, $arrayMatch);

			// Если текущий вариант не совпал, то продолжаем поиск
			if(!$boolMatch){
				continue;
			}

			// Параметры для вызова калбека
			$arrayParameterList = [];

			// Получаем список параметров
			$reflectionFunction = new \ReflectionFunction($functionHandle);
			$arrayReflectionParameter = $reflectionFunction->getParameters();

			// Пытаемся найти все параметры в строке
			foreach($arrayReflectionParameter as $reflectionParameter){

				// Если переменная не найдена
				if(!isset($arrayMatch[$reflectionParameter->getName()])){

					// Если есть значение по умолчанию то передаём его
					if($reflectionParameter->isDefaultValueAvailable()){
						$arrayParameterList[] = $reflectionParameter->getDefaultValue();
					}

					// Если параметр можно установить в null то делаем это
					else if($reflectionParameter->allowsNull()){
						$arrayParameterList[] = null;
					}

					// Переменную для обязательного параметра найти не удалось
					else{
						throw new \Exception('Not set required parameter ' . $reflectionParameter->getName());
					}

				}

				// Добавляем найденую переменную к параметрам
				else{
					$arrayParameterList[] = $arrayMatch[$reflectionParameter->getName()];
				}
			}

			$this->lastError = null;

			// Выполняем функцию обработчик и возвращаем результат
			return $functionHandle(...$arrayParameterList);
		}

		$this->lastError = 2;
		return null;
	}
}