<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.08.2017
 * Time: 10:42
 */

namespace q\net;


use q\header\Item;

class Header{

	/**
	 * @var Item[]
	 */
	protected $arrayHeaderItem = [];

	/**
	 * Добавить заголовок
	 *
	 * @param Item $headerItemNew
	 * @param bool $boolOverwrite
	 */
	public function addHeaderItem(Item $headerItemNew, bool $boolOverwrite = true){


		// Нужно переписать старый заголовок
		if($boolOverwrite){

			// Удаляем заголовки с тем же именем что и новый заголовок
			$this->arrayHeaderItem = \array_filter($this->arrayHeaderItem, function(Item $headerItem) use (&$headerItemNew){
				return $headerItem->getName() !== $headerItemNew->getName();
			});
		}

		// Добавляем заголовок
		$this->arrayHeaderItem[] = $headerItemNew;
	}

	/**
	 * Добавить заголовок
	 *
	 * @param string $stringName
	 * @param string $stringValue
	 * @param bool $boolOverwrite
	 */
	public function add(string $stringName, string $stringValue, bool $boolOverwrite = true){

		// Создаём заголовок
		$headerItem = new Item($stringName, $stringValue);

		// Добавляем заголовок
		$this->addHeaderItem($headerItem, $boolOverwrite);
	}

	/**
	 * Добавить заголовки
	 * @param string[] $arrayHeader
	 */
	public function addStringHeader(string ...$arrayHeader){

		// Перебираем заголовки
		foreach($arrayHeader as $stringHeader){

			// Это заголовок статуса ответа
			if(preg_match('#^HTTP/(1.[10]|2.0) (\d\d\d) .*$#usi', $stringHeader)){
				continue;
			}

			$this->addHeaderItem(Item::createFromString($stringHeader), false);
		}
	}

	/**
	 * Возвращает заголовки
	 *
	 * @param string|null $stringName Имя заголовков или null если вернуть нужно все
	 * @return Item[]
	 */
	public function getHeaders(string $stringName = null):array{

		// Имя не задано
		if(null === $stringName){

			// Возвращаем все заголовки
			return $this->arrayHeaderItem;
		}

		// Фильтруем заголовки
		return \array_filter($this->arrayHeaderItem, function(Item $headerItem) use (&$stringName){

			// Возвращаем только те которые совпадают с переданным именем
			return $stringName === $headerItem->getName();
		});
	}

	/**
	 * Преобразование заголовков в массив строк для запроса
	 * @param string|null $stringHeaderName
	 * @return array
	 */
	public function getHeaderString(string $stringHeaderName = null):array{
		$arrayHeaderItem = $this->getHeaders($stringHeaderName);
		return \array_map(function(Item $headerItem){
			return (string)$headerItem;
		}, $arrayHeaderItem);
	}

	/**
	 * Возвращает заголовки как строку
	 */
	public function __toString():string{

		// Склеиваем заголовки и возвращаем их
		return implode("\r\n", array_map(function(Item $headerItem){

			// Преобразовываем заголовок в строку
			return (string)$headerItem;

		}, $this->arrayHeaderItem));

	}

}