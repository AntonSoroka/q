<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.08.2017
 * Time: 13:17
 */

namespace q\net;

function portDefaultScheme(int $intPort):string{
	switch($intPort){
		case 80: return 'http';
		case 443: return 'https';
		case 21: return 'ftp';
		case 115: return 'sftp';
		default: throw new \LogicException('Bad port');
	}
}