<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 08.08.2017
 * Time: 8:28
 */

namespace q\net;


use q\net\cookie\Item;

class Cookie implements \JsonSerializable{

	/**
	 * @var bool Указывает нужно ли показывать исключения при создании кук
	 */
	public $boolCreateException = false;


	/**
	 * @var Item[]
	 */
	protected $arrayCookieItem = [];

	/**
	 * Добавляем/обновляем кукуи
	 * @param Item[] $arrayCookieItem
	 */
	public function set(Item ...$arrayCookieItem){

		// Перебираем переданные куки
		foreach($arrayCookieItem as $cookieItem){

			// Указывает что кука уже заменена
			$boolReplace = false;

			// Проверяем есть ли такая кука в списке
			foreach($this->arrayCookieItem as $intIndex => $cookieItemCurrent){

				// Нашли одинаковую куку
				if(Item::isEquals($cookieItemCurrent, $cookieItem)){

					// Заменяем более новой
					$this->arrayCookieItem[$intIndex] = Item::getNew($cookieItemCurrent, $cookieItem);

					// Указываем что заменили куку
					$boolReplace = true;
				}
			}

			// Старую куку не заменяли
			if(!$boolReplace){

				// Добавляем новую куку
				$this->arrayCookieItem[] = $cookieItem;
			}

		}
	}

	/**
	 * Добавляет новую куку из заголовков ответа
	 *
	 * @param string $stringUrl
	 * @param string[] ...$arraySetCookieHeader
	 */
	public function setFromSetCookieHeader(string $stringUrl, string ...$arraySetCookieHeader){

		/** var Item[] */
		$arrayCookieItem = [];

		// Перебираем куки
		foreach($arraySetCookieHeader as $stringSetCookieHeader){

			// Если произойдёт ошибка во время создания куки связанная с безопасностью,
			// то такую ошибку можно просто пропустить мимо ушей, как это делают браузеры
			// так будет проще для пользователя библиотеки
			if(!$this->boolCreateException){

				// Ожидаем ошибок связанных с секюрностью
				try{

					// Создаём куку
					$cookieItem = Item::createFromSetCookieHeader($stringUrl, $stringSetCookieHeader);

					// Добавляем куку
					$arrayCookieItem[] = $cookieItem;
				}

				// Не обращаем внимания на несекюрные куки
				catch(\DomainException $e){}

			}

			// Не пропускаем ошибки секюрности
			else{

				// Создаём и добавляем куку
				$arrayCookieItem[] = Item::createFromSetCookieHeader($stringUrl, $stringSetCookieHeader);
			}


		}

		// Добавляем куки
		$this->set(...$arrayCookieItem);
	}

	/**
	 * Удаляет старые куки
	 */
	public function clearOld(){

		// Удаляем старые куки
		$this->arrayCookieItem = \array_filter($this->arrayCookieItem, function(Item $cookieItem){
			return !$cookieItem->isOld();
		});
	}


	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize(){

		// Массив преобразованных кук
		$arrayResult = [];

		// Перебираем куки
		foreach($this->arrayCookieItem as $cookieItem){

			// Преобразуем куку
			$arrayResult[] = $cookieItem->jsonSerialize();
		}

		return $arrayResult;
	}

	/**
	 * Преобразуем массив куки в объект
	 *
	 * @param array $arrayResult
	 * @return Cookie
	 */
	public static function jsonUnserialize(array $arrayResult):Cookie{

		// Создаём хранилище
		$cookie = new static();

		$intMaxId = 0;

		// Перебираем JSON куки
		foreach($arrayResult as $arrayHeaderItem){

			$cookieItem = Item::jsonUnserialize($arrayHeaderItem);

			$intMaxId = max($intMaxId, $cookieItem->getId());

			// Преобразуем в объекты и добавляем в набор куков
			$cookie->set($cookieItem);
		}

		Item::$intIdGenerate = $intMaxId;

		// Возвращаем куки
		return $cookie;
	}

	/**
	 * Отдаёт куки для нужного url
	 *
	 * @param string $stringUrl
	 * @return string
	 */
	public function get(string $stringUrl):string{


		// Отчищаем старые
		$this->clearOld();

		// Куки для этого URL
		/** Item[][] */
		$arrayCookieItemCurrent = [];


		// Перебираем все куки
		foreach($this->arrayCookieItem as $cookieItem){
			if($cookieItem->isAllowedUrl($stringUrl)){

				// Если такой куки ещё нет то определяем для неё место
				if(!isset($arrayCookieItemCurrent[$cookieItem->getName()])){
					$arrayCookieItemCurrent[$cookieItem->getName()] = [];
				}

				// Добавляем куку
				$arrayCookieItemCurrent[$cookieItem->getName()][] = $cookieItem;
			}
		}

		// Выбираем самые новые
		foreach($arrayCookieItemCurrent as $stringName => $arrayCookieItem){

			// Выбираем новые и преобразуем в строки
			$arrayCookieItemCurrent[$stringName] = (string)Item::getNew(...$arrayCookieItem);
		}

		// Обхединяем куки
		$stringCookieHeader = \implode('; ', $arrayCookieItemCurrent);

		// Отдаём куку
		return $stringCookieHeader;
	}

	/**
	 * Массив заголовков для установки кук на указвнный URL
	 *
	 * @param string $stringUrl
	 * @return array
	 */
	public function getFromSet(string $stringUrl):array{


		// Отчищаем старые
		$this->clearOld();

		// Куки для этого URL
		/** Item[][] */
		$arrayCookieItemCurrent = [];


		// Перебираем все куки
		foreach($this->arrayCookieItem as $cookieItem){
			if(null === $stringUrl || $cookieItem->isAllowedUrl($stringUrl)){


				// Если такой куки ещё нет то определяем для неё место
				if(!isset($arrayCookieItemCurrent[$cookieItem->getName()])){
					$arrayCookieItemCurrent[$cookieItem->getName()] = [];
				}

				// Добавляем куку
				$arrayCookieItemCurrent[$cookieItem->getName()][] = $cookieItem;
			}
		}

		// Выбираем самые новые
		foreach($arrayCookieItemCurrent as $stringName => $arrayCookieItem){

			// Выбираем новые и преобразуем в строки
			$arrayCookieItemCurrent[$stringName] = 'Set-Cookie: ' . Item::getNew(...$arrayCookieItem)->getFromSet();
		}

		// Отдаём куку
		return $arrayCookieItemCurrent;
	}

	/**
	 * Все имеющиеся на данный момент куки
	 * @return array
	 */
	public function getAllFromSet():array{


		// Отчищаем старые
		$this->clearOld();

		// Куки для этого URL
		$arrayCookieSet = [];


		// Перебираем все куки
		foreach($this->arrayCookieItem as $cookieItem){
			// Добавляем куку
			$arrayCookieSet[] = 'Set-Cookie: ' . $cookieItem->getFromSet();
		}

		// Отдаём куку
		return $arrayCookieSet;
	}
}