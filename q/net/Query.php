<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.08.2017
 * Time: 13:12
 */

namespace q\net;


class Query{

	/**
	 * @var Header
	 */
	protected $header;

	/**
	 * @var Cookie
	 */
	protected $cookie;

	/**
	 * Query constructor.
	 * @param Header|null $header
	 * @param Cookie|null $cookie
	 */
	public function __construct(Header $header = null, Cookie $cookie = null){
		$this->header = $header ?? new Header();
		$this->cookie = $cookie ?? new Cookie();
	}

	/**
	 * @return Cookie Куки
	 */
	public function getCookie():Cookie{
		return $this->cookie;
	}

	/**
	 * @return Header Заголовки
	 */
	public function getHeader():Header{
		return $this->header;
	}


	/**
	 * @var int Код ответа
	 */
	public $intResponseCode = 0;

	/**
	 * @var string Последний полученный url
	 */
	public $stringLastUrl = '';

	/**
	 * Выполнить запрос
	 *
	 * @param string $stringUrl
	 * @param string $stringMethod
	 * @param array $arrayData
	 * @param int $intLimitRedirect
	 * @return bool|string
	 */
	public function get(string $stringUrl, string $stringMethod = 'GET', array $arrayData = [], int $intLimitRedirect = 10){

		// Параметры запросов
		$arrayContextParams = [
			'http' => [

				// Метод запроса
				'method' => $stringMethod,

				// Запрещаем автоматический редирект, будем делать его вручную
				'follow_location' => false,
			],
		];

		// Метод POST
		if($stringMethod === 'POST'){

			// Добавляем параметры
			$arrayContextParams['http']['content'] = http_build_query($arrayData);
		}

		// Записываем куки
		$this->header->add('Cookie', $this->cookie->get($stringUrl));

		// Заголовки запроса
		$arrayContextParams['http']['header'] = (string)$this->header;

		// Создаём контекст
		$context = stream_context_create($arrayContextParams);

		// Записываем последний url который запросили
		$this->stringLastUrl = $stringUrl;

		// Запрашиваем данные
		$stringMessage = file_get_contents($stringUrl, false, $context);

		// Забираем код ответа
		$this->intResponseCode = (int)preg_replace('#HTTP/(1\.0|1\.1|2\.0) (\d\d\d).*$#usi', '$2', $http_response_header[0]);

		// Заголовки ответа
		$headerResponse = new Header();
		$headerResponse->addStringHeader(...$http_response_header);

		// Добавляем новые куки
		$this->cookie->setFromSetCookieHeader($stringUrl, ...$headerResponse->getHeaderString('Set-Cookie'));

		// Определяем нужно ли перенаправление
		$arrayHeaderItem = $headerResponse->getHeaders('Location');
		$headerItemLocation = current($arrayHeaderItem);

		if($headerItemLocation && $this->intResponseCode >= 300 && $this->intResponseCode < 400){

			// Вынимаем ссылку на которую нас перенаправили
			$stringUrlNew = $headerItemLocation->getValue();


			// Заголовок пустой
			if($stringUrlNew === ''){
				return $stringMessage;
			}

			// Относительная ссылка
			if(!preg_match('#^((https?|ftps?)\:\/\/|\/\/)#usi', $stringUrlNew)){

				// Информация о старом URL
				$arrayUrlInfo = \q\net\urlParse($stringUrl);

				// Если нет пути но запишем в него пустую строку
				if(!isset($arrayUrlInfo['path'])){
					$arrayUrlInfo['path'] = '';
				}

				// Первый символ
				$stringFirstChar = $stringUrlNew[0];

				// Передан якорь
				if($stringFirstChar === '#'){

					// Обновляем якорь
					$arrayUrlInfo['fragment'] = substr($stringUrlNew, 1);
				}

				// Переданы параметры запроса
				else if($stringFirstChar === '?'){

					// Удаляем якорь
					unset($arrayUrlInfo['fragment']);

					// Обновляем параметры запроса
					$arrayUrlInfo['query'] = substr($stringUrlNew, 1);
				}
				else{
					// Удаляем якорь
					unset($arrayUrlInfo['fragment']);

					// Удаляем параметры запроса
					unset($arrayUrlInfo['query']);

					// Обрезается весь путь
					if($stringFirstChar === '/'){

						// Обновляем путь
						$arrayUrlInfo['path'] = $stringUrlNew;
					}

					// Относительно текущего пути
					else{

						// Нужно убрать имя
						if($arrayUrlInfo['path'] !== '/'){
							$arrayUrlInfo['path'] = basename($arrayUrlInfo['path']);
						}

						// Записываем путь
						$arrayUrlInfo['path'] .= $stringUrlNew;
					}

				}

				// Генерируем ссылку из информации о старой
				$stringUrlNew = \q\net\urlUnparse($arrayUrlInfo);
			}

			// Лимит редиректов исчерпан
			if($intLimitRedirect <= 0){
				return $stringMessage;
			}

			// Уменьшаем лимит редирктов
			$intLimitRedirect++;

			// Запрашиваем заново
			return $this->get($stringUrlNew, 'GET', $arrayData, $intLimitRedirect);
		}

		return $stringMessage;
	}
}