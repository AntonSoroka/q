<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.08.2017
 * Time: 10:42
 */

namespace q\header;


class Item{

	/**
	 * @var string
	 */
	protected $stringName;

	/**
	 * @var string
	 */
	protected $stringValue;

	/**
	 * Item constructor.
	 * @param string $stringName
	 * @param string $stringValue
	 * @throws \LogicException
	 */
	public function __construct(string $stringName, string $stringValue){

		// Убираем лишние пробелы в начале и в конце
		$stringName = rtrim($stringName);
		$stringValue = trim($stringValue);

		// Проверяем нет ли лишних знаков
		if(!preg_match('#^[\x21-\x39\x3B-\x7E]+$#usi', $stringName)){

			// Нашлись лишние знаки
			throw new \LogicException('Header name contains error chars. Only ansi 33-126 codes chars.');
		}

		// Удаляем пробелы после перевода строки
		$stringValue = preg_replace('#\n[ \t]+#usi', "\n", $stringValue);

		// Удаляем двойные пробелы
		$stringValue = preg_replace('#[ \t]+#usi', ' ', $stringValue);

		// Присваиваем значение
		$this->stringName = $stringName;
		$this->stringValue = $stringValue;
	}

	/**
	 * Регулярка для проверки заголовка на валидность
	 */
	const REGULAR_HEADER = '#^(?<name>[\x21-\x39\x3B-\x7E]+)\s*:\s*(?<value>(?:[^\n\r]*(?:\r\n|\r|\n)[\t ]+)*(?:[^\n\r]+))$#usi';

	/**
	 * Создать заголовок из строки
	 *
	 * @param string $stringHeader
	 * @return Item
	 * @throws \LogicException
	 */
	public static function createFromString(string $stringHeader):Item{

		// Массив для заполнения совпадениями
		$arrayMatches = [];

		// Проверяем правильность куки
		$boolMatch = preg_match(self::REGULAR_HEADER, $stringHeader, $arrayMatches);

		// Не совпало с регуляркой
		if(!$boolMatch){
			throw new \LogicException("Header not valid \"{$stringHeader}\"");
		}

		// Создаём заголовок и возвращаем его
		return new Item($arrayMatches['name'], $arrayMatches['value']);
	}

	/**
	 * Значение переменной
	 *
	 * @return string
	 */
	public function getValue():string{
		return $this->stringValue;
	}

	/**
	 * Имя заголовка
	 *
	 * @return string
	 */
	public function getName():string {
		return $this->stringName;
	}

	/**
	 * Преобразование в строку
	 *
	 * @return string
	 */
	public function __toString(){

		// Добавляем пробел после перевода строки, так нужно по стандарту для многострочных значений
		$stringValue = preg_replace('#\n#usi', "\n ", $this->stringValue);

		// Соединяем ключ и значение
		return $this->stringName . ': ' . $stringValue;
	}

}