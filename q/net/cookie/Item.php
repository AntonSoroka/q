<?php

/**
 * Created by PhpStorm.
 * User: anton
 * Date: 08.08.2017
 * Time: 8:29
 */

namespace q\net\cookie;


class Item implements \JsonSerializable{

	/**
	 * @var string Имя куки
	 */
	protected $stringName;

	/**
	 * @var string Значение куки
	 */
	protected $stringValue;

	/**
	 * @var int|null Время когда кука истечёт
	 */
	protected $intExpires;

	/**
	 * @var int|null Максимальное время жизни
	 */
	protected $intMaxAge;

	/**
	 * @var float Время создания куки
	 */
	protected $floatDateCreate;

	/**
	 * @var string|null Путь по которому действует кука
	 */
	protected $stringPath;

	/**
	 * @var string Домен которому принадлежит кука
	 */
	protected $stringDomain;

	/**
	 * @var string Домен перевёрнутый наоборот
	 */
	protected $stringDomainReverse;

	/**
	 * @var bool Только для http
	 */
	protected $boolHttpOnly = false;

	/**
	 * @var bool Передавать только по https
	 */
	protected $boolSecure = false;

	/**
	 * @var int
	 */
	protected $intId;


	/**
	 * Item constructor.
	 * @param string $stringName Имя куки
	 * @param string $stringValue Значение куки
	 * @param string $stringUrl
	 * @param string $stringDomain Url на котором была установлена кука
	 * @param string $stringPath
	 */
	public function __construct(
		string $stringName,
		string $stringValue,
		string $stringUrl,
		string $stringDomain = null,
		string $stringPath = null
	){
		$this->intId = static::generateId();
		$this->floatDateCreate = microtime(true);
		$this->stringName = $stringName;
		$this->stringValue = $stringValue;

		// Информация о URL
		$arrayUrlInfo = parse_url($stringUrl);


		// Домен не указан
		if(null === $stringDomain){

			// Извлекаем домен
			$stringDomain = $arrayUrlInfo['host'];
		}

		// Удаляем точки в начале домена
		$stringDomain = ltrim($stringDomain, '.');

		// Указываем домен и путь
		$this->stringDomain = $stringDomain;
		$this->stringPath = $stringPath;


		// Неразрешено ставить куки подкаталогам
		if(!$this->isAllowedPath($arrayUrlInfo['path'] ?? null, $this->stringPath)){

			// Генерируем исключение
			throw new \DomainException (
				'The value for the Path attribute is not a prefix of the request-URI. ' .
				'https://tools.ietf.org/html/rfc2109#section-4.3.2'
			);
		}


		// Проверяем домен
		if(
			// Это не localhost
			$stringDomain !== 'localhost' &&

			// Домен не содержит точку
			strpos($stringDomain, '.', 1) !== false &&

			// Нельзя ставить куки доменам своего родительского домена
			!$this->isAllowedDomain($arrayUrlInfo['host'], $this->stringDomain)
		){

			// Генерируем исключение
			throw new \DomainException (
				'The request-host is a FQDN (not IP address) and has the form HD, ' .
				'where D is the value of the Domain attribute, and H is a string ' .
				'that contains one or more dots. ' .
				'https://tools.ietf.org/html/rfc2109#section-4.3.2'
			);
		}
	}

	/**
	 * var int ID для генератора
	 */
	public static $intIdGenerate = 0;

	/**
	 * @return int Генерирует уникальный id
	 */
	private static function generateId():int{
		static::$intIdGenerate += 1;
		return static::$intIdGenerate;
	}

	/**
	 * @return int Возвращает id
	 */
	public function getId():int{
		return $this->intId;
	}

	/**
	 * @return int Дата создания куки
	 */
	public function getDateCreate():int{
		return $this->floatDateCreate;
	}

	/**
	 * @param int|null $intExpires Дата истечения куки
	 */
	public function setExpires(int $intExpires = null){
		$this->intExpires = $intExpires;
	}

	/**
	 * @param int|null $intMaxAge Максимальное время жизни куки
	 */
	public function setMaxAge(int $intMaxAge = null){
		$this->intMaxAge = $intMaxAge;
	}

	/**
	 * @param bool $boolHttpOnly Указывает что куку нельзя увидить в JS
	 */
	public function setHttpOnly(bool $boolHttpOnly){
		$this->boolHttpOnly = $boolHttpOnly;
	}

	/**
	 * @param bool $boolSecure Указывает что куку нужно передавать только по https соединению
	 */
	public function setSecure(bool $boolSecure){
		$this->boolSecure = $boolSecure;
	}

	/**
	 * @param string $stringValue Новое значение
	 */
	public function setValue(string $stringValue){
		$this->stringValue = $stringValue;
	}

	/**
	 * @return string Имя куки
	 */
	public function getName():string{
		return $this->stringName;
	}

	/**
	 * Не является ли кука устаревшей
	 * @return bool
	 */
	public function isOld():bool{

		// Наступило время истечения куки
		if(null !== $this->intExpires && $this->intExpires < time()){
			return true;
		}

		// Кука прожила отведённое ей время
		if(null !== $this->intMaxAge && $this->intMaxAge < time() - $this->floatDateCreate){
			return true;
		}

		// Кука всё ещё актуальна
		return false;
	}

	/**
	 * Переворачивает домен задом наперёд
	 * @param string $stringDomain
	 * @return string
	 */
	final public static function reverseDomain(string $stringDomain):string{

		// Разбиваем домен на части по точке (.)
		$arrayDomainReverse = explode('.', $stringDomain);

		// Если первая часть пуста, то это сделано для старых браузеров
		if($arrayDomainReverse[0] === ''){

			// Убираем её
			array_shift($arrayDomainReverse);
		}

		// Переворачиваем доменное имя
		$arrayDomainReverse = array_reverse($arrayDomainReverse);

		// Соединяем точкой (.)
		$stringDomainReverse = \implode('.', $arrayDomainReverse);

		// Возвращаем домен
		return $stringDomainReverse;
	}

	/**
	 * Проверяет разрешено ли отправлять куки этому домену
	 *
	 * @param string $stringDomainUrl Домен который хочет забрать куку
	 * @param string|null $stringDomainCookie Домен куки
	 * @return bool
	 */
	public function isAllowedDomain(string $stringDomainUrl , string $stringDomainCookie = null):bool{

		// Берум локальный домен куки
		if(null === $stringDomainCookie){
			$stringDomainCookie = $this->stringDomain;
		}

		// Проверяем подходит домен или нет и отправляем
		return preg_match('#^([^\.]+\.|\.|)(' . preg_quote($stringDomainCookie, '#') . ')$#usi', $stringDomainUrl);
	}

	/**
	 * Проверяем нужно ли отдавать куку url адресу с данным путём
	 *
	 * @param string|null $stringPathUrl Путь в URL
	 * @param string|null $stringPathCookie Путь разрещённый для этой куки
	 * @return bool
	 */
	public function isAllowedPath(string $stringPathUrl = null, string $stringPathCookie = null):bool{

		// Берём локальный путь
		if(null === $stringPathCookie){
			$stringPathCookie = $this->stringPath;
		}

		// Если путь куки пустой то подходит любой путь из url
		if(in_array($stringPathCookie, [null, '', '/'], true)){
			return true;
		}

		// Путь URL начинается так же как путь из куки
		if(preg_match('#^' . preg_quote($stringPathCookie, '#') . '.*$#usi', $stringPathUrl )){
			return true;
		}

		// Путь не подходит
		return false;
	}

	/**
	 * Проверяет нужно ли отправлять эту куку по указаному url адресу
	 *
	 * @param string $stringUrl
	 * @return bool
	 */
	public function isAllowedUrl(string $stringUrl):bool{

		// Данные о ссылке
		$arrayUrlInfo = parse_url($stringUrl);

		// Этому домену нельзя забирать куку
		if(!$this->isAllowedDomain($arrayUrlInfo['host'])){
			return false;
		}

		// Этому пути нельзя забирать куку
		if(!$this->isAllowedPath($arrayUrlInfo['path'])){
			return false;
		}

		// Отправка разрешена
		return true;
	}


	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize(){
		return [
			0 => $this->stringName,
			1 => $this->stringValue,
			2 => $this->stringDomain,
			3 => $this->stringPath,
			4 => $this->floatDateCreate,
			5 => $this->intMaxAge,
			6 => $this->intExpires,
			7 => $this->boolSecure,
			8 => $this->boolHttpOnly,
			9 => $this->intId,
		];
	}

	/**
	 * Преобразует упакованую куку в объкект
	 * @param array $arrayCookie
	 * @return Item
	 */
	public static function jsonUnserialize(array $arrayCookie):Item{

		// Создаём объект
		$cookieItem = new Item(
			$arrayCookie[0],
			$arrayCookie[1],
			'http://' . $arrayCookie[2] . $arrayCookie[3],
			$arrayCookie[2],
			$arrayCookie[3]
		);

		// Проставляем данные
		$cookieItem->floatDateCreate = $arrayCookie[4];
		$cookieItem->intMaxAge = $arrayCookie[5];
		$cookieItem->intExpires = $arrayCookie[6];
		$cookieItem->boolSecure = $arrayCookie[7];
		$cookieItem->boolHttpOnly = $arrayCookie[8];
		$cookieItem->intId = $arrayCookie[9];

		// Возвращаем объект
		return $cookieItem;
	}

	/**
	 * Создание куки из заголовка Set-Cookie
	 * @param string $stringUrl
	 * @param string $stringSetCookieHeader
	 * @return Item
	 */
	public static function createFromSetCookieHeader(string $stringUrl, string $stringSetCookieHeader):Item{

		// Ищем и убираем сам заголовок
		$stringSetCookieHeader = preg_replace('#^\s*Set-Cookie:\s*#usi', '', $stringSetCookieHeader);

		// Разбиваем заголовок на части
		$arrayHeaderPartList = explode(';', $stringSetCookieHeader);

		// Забираем имя и значение куки
		$arrayNameValue = explode('=', array_shift($arrayHeaderPartList), 2);

		// Отчищаем от лишних пробелов по краям
		$stringName = trim($arrayNameValue[0]);
		$stringValue = trim($arrayNameValue[1]);

		// Свойства куки
		foreach($arrayHeaderPartList as $stringHeaderPart){

			// Записи формата "ключ=значение"
			if(strpos($stringHeaderPart, '=') !== false){

				// Разбиваем строку
				list($stringPartKey, $stringPartValue) = explode('=', $stringHeaderPart);

				// Убираем лишние пробелы по бокам из значения
				$stringPartValue = trim($stringPartValue);
			}
			// Записи указывающие на наличие свойства
			else{
				$stringPartKey = $stringHeaderPart;
				$stringPartValue = true;
			}

			// Убираем лишние пробелы по бокам из ключа
			$stringPartKey = trim($stringPartKey);

			// Определяем параметр
			switch(mb_strtolower($stringPartKey)){
				case 'expires':

					// Подбираем формат даты в куке
					foreach([
						\DateTime::COOKIE,
						\DateTime::RFC822,
						\DateTime::RFC850,
						\DateTime::RFC1036,
						\DateTime::RFC1123,
						\DateTime::RFC2822,
						\DateTime::RFC3339,
						\DateTime::ATOM,
						\DateTime::ISO8601,
						\DateTime::RSS,
						\DateTime::W3C,
					] as $stringFormat){

						// Пытаемся получить дату с текущим форматом
						$dtExpires = \DateTime::createFromFormat($stringFormat, $stringPartValue);

						// Формат подошёл дата распознана
						if($dtExpires){

							// Записываем дату
							$intExpires = $dtExpires->getTimestamp();

							// Заканчиваем поиск
							break;
						}
					}

					break;
				case 'max-age':
					$intMaxAge = (int)$stringPartValue;
					break;
				case 'domain':
					$stringDomain = $stringPartValue;
					break;
				case 'path':
					$stringPath = $stringPartValue;
					break;
				case 'secure':
					$boolSecure = true;
					break;
				case 'httponly':
					$boolHttpOnly = true;
					break;
			}

		}


		// Создаём куку
		$cookieItem = new Item($stringName, $stringValue, $stringUrl, $stringDomain ?? null, $stringPath ?? null);

		// Проставляем параметры
		$cookieItem->intExpires = $intExpires ?? null;
		$cookieItem->intMaxAge = $intMaxAge ?? null;
		$cookieItem->boolHttpOnly = $boolHttpOnly ?? false;
		$cookieItem->boolSecure = $boolSecure ?? false;

		// Возвращаем куку
		return $cookieItem;
	}

	/**
	 * Проверка являются ли куки идентичными
	 *
	 * @param Item $cookieItem1
	 * @param Item $cookieItem2
	 * @return bool
	 */
	public static function isEquals(Item $cookieItem1, Item $cookieItem2):bool{
		if(
			$cookieItem1->getId() === $cookieItem2->getId() || (
				$cookieItem1->stringName == $cookieItem2->stringName &&
				$cookieItem1->stringPath == $cookieItem2->stringPath &&
				$cookieItem1->stringDomain == $cookieItem2->stringDomain
			)
		){
			return true;
		}

		return false;
	}


	/**
	 * Находит самую новую куку из переданных в функцию
	 *
	 * @param Item[] ...$arrayCookieItem
	 * @return Item
	 * @throws \ArgumentCountError
	 */
	public static function getNew(Item ...$arrayCookieItem):Item{

		if(count($arrayCookieItem) === 0){
			throw new \ArgumentCountError('At least one argument');
		}

		/** @var Item $cookieItemNew */
		$cookieItemNew = null;

		foreach($arrayCookieItem as $cookieItem){
			if(
				// Это первая кука
				null === $cookieItemNew ||

				// Новее запомненой
				$cookieItem->floatDateCreate > $cookieItemNew->floatDateCreate ||

				(
					// Время создания равно
					$cookieItem->floatDateCreate === $cookieItemNew->floatDateCreate &&

					// И id больше
					$cookieItem->intId > $cookieItemNew->intId
				)
			){
				// Обновляем переменную с самой новой кукой
				$cookieItemNew = $cookieItem;
			}
		}

		// Отдаём самую новую куку
		return $cookieItemNew;
	}

	/**
	 * Переобразование в строку
	 * @return string
	 */
	public function __toString():string{
		return $this->stringName . '=' . $this->stringValue;
	}

	/**
	 * Строка для установки куки
	 * @return string
	 */
	public function getFromSet():string{
		return (
			$this->stringName . '=' . $this->stringValue .
			'; Domain=' . $this->stringDomain .
			(null === $this->stringPath ? '' : '; Path=' . $this->stringPath) .
			(null === $this->intExpires ? '' : '; Expires=' . date(\DateTime::COOKIE, $this->intExpires)) .
			(null === $this->intMaxAge ? '' : '; Max-Age=' . $this->intMaxAge)
		);
	}
}