<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.08.2017
 * Time: 15:50
 */

namespace q\net;

function urlParse(string $stringUrl):array{
	return parse_url($stringUrl);
}