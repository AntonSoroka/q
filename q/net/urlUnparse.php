<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.08.2017
 * Time: 15:51
 */

namespace q\net;

/**
 * Собирает массив данных в URL
 *
 * @param array $arrayUrlInfo
 * @return string
 */
function urlUnparse(array $arrayUrlInfo):string{

	$scheme   = isset($arrayUrlInfo['scheme']) ? $arrayUrlInfo['scheme'] . '://' : '';
	$host     = $arrayUrlInfo['host'] ?? '';
	$port     = isset($arrayUrlInfo['port']) ? ':' . $arrayUrlInfo['port'] : '';
	$user     = $arrayUrlInfo['user'] ?? '';
	$pass     = isset($arrayUrlInfo['pass']) ? ':' . $arrayUrlInfo['pass']  : '';
	$pass     = ($user || $pass) ? "$pass@" : '';
	$path     = $arrayUrlInfo['path'] ?? '';
	$query    = isset($arrayUrlInfo['query']) ? '?' . $arrayUrlInfo['query'] : '';
	$fragment = isset($arrayUrlInfo['fragment']) ? '#' . $arrayUrlInfo['fragment'] : '';

	return "$scheme$user$pass$host$port$path$query$fragment";
}