<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:34
 */
namespace q\html;

const DECODE = ['&',    '<',   '>',   '"',     "'",      '/',     ];
const ENCODE = ['&amp;','&lt;','&gt;','&quot;','&#x27;', '&#x2F;',];