<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.03.2017
 * Time: 13:27
 */

namespace q\html;

/**
 * Заменить HTML5 сущности в эх символьные представления
 *
 * @param string $stringText Строка с HTML5 сущностями
 * @return string
 */
function decode(string $stringText):string{
	return str_replace(\q\html\ENCODE, \q\html\DECODE, $stringText);
}