<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.03.2017
 * Time: 16:16
 */

namespace q\html;

/**
 * Экранизация теста и обрамления его текстом для 
 * передачи его в атрибуты HTML5 тегов
 * 
 * @param string $stringText Тест который нужно назначить атрибуту
 * @return string
 */
function attr(string $stringText):string{
	return '"' . \q\html\encode($stringText) . '"';
}