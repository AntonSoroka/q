<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.03.2017
 * Time: 13:16
 */

namespace q\html;

/**
 * Перобразование симвалов небезопасного текста в их соответсвующие HTML5 сущности
 *
 * @param string $stringText Небезопасный текст
 * @return string
 */
function encode(string $stringText):string{
	return str_replace(\q\html\DECODE, \q\html\ENCODE, $stringText);
}