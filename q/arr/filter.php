<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 09.03.2017
 * Time: 10:17
 */

namespace q\arr\filter;
	const CONTINUE_ELEMENT = [false, null, ''];

namespace q\arr;

/**
 * Отфильтровать массив вызвав для каждого элемента массива функцию
 * обработчик которая должна вернуть одно из значений из массива
 * \q\arr\filter\CONTINUE_ELEMENT для того что-бы элемент был отсеян
 * из массива
 *
 * @param array $arrayParts Массив который нужно отфильтровать
 * @param callable $functionHandlerPart Функция обработчик
 * @param bool $boolPrevAndNext Передавать значения предыдущих и следующих элементов в обработчик
 * @return array Отфильтрованный массив
 */
function filter(array $arrayParts, callable $functionHandlerPart, bool $boolPrevAndNext = false):array{

	// Применяем функцию к каждому элементу
	$arrayResultMap = \q\arr\map($arrayParts, $functionHandlerPart, $boolPrevAndNext);

	// Отсеиваем элементы которые есть в массиве \q\arr\filter\CONTINUE_ELEMENT
	return array_filter($arrayResultMap, function($mixedValue){
		return !in_array($mixedValue, \q\arr\filter\CONTINUE_ELEMENT, true);
	});

}

