<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 07.03.2017
 * Time: 16:02
 */

namespace q\arr;

/**
 * Альяс для \q\arr\implode с пустой строкой вместо первого аргумента
 *
 * @param array $arrayParts 
 * @param callable|null $functionHandlerPart
 * @param bool $booleanPrevAndNext
 * @return string
 */
function toString(
	array $arrayParts,
	callable $functionHandlerPart = null,
	bool $booleanPrevAndNext = false
){
	return \q\arr\implode('', $arrayParts, $functionHandlerPart, $booleanPrevAndNext);
}