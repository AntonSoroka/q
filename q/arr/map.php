<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 09.03.2017
 * Time: 10:20
 */


namespace q\arr;

/**
 * Обработка каждого элемента функции переданной функцией
 *
 * @param array $arrayParts
 * @param callable $functionHandlerPart
 * @param bool $boolPrevAndNext
 * @return array
 */
function map(array $arrayParts, callable $functionHandlerPart, bool $boolPrevAndNext = false):array{

	// Ключи
	$arrayKeys = array_keys($arrayParts);

	// Порядковый номера ячеек
	$arrayIndex = array_keys($arrayKeys);

	// Только 3 параметра. Значение, ключ и номер ячейки
	if($boolPrevAndNext === false){

		// Обчная обработка
		$arrayResult = array_map(
			$functionHandlerPart,
			$arrayParts,
			$arrayKeys,
			$arrayIndex
		);
	}

	// Передача дополнительных аргументов, предыдущего и следующего элемента
	else{

		// Следующие элементы
		$arrayNext = $arrayParts;
		$arrayNext = array_slice($arrayNext, 1);
		$arrayNext[] = null;

		// Предыдущие элементы
		$arrayPrev = $arrayParts;
		$arrayPrev = array_slice($arrayPrev, 0, -1);
		array_unshift($arrayPrev, null);

		// Обработка с предыдущим и следующим массивом
		$arrayResult = array_map(
			$functionHandlerPart,
			$arrayParts,
			$arrayKeys,
			$arrayIndex,
			$arrayPrev,
			$arrayNext
		);
	}

	return array_combine(array_keys($arrayParts), $arrayResult);
}