<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 07.03.2017
 * Time: 14:34
 */

namespace q\arr;

/**
 * Замена функции implode позваляет передвать функцию обработчик
 * и обрабатывать предыдущее и следующее значение
 *
 * @param string $stringConnector Строка которой нужно соединить элементы
 * @param array $arrayParts Массив аргументы которого нужно соединить
 * @param callable|null $functionHandlerPart Функция обработчик каждого элемента
 * @param bool $booleanPrevAndNext Передавать в предыдущее и следующее значение в функцию обработчик
 * @return string
 */
function implode(
	string $stringConnector,
	array $arrayParts,
	callable $functionHandlerPart = null,
	bool $booleanPrevAndNext = false
):string{

	// Функция обработчик не переданна
	if(null === $functionHandlerPart){
		// Выполняем встроеную функцию и отдаём результат
		return \implode($stringConnector, $arrayParts);
	}

	// Обработанные элементы
	$arrayStringResult = \q\arr\map($arrayParts, $functionHandlerPart,$booleanPrevAndNext);

	// Собираем массив
	return \implode($stringConnector, $arrayStringResult);
}