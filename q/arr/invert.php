<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.2017
 * Time: 15:51
 */
namespace q\arr;

/**
 * Перевернуть двумерный массив
 *
 * @param array|null $arrayIn
 * @return array
 * @throws \Exception
 */
function invert(array $arrayIn = null):array{

	// Если количество элементов равно 0 или передано значение null
	// То возвращаем пустой массив
	if(null === $arrayIn || count($arrayIn) === 0){
		return [];
	}

	// Первая строка
	$arrayFirstRow = current($arrayIn);

	// Если это не строка, то генерируем исключение
	if(!is_array($arrayFirstRow)){
		throw new \Exception('Is not two-dimensional array');
	}

	// Ключи первого уровня
	$arrayKeysFirst = array_keys($arrayIn);
	// Ключи второго уровня
	$arrayKeysSecond = array_keys($arrayFirstRow);

	// Массив с результатом
	$arrayOut = array_fill_keys($arrayKeysSecond, []);

	// Заполняем массив значниями
	foreach($arrayKeysSecond as $mixedKeySecond) {

		$arrayOut[$mixedKeySecond] = array_combine($arrayKeysFirst, array_column($arrayIn, $mixedKeySecond));
	}

	return $arrayOut;
}