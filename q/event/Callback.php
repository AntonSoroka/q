<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.03.2017
 * Time: 10:24
 */

namespace q\event;


/**
 * Class Callback
 * @package q
 */
class Callback
{
	/**
	 * @var callable Калбек функция
	 */
	private $functionCallback;

	/**
	 * @var array Массив с индексами параметров которые нцжны калбеку
	 */
	private $arrayIndexArgument;

	/**
	 * Callback constructor.
	 * @param callable $functionCallback
	 * @param array $arrayIndexArgument
	 */
	public function __construct(callable $functionCallback, array $arrayIndexArgument)
	{
		$this->functionCallback = $functionCallback;
		$this->arrayIndexArgument = $arrayIndexArgument;
	}

	/**
	 * Выполнить калбек функцию
	 *
	 * @param array ...$arrayArgument
	 * @return mixed
	 */
	public function call(&...$arrayArgument){

		// Массив для паремтров нужных калбек функции
		$arrayPassArgument = [];

		// Добавляем нужные параметры
		foreach ($this->arrayIndexArgument as $intIndex){
			$arrayPassArgument[] = &$arrayArgument[$intIndex];
		}

		// Вызываем функцию и возвращаем результат
		return ($this->functionCallback)(...$arrayPassArgument);
	}
}