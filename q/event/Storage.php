<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.03.2017
 * Time: 9:32
 */

namespace q\event;


/**
 * Class Storage
 */
class Storage
{
	/**
	 * @var string Имя события
	 */
	private $stringEventName;

	/**
	 * @var \ReflectionMethod Оригмнальный метод
	 */
	private $reflectionMethod;

	/**
	 * @var Callback[] Массив слушателей
	 */
	private $arrayCallback = [];

	/**
	 * @var \ReflectionParameter[] Массив параметров
	 */
	private $reflectionParameters;


	/**
	 * Storage constructor.
	 * @param string $stringEventName
	 * @param \ReflectionMethod $reflectionMethod
	 */
	public function __construct(string $stringEventName, \ReflectionMethod $reflectionMethod)
	{
		$this->stringEventName = $stringEventName;
		$this->reflectionMethod = $reflectionMethod;
		$this->reflectionParameters = $this->reflectionMethod->getParameters();
	}

	/**
	 * Добавляем слушателя события
	 * @param callable $functionCallback
	 */
	public function addListener(callable $functionCallback){

		// Параметры которые нужны обработчику
		$reflectionParametersCallback = (new \ReflectionFunction($functionCallback))->getParameters();

		// Индексы найденых аргументов
		$arrayParametersIndex = [];

		// Перебираем их для проверки
		foreach($reflectionParametersCallback as $reflectionParameterCallback){

			// Найден параметр или нет
			$boolFound = false;

			// Перебираем параметры слушателя
			foreach ($this->reflectionParameters as $intIndex => $reflectionParameterListener){

				// Имена переменных не совпадают пропускаем параметр
				if($reflectionParameterCallback->getName() !== $reflectionParameterListener->getName()){
					continue;
				}

				// Не совпадает возможность принимать значение null
				if($reflectionParameterCallback->allowsNull() !== $reflectionParameterListener->allowsNull()){

					// Это неприемлемо, поэтому бросаем исключение
					throw new \InvalidArgumentException(
						'Class ' . $this->reflectionMethod->class . ', ' .
						'event ' . $this->stringEventName . ', ' .
						'method ' . $this->reflectionMethod->getName() . ', ' .
						'parameter ' . $reflectionParameterListener->getName() . ', ' .
						'is ' . ($reflectionParameterListener->allowsNull() ? '' : 'not ') . 'allowsNull'
					);
				}

				// Не совпадают тип принемаемого параметра
				if((string)$reflectionParameterCallback->getType() !== (string)$reflectionParameterListener->getType()){

					// Это неприемлемо, поэтому бросаем исключение
					throw new \InvalidArgumentException(
						'Class ' . $this->reflectionMethod->class . ', ' .
						'event ' . $this->stringEventName . ', ' .
						'method ' . $this->reflectionMethod->getName() . ', ' .
						'parameter ' . $reflectionParameterListener->getName() . ', ' .
						'type is ' .
						(null === $reflectionParameterListener->getType()
							? 'mixed'
							: (string)$reflectionParameterListener->getType()
						) . ', ' .
						'not ' .
						(null === $reflectionParameterCallback->getType()
							? 'mixed'
							: (string)$reflectionParameterCallback->getType()
						)
					);
				}

				// Аргумент в калбеке принимается неверным образом
				if($reflectionParameterCallback->isPassedByReference() !== $reflectionParameterListener->isPassedByReference()){

					// Это неприемлемо, поэтому бросаем исключение
					throw new \InvalidArgumentException(
						'Class ' . $this->reflectionMethod->class . ', ' .
						'event ' . $this->stringEventName . ', ' .
						'method ' . $this->reflectionMethod->getName() . ', ' .
						'parameter ' . $reflectionParameterListener->getName() . ', ' .
						'pass by ' .
						($reflectionParameterListener->isPassedByReference()
							? 'reference'
							: 'value'
						)
					);
				}

				// Параметр найден
				$boolFound = true;

				// Добавляем индекс параметра
				$arrayParametersIndex[] = $intIndex;

				// Прекращаем поиск, т.к. уже нашли параметр
				break;
			}

			// Если запрашиваемый калбеком параметр не найден, бросаем исключение
			if(!$boolFound){
				throw new \InvalidArgumentException(
					'Class ' . $this->reflectionMethod->class . ' ' .
					'event' . $this->stringEventName . ', ' .
					'method ' . $this->reflectionMethod->getName() . ' ' .
					'parameter ' . $reflectionParameterCallback->getName() . ' ' .
					'not found'
				);
			}
		}

		// Все нужные калбеку параметры нашлись в методе, добавляем его в мвссив обработчиков
		$this->arrayCallback[] = new Callback($functionCallback, $arrayParametersIndex);

	}

	/**
	 * Вызываем сщбытие
	 * @param array ...$arrayArgument
	 * @return mixed[]
	 */
	public function initEvent(&...$arrayArgument):array{

		// Массив с результатом выполнения функций
		$arrayMixedResult = [];

		// Перебираем обработчики
		foreach ($this->arrayCallback as $functionCallback){

			// Выполняем функцию обработчик
			$arrayMixedResult[] = $functionCallback->call(...$arrayArgument);
		}

		// Возвращаем результаты
		return $arrayMixedResult;
	}
}