<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.03.2017
 * Time: 8:50
 */

namespace q;
use q\event\{
	Storage
};


/**
 * Class Event
 * @package q
 */
abstract class Event
{
	/**
	 * @var Storage[] События которые поддерживает данный класс
	 */
	private $__arrayStorage = [];

	/**
	 * Event constructor.
	 */
	public function __construct()
	{
		// Находим все текущего класса
		$arrayReflectionMethod = (new \ReflectionClass(static::class))->getMethods();

		// Перебираем их
		foreach ($arrayReflectionMethod as $reflectionMethod){

			// Преобразуем имя метода в имя события
			$stringEventName = $this->transformMethodToEventName($reflectionMethod->getName());

			// Если не удалось преобразовать, то пропускаем метод
			if($stringEventName === null){
				continue;
			}

			$this->__arrayStorage[$stringEventName] = new Storage($stringEventName, $reflectionMethod);
		}
	}

	/**
	 * Трансформируем имя метода в название события
	 *
	 * @param string $stringMethodName
	 * @return string|null
	 */
	private function transformMethodToEventName(string $stringMethodName){

		// Если оно не совпадает с паттерном метода события
		if(strpos($stringMethodName, 'event') !== 0){

			// То возвращаем null
			return null;
		}

		// Генерируем имя из имени метода
		$stringEventName = substr($stringMethodName, 5);

		// Заменяем большие буквы на точку + эту букву
		$stringEventName = preg_replace('#([A-Z])#us', '.$1', $stringEventName);

		// Приводим строку к нижнему регистру
		$stringEventName = mb_strtolower($stringEventName);

		// Убираем точки по краям
		$stringEventName = trim($stringEventName, '.');

		// Возвращаем имя события
		return $stringEventName;
	}

	/**
	 * Добавить калбек функцию на определённое событие
	 *
	 * @param string $stringEventName название собития
	 * @param callable $functionCallback Калбек функция
	 * @return static
	 */
	public function on(string $stringEventName, callable $functionCallback){

		$stringEventName = trim($stringEventName, ' ,;');

		// Если есть пробелы точки запятые или точки с запятой это значит что функцию нужно навешать на несколько событий
		if(preg_match('# |,|;#usi', $stringEventName)){

			// Массив событий
			$arrayEvent = preg_split('#( |,|;)+#usi',$stringEventName);

			// Добавляем слушателя для каждого события отдельно
			foreach($arrayEvent as $stringMethod){
				$this->on($stringMethod, $functionCallback);
			}

			// Выходим
			return $this;
		}

		// Если такого события нет то выбрасываем исключение
		if(!isset($this->__arrayStorage[$stringEventName])){
			throw new \OutOfBoundsException('Event ' . $stringEventName . ' not found');
		}

		// добавляем слушателя
		$this->__arrayStorage[$stringEventName]->addListener($functionCallback);

		return $this;
	}

	/**
	 * Инициируем событие
	 *
	 * @param string $stringEventName
	 * @param array ...$arrayArgument
	 * @return array
	 */
	public function trigger(string $stringEventName, &...$arrayArgument):array{

		// Если такого события нет то выбрасываем исключение
		if(!isset($this->__arrayStorage[$stringEventName])){
			//throw new \OutOfBoundsException('Event ' . $stringEventName . ' not found');
			return [];
		}

		// Вызываем событие и возвращаем массив результатов выполнения калбек функций
		return $this->__arrayStorage[$stringEventName]->initEvent(...$arrayArgument);
	}


}