<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.09.2017
 * Time: 13:48
 */


namespace q\ai\distribution;

/**
 * Распределение больцмона
 *
 * @param float $floatCurrent Текущее значение
 * @param float $floatInitial Начальное значение
 * @return float Результат
 */
function bolcman(float $floatCurrent, float $floatInitial = 0.0):float{
	return $floatInitial / log(1 + $floatCurrent);
}