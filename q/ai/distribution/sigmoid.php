<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.09.2017
 * Time: 14:23
 */

/**
 * Сигмоидная активационная функция
 *
 * @param float $floatX
 * @return float
 */
function sigmoid(float $floatX):float{
	return 1 / (1 + exp(-$floatX));
}