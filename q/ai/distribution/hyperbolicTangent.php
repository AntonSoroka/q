<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.09.2017
 * Time: 14:19
 */

/**
 * Актовационная функция гиперболического тангенса
 *
 * @param float $floatX
 * @return float
 */
function hyperbolicTangent(float $floatX):float{
	$floatExp = exp(2 * $floatX);
	return ($floatExp - 1) / ($floatExp + 1);
}