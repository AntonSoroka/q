<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.09.2017
 * Time: 13:45
 */

namespace q\ai\distribution;

/**
 * Распределение Коши зависимое от текущего значения и искуственного времени
 *
 * @param float $floatCurrent Текущее положение
 * @param float $floatTime Искуственное время относительно первой итерации
 * @param float $floatInitial Точка инициализации
 * @return float
 */
function kohi(float $floatCurrent, float $floatTime, float $floatInitial = 0.0):float{
	$floatT = $floatInitial / (1 - $floatCurrent);
	return $floatT / ($floatT ** 2 + $floatTime ** 2);
}