<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.09.2017
 * Time: 9:13
 */

namespace q\ai;


class Vector{

	/**
	 * @var int[]|float[] Компоненты вектора
	 */
	protected $arrayComponents = [];

	public function __construct(...$arrayComponent){
		$this->addComponent(...$arrayComponent);
	}

	/**
	 * Пре
	 *
	 * @param array ...$arrayComponent
	 * @return array
	 * @throws \TypeError
	 */
	public static function toComponents(...$arrayComponent):array{

		$arrayComponentReturn = [];

		// Проверяем передванные компоненты
		foreach($arrayComponent as $mixedComponent){

			// Тип компонента
			$stringType = gettype($mixedComponent);

			switch($stringType){
				case 'NULL':
					$arrayComponentReturn[] = 0;
					break;
				case 'integer':
				case 'double':
				$arrayComponentReturn[] = $mixedComponent;
					break;
				case 'boolean':
					$arrayComponentReturn[] = $mixedComponent ? 1 : 0;
					break;
				case 'string':

					// Если строка пустая то ничего не добавляем
					if(mb_strlen($mixedComponent) === 0){
						break;
					}

					// Текущий символ
					$intOffset = 0;

					// Добавляем каждый символ строки
					while(true){

						// Добавляем код символа
						$arrayComponentReturn[] = \q\text\ordUtf8($mixedComponent, $intOffset);

						// Если строка закончилась то выходим из цикла
						if($intOffset === -1){
							break;
						}
					}
					break;
				case 'array':
					array_push($arrayComponentReturn, ...static::toComponents($mixedComponent));
					break;

				default:
					// Если это вектор то добавляем его
					if($mixedComponent instanceof self){
						array_push($arrayComponentReturn, ...$mixedComponent->arrayComponents);
						break;
					}
					throw new \TypeError('Unknown type for vector component');
			}
		}

		return $arrayComponentReturn;
	}

	/**
	 * Добавляет компонент к вектору
	 *
	 * @param array ...$arrayComponent
	 * @throws \TypeError
	 */
	public function addComponent(...$arrayComponent){
		$this->arrayComponents = static::toComponents(...$arrayComponent);
	}


	/**
	 * Вычисляет длинну вектора
	 * @return float
	 */
	public function length():float{

		// Сумма квадратов компонентов
		$mixedSumPow = 0;

		// Считаем сумму квадратов
		foreach($this->arrayComponents as $mixedComponent){
			$mixedSumPow += $mixedComponent ** 2;
		}

		// Извлекаем корень из суммы и возвращаем его
		return sqrt($mixedSumPow);
	}

	/**
	 * Создаёт и возвращает новый нормализованный вектор на основе своих компонентов
	 *
	 * @return Vector
	 */
	public function normalize():Vector{

		// Длинна вектора
		$floatLength = $this->length();

		// Компоненты нового вектора
		$arrayComponents = $this->arrayComponents;

		// Делим X на корень суммы квадратов всех X
		foreach($this->arrayComponents as $mixedComponent){
			$arrayComponents[] = $mixedComponent / $floatLength;
		}

		// Возвращаем нормализованный вектор
		return new Vector($arrayComponents);
	}


//	public function multiplication($mixedOperand):Vector{
//		$vectorResult = new Vector($this->arrayComponents);
//		return $vectorResult;
//	}
}