<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.09.2017
 * Time: 13:25
 */

namespace q\ai\neuron\lstm;

class Item extends \q\ai\neuron\Item{

	protected $vectorForgetT; // Вектор забывания
	protected $vectorInputT; // Вектор входного вентиля
	protected $vectorOutputT; // Вектор выходного вентиля
	protected $vectorConditionT; // Вектор текущего состояния

}