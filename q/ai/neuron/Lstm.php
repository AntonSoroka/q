<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.09.2017
 * Time: 13:19
 */

namespace q\ai\neuron;


use q\ai\neuron\lstm\Item;

class Lstm{

	/**
	 * @var int Количество нейронов
	 */
	protected $intLength;

	protected $arrayItem = [];

	public function __construct(int $intLength = 100){
		$this->intLength = $intLength;

		for($intIndex = 0; $intIndex < $intLength; $intIndex++){
			$this->arrayItem = new Item(mt_rand() / mt_getrandmax());
		}
	}
}