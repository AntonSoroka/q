<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.09.2017
 * Time: 13:27
 */

namespace q\ai\neuron;


abstract class Item{

	/**
	 * @var int
	 */
	protected $intWight;

	/**
	 * Item constructor.
	 * @param int $intWight Начальный вес нейрона
	 */
	public function __construct(int $intWight){
		$this->intWight = $intWight;
	}
}