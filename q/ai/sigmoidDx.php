<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.09.2017
 * Time: 16:56
 */

namespace q\ai;

/**
 * Производная сигмоижной функции от x
 *
 * @param float $floatX
 * @return float
 */
function sigmoidDx(float $floatX):float{
	$floatSigmoid = \q\ai\sigmoid($floatX);
	return $floatSigmoid * (1 - $floatSigmoid);
}