<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.09.2017
 * Time: 16:03
 */

namespace q\ai;

/**
 * Активационная функция для сглаживания значения между нулём и единицей
 *
 * @param float $floatX
 * @return float
 */
function sigmoid(float $floatX):float {
	return 1 / (1 + exp(-$floatX));
}