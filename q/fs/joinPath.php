<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 15:08
 */

namespace q\fs;

/**
 * Соединяет части переданного пути в один
 * @return string
 */
function joinPath():string{

	// Соединяем переданные аргументы слешем
	$stringPath = \implode('/', func_get_args());

	// Меняем виндовые слешы на юникстовые, винде всё равно, а перфикционисту приятно
	$stringPath = str_replace('\\', '/', $stringPath);

	// Удаляем относительные части путей и повторяющиеся слеши
	while(true){

		// Количество замен на этой итерации
		$intCountReplace = 0;

		// Производим поиск и замену
		$stringPath = preg_replace(
			[
				// Повторяющмеся слеши
				'#\\/{2,}#usi',

				// Относительный путь указывающий на текущую директорию /./
				'#\\/\\.(\\/|$)#usi',

				// Относительный путь указывающий на родительскую директорию /../
				'#([^\\/:]+)\\/\\.\\.(\\/|$)#usi',
			],
			'/', $stringPath, -1, $intCountReplace
		);

		// Если небыло сделано ни одной замены на этой итерации, то прекращаем цикл
		if($intCountReplace === 0){
			break;
		}
	}

	// Возвращаем склеяный путь
	return $stringPath;
}