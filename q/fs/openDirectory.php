<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 14:45
 */
namespace q\fs;

/**
 * @param string $stringDirectoryPath
 * @param resource|null $context
 * @return resource
 */
function openDirectory(string $stringDirectoryPath, $context = null){
	if(null === $context){
		return \opendir($stringDirectoryPath);
	}
	return \opendir($stringDirectoryPath, $context);
}