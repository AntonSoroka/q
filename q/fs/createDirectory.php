<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 14:53
 */
namespace q\fs;

/**
 * @param string $stringPath
 * @param int $intAccessMod
 * @param bool $boolRecursive
 * @param resource|null $context
 * @return bool
 */
function createDirectory(string $stringPath, int $intAccessMod = 0777, bool $boolRecursive = false, $context = null): bool{
	if(null === $context){
		return \mkdir($stringPath, $intAccessMod, $boolRecursive);
	}
	return \mkdir($stringPath, $intAccessMod, $boolRecursive, $context);
}