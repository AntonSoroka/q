<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 15:03
 */
namespace q\fs;

function crateFile(string $stringFileName, $context = null):bool{
	if(null === $context){
		return \is_int(\file_put_contents($stringFileName, ''));
	}
	return \is_int(\file_put_contents($stringFileName, '', 0, $context));
}