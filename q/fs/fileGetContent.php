<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 01.11.2017
 * Time: 9:22
 */

namespace q\fs;

use q\Cache;

/**
 * Запрос файла/ссылки с возможностью кеширования
 *
 * @param string $stringPath
 * @param float $floatCacheTime
 * @param mixed $context
 * @return bool|string
 */
function fileGetContent(string $stringPath, float $floatCacheTime = 0.0, $context = null){

	static $cache;

	if($floatCacheTime <= 0){
		// Запрашиваем файл и сразу отдаём его
		return file_get_contents($stringPath, false, $context);
	}


	if(!($cache instanceof Cache)){
		$cache = Cache::createCache('q\fs\fileGetContent');
	}

	// Ключ в кеше
	$stringCacheKey = md5($stringPath);

	// Статус ответа
	$intStatus = 0;

	// Пытаемся найти данные в кеше
	$stringResult = $cache->get($stringCacheKey, $floatCacheTime, $intStatus);

	// Кеш актуальный
	if($intStatus === Cache::STATUS_OK){

		// Отдаём его значение
		return $stringResult;
	}

	// Кеш устарел
	if($intStatus === Cache::STATUS_OLD) {

		// Записываем старое значение, чтобы обновить время
		$cache->set($stringCacheKey, $stringResult);
	}

	// Запрашиваем новое значение
	$stringResult = file_get_contents($stringPath, false, $context);

	// Записываем новое значение
	$cache->set($stringCacheKey, $stringResult);

	// Возвращаем новое значение
	return $stringResult;

}