<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 15:19
 */
namespace q\fs;

/**
 * Копирует директорию с её правами
 *
 * @param string $stringPathSource
 * @param string $stringPathDestination
 * @param bool $boolRecursive
 * @param null $context
 * @return bool
 */
function copyDirectory(string $stringPathSource, string $stringPathDestination, bool $boolRecursive = false, $context = null):bool{
	$intAccessMode = \fileperms($stringPathSource);
	return \q\fs\createDirectory($stringPathDestination, $intAccessMode, $boolRecursive, $context);
}