<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 9:44
 */

namespace q\fs;

function scan(string $stringPath, callable $functionCallback, $context = null){

	// Если путь не существует то прекращаем выполнение
	if(!file_exists($stringPath)){
		return;
	}

	// Если это файл то просто вызываем калбек и выходим
	if(!is_dir($stringPath)){
		$functionCallback($stringPath);
		return;
	}

	// Добавляем обязательный слеш в конце
	$stringPath = \rtrim($stringPath, '/\\') . '/';

	// Вызываем калбек для директории
	$functionCallback($stringPath);

	// Открываем директорию
	$resourceDirectory = \q\fs\openDirectory($stringPath, $context);
	
	
	// Последовательно читаем файлы
	while($stringPathCurrent = \readdir($resourceDirectory)){

		// Пропускаем ссылки
		if($stringPathCurrent === '.' || $stringPathCurrent === '..'){
			continue;
		}

		// Читаем внутренности
		scan($stringPath . $stringPathCurrent, $functionCallback, $context);
	}

	\closedir($resourceDirectory);
}