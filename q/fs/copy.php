<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.03.2017
 * Time: 20:20
 */

namespace q\fs;

/**
 * Рекурсивное копирование
 *
 * @param string $stringSource
 * @param string $stringDestination
 * @param resource|null $context
 * @return bool
 */
function copy(string $stringSource, string $stringDestination, $context = null):bool{


    // Если это файл копируем его
    if(is_file($stringSource)){
        return \q\fs\copyFile($stringSource, $stringDestination, $context);
    }

    // Добавляем обязательный слеш в конце пути, т.к. это директория
    $stringSource = rtrim($stringSource, '/\\') . '/';
    $stringDestination = rtrim($stringDestination, '/\\') . '/';

    // Копируем сам каталог, с оригинальными правами доступа
    \q\fs\copyDirectory($stringSource, $stringDestination, false, $context);

    // Открываем папку
    $resourceDirectory = \q\fs\openDirectory($stringSource, $context);

    // Читаем содержимое и копируем его
    while($stringChildrenName = \readdir($resourceDirectory)){

        // Если это ссылка на родителя или самого себя, то пропускаем
        if($stringChildrenName === '.' || $stringChildrenName === '..'){
            continue;
        }

        // Вызываем текущую функцию и если она не сумела скопировать файл возвращаем false
        if(!copy($stringSource . $stringChildrenName, $stringDestination . $stringChildrenName, $context)){
            return false;
        }
    }

    // Все прошло хорошо
    return true;
}