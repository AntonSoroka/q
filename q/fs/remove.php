<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 9:31
 */

namespace q\fs;

function remove(string $stringPath, $context = null):bool{

	// Если файл не не существует то возвращаем true
	if(!file_exists($stringPath)){
		return true;
	}

	// Если это файл то просто удаляем его
	if(is_file($stringPath)){
		return \q\fs\removeFile($stringPath, $context);
	}

	// Если это директория то сначала нужно удалить всё содержимое

	// Проверяем что-бы слеш обязательно был
	$stringPath = \rtrim($stringPath, '/\\') . '/';

	// Открываем каталог
	$resourceDirectory = \q\fs\openDirectory($stringPath, $context);

	// Читаем последовательно содержимое каталога
	while($stringPathCurrent = readdir($resourceDirectory)){

		// Пропускаем ссылки на себя и на родителя
		if($stringPathCurrent === '.' || $stringPathCurrent === '..'){
			continue;
		}

		// Удаляем элемент, если не получилось возвращаем false
		if(!remove($stringPath . $stringPathCurrent, $context)){
			return false;
		}
	}

	// Закрываем директорию
	\closedir($resourceDirectory);

	// Удаляем директорию и возвращаем результат
	return \q\fs\removeDirectory($stringPath, $context);
}