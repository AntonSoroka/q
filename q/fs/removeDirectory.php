<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 14:50
 */
namespace q\fs;

function removeDirectory(string $stringDirectoryPath, $context = null){
	if(null === $context){
		return \rmdir($stringDirectoryPath);
	}
	return \rmdir($stringDirectoryPath, $context);
}