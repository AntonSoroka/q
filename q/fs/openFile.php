<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 15:09
 */

namespace q\fs;

/**
 * @param string $stringPath
 * @param string $stringMode
 * @param null $context
 * @return resource
 */
function openFile(string $stringPath, string $stringMode, $context = null){
	if(null === $context){
		return \fopen($stringPath, $stringMode, false);
	}
	return \fopen($stringPath, $stringMode, false, $context);
}