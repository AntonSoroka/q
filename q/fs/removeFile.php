<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 14:43
 */
namespace q\fs;

function removeFile(string $stringPath, $context = null){
	if(null === $context){
		return \unlink($stringPath);
	}
	return \unlink($stringPath, $context);
}