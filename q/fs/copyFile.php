<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 15:17
 */
namespace q\fs;

/**
 * @param string $stringPathSource
 * @param string $stringPathDestination
 * @param null $context
 * @return bool
 */
function copyFile(string $stringPathSource, string $stringPathDestination, $context = null):bool{
	if(null === $context){
		return \copy($stringPathSource, $stringPathDestination);
	}
	return \copy($stringPathSource, $stringPathDestination, $context);

}