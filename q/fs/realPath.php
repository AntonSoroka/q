<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 15:06
 */

namespace q\fs;

function realPath(...$arrayPartPath):string{

	// Приводим путь к нужному виду
	$stringPath = joinPath(...$arrayPartPath);

	// Если файл не существует то вызываем исключение
	if(!file_exists($stringPath)){
		throw new \Exception("Path {$stringPath} not exists");
	}

	// Возвращаем указаный путь
	return $stringPath;
}