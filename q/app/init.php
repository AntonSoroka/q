<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 16:45
 */

namespace q\app;

if(PHP_VERSION_ID < 70000 === true){
	throw new \RuntimeException('PHP 7.0 or later, sorry');
}

// Параметры запроса
$arrayRequestInfo = \q\utils\requestInfo();

// Данные о запросе
define(__NAMESPACE__ . '\\' . 'REQUEST_DATA', $arrayRequestInfo);


/**
 * Точное время запроса.
 */
const REQUEST_START = REQUEST_DATA['request_start'];

/**
 * Файл который вызвал функцию (/www/site.com/index.php)
 */
const PATH_SCRIPT = REQUEST_DATA['path_script'];

/**
 * Путь до коневой директории сервера (/www/)
 */
const PATH_ROOT = REQUEST_DATA['path_root'];

/**
 * Путь до директории скрипта который запустили (/www/site.com/)
 */
const PATH_SITE = REQUEST_DATA['path_site'];

/**
 * Путь до директории исполнения (/www/|/|~)
 */
const PATH_CWD = REQUEST_DATA['path_cwd'];

/**
 * Запрос (/q/w/e/)
 */
const REQUEST = REQUEST_DATA['request'];

/**
 * Запрос без слеша вначале (q/w/e/)
 */
const REQUEST_PART = REQUEST_DATA['request_part'];

/**
 * Префикс сайта относительно корневой директории сервера (/site.com/).
 */
const PREFIX = REQUEST_DATA['prefix'];

/**
 * Префикс без слеша в конце (/site.com)
 */
const PREFIX_PART = REQUEST_DATA['prefix_part'];

/**
 * Ip того кто сделал запрос (127.0.0.1 при исполнение через консоль)
 */
const IP = REQUEST_DATA['ip'];

/**
 * IP в виде числа без знака (2130706433 при исполнение через консоль)
 */
const IP_LONG = REQUEST_DATA['ip_long'];

/**
 * Протокол (http|https).
 */
const SCHEME = REQUEST_DATA['scheme'];

/**
 * Хост (127.0.0.1 для выполнения из консоли)
 */
const HOST = REQUEST_DATA['host'];

/**
 * Порт (80 для выполнения из консоли)
 */
const PORT = REQUEST_DATA['port'];

/**
 * Указывает что текущий порт это порт по умолчанию для текущего протакола.
 */
const PORT_DEFAULT = REQUEST_DATA['port_default'];

/**
 * Если это порт по умолчанию, то пустая строка, иначе строка в формате ":PORT".
 */
const PORT_STRING = REQUEST_DATA['port_string'];

/**
 * URL до корневой директории сайта "http://you.com/site.com/".
 */
const URL = REQUEST_DATA['url'];

/**
 * Полный URL запроса "http://you.com/site.com/q/w/e/".
 */
const URL_REQUEST = REQUEST_DATA['url_request'];

/**
 * Полный URL запроса с параметрами "http://you.com/site.com/q/w/e/?qwe=1".
 */
const URL_FULL = REQUEST_DATA['url_full'];

/**
 * Метод запроса (GET|POST|PUT|DELETE|OPTIONS).
 */
const METHOD = REQUEST_DATA['method'];

/**
 * GET параметры.
 */
const GET = REQUEST_DATA['get'];

/**
 * POST параметры (если запрос выполняется методом PUT, то тут будут присланные данные).
 */
const POST = REQUEST_DATA['post'];

/**
 * FILES данные переданных файлов.
 */
const FILES = REQUEST_DATA['files'];

/**
 * FILES данные переданных файлов.
 */
const USER_AGENT = REQUEST_DATA['user_agent'];



// Отклчаем вывод ошибок вместе с HTML кодом
ini_set('html_errors', 'off');

// Заголовки по умолчанию запрещающие браузеру
// кешировать результат запроса и говорящие ему
// что это HTML в кодировке utf-8.
// В случае необходимости можно изменить это
// поведение для определённых страниц которые
// необходимо кешировать или если они не отдают
// HTML код

// HTML, utf-8
@header('Content-type: text/html; charset=utf-8');
// Дата в прошлом - время кода сгенерированная страница устареет
@header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
// Всегда модифицируется
@header('Last-Modified: ' . gmdate(\DateTime::RFC850));
// HTTP/1.1 - Говорим браузеру не кешировать ответ
@header('Cache-Control: no-store, no-cache, must-revalidate');
@header('Cache-Control: post-check=0, pre-check=0', false);
// Для старого протокола HTTP/1.0
@header('Pragma: no-cache');


