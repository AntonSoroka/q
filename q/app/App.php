<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 13:42
 */

namespace q;


use q\orm\Db;
use q\sql\Connect;
use q\utils\Route;

class App{

	/**
	 * @var App[]
	 */
	static protected $arrayAppList = [];

	/**
	 * @var string Имя приложения
	 */
	protected $stringName;

	/**
	 * @var View[] вьюшки
	 */
	protected $arrayView = [];

	/**
	 * @var array[array]
	 */
	protected $arrayConfigViewList = [];

	/**
	 * @var Connect
	 */
	protected $connect;

	/**
	 * @var array
	 */
	protected $arrayConfigConnect;


	/**
	 * @var Orm
	 */
	protected $orm;

	/**
	 * @var array
	 */
	protected $arrayConfigOrm;


	/**
	 * @var Cache
	 */
	protected $cache;

	/**
	 * @var array
	 */
	protected $arrayConfigCache;

	/**
	 * @var Route
	 */
	protected $route;

	/**
	 * Инициализация вьюшек
	 *
	 * @param string $stringName Имя комплекта вьюшек
	 * @param string $stringPath Путь до директории с вьюшками
	 * @param string $stringUrl Ссылка до директории с вьюшками
	 */
	public function initView(string $stringName, string $stringPath, string $stringUrl){

		// Вьюшка с таким именем уже инициирована
		if(isset($this->arrayConfigViewList[$stringName])){

			// Генерируем исключение
			throw new \LogicException("View['{$stringName}'] already init");
		}

		// Записываем параметры
		$this->arrayConfigViewList[$stringName] = [
			'name' => $stringName,
			'path' => $stringPath,
			'url' => $stringUrl,
		];
	}

	/**
	 * Возвращает вьюху по её имени
	 *
	 * @param string $stringName
	 * @return View
	 */
	public function getView(string $stringName):View{

		// Вьюха с таким именем ещё не инициализирована
		if(!isset($this->arrayView[$stringName])){

			// Настроек для вьюхи с таким именем нету
			if(!isset($this->arrayConfigViewList[$stringName])){

				// Генерируем исключение
				throw new \LogicException("View['$stringName'] config not set. Call method initView('$stringName', [other]) to fix it");
			}

			// Создаём вьюху согласно настройкам
			$this->arrayView[$stringName] = View::__create(
				$this->arrayConfigViewList[$stringName]['path'],
				$this->arrayConfigViewList[$stringName]['url'],
				$this
			);
		}

		return $this->arrayView[$stringName];
	}

	/**
	 * Инициализация подключения к базе
	 *
	 * @param string $stringBase
	 * @param string $stringUser
	 * @param string $stringPassword
	 * @param string $stringHost
	 * @param int $intPort
	 */
	public function initConnect(
		string $stringBase,
		string $stringUser = 'root',
		string $stringPassword = '',
		string $stringHost = '127.0.0.1',
		int $intPort = 3306
	){


		// Настройки для подключения уже пришли
		if(is_array($this->arrayConfigConnect)){

			// Генерируем исключение
			throw new \LogicException('Connect already init');
		}

		// Запоминаем настройки для подключения к базе
		$this->arrayConfigConnect = [
			'user' => $stringUser,
			'password' => $stringPassword,
			'base' => $stringBase,
			'host' => $stringHost,
			'port' => $intPort,
		];

		// Создаём и запоминаем подключение
		$this->connect = new Connect(
			$stringUser,
			$stringPassword,
			$stringBase,
			$stringHost,
			$intPort
		);
	}

	/**
	 * @return Connect Подключение к базе для этого приложения
	 */
	public function getConnect():Connect{

		// Соединение ещё не создано
		if(!($this->connect instanceof Connect)){

			// Настройки ещё не пришли
			if(!is_array($this->arrayConfigConnect)){

				// Вызываем исключение
				throw new \LogicException('Connect config not set. Call method initConnect to fix it');
			}

			// Создаём и запоминаем подключение
			$this->connect = new Connect(
				$this->arrayConfigConnect['user'],
				$this->arrayConfigConnect['password'],
				$this->arrayConfigConnect['base'],
				$this->arrayConfigConnect['host'],
				$this->arrayConfigConnect['port']
			);
		}

		// Возвращаем подключение
		return $this->connect;
	}

	/**
	 * Инициализация ORM
	 *
	 * @param string $stringOrmName Имя ORM
	 * @param string $stringFileInitialization Файл в который стоит записывать код для генерации структуры базы
	 * @param string $stringTablePrefix Префикс имём таблиц для которых нужно инициировать структуру
	 */
	public function initOrm(
		string $stringOrmName,
		string $stringFileInitialization,
		string $stringTablePrefix = ''
	){

		// Конфиги уже приходили
		if(is_array($this->arrayConfigOrm)){

			// Генерируем исключение
			throw new \LogicException('Orm already init');
		}

		// Запоминаем настройки для создания ORM
		$this->arrayConfigOrm = [
			'name' => $stringOrmName,
			'file_initialization' => $stringFileInitialization,
			'table_prefix' => $stringTablePrefix,
		];


	}

	/**
	 * @return Db этого приложения
	 */
	public function getDb():Db{

		// ORM ещё не создана
		if(!($this->orm instanceof Orm)){

			// Настройки ещё не пришли
			if(!is_array($this->arrayConfigOrm)){

				// Вызываем исключение
				throw new \LogicException('Orm config not set. Call method initOrm to fix it');
			}

			// Orm с таким именем уже есть
			if(Orm::issetInstance($this->arrayConfigOrm['name'])){

				// Создаём ORM
				$ormObject = Orm::getInstance($this->arrayConfigOrm['name'], $this->getConnect());
			}

			else{

				// Создаём ORM
				$ormObject = new Orm($this->getConnect(), $this->arrayConfigOrm['name']);
			}


			// Создаём базу
			$dbObject = $ormObject->createDb($this->getConnect()->getBaseName());

			// Генерируем структуру базы
			$dbObject->generateInitializationFile(
				$this->arrayConfigOrm['file_initialization'],
				$this->arrayConfigOrm['table_prefix']
			);

			// Запоминаем ORM
			$this->orm = $ormObject;
		}

		return $this->orm->getDb($this->getConnect()->getBaseName());
	}

	/**
	 * Создание роутов
	 * @return Route
	 */
	public function getRoute():Route{

		// Роутинг ещё не инициализирован
		if(!($this->route instanceof Route)){
			$this->route = new Route();
		}

		return $this->route;
	}

	/**
	 * Инициализация кеша
	 * @param string $stringName
	 * @throws \LogicException
	 */
	public function initCache(string $stringName){

		// Настройки кеша уже были переданы
		if($this->arrayConfigCache !== null){

			// Генерируем исключение
			throw new \LogicException('Cache already exist');
		}

		// Сохраняем настройки
		$this->arrayConfigCache = [
			'cache_name' => $stringName
		];
	}

	/**
	 * Возвращает объяект кеша для этого приложения
	 * @return Cache
	 */
	public function getCache():Cache{

		// Объяект кеша ещё не инициирован
		if(!($this->cache instanceof Cache)){

			// Настроек нет
			if($this->arrayConfigCache === null){

				// Называем кеш, так же как называется приложение
				$this->arrayConfigCache = [
					'cache_name' => $this->stringName,
				];
			}

			// Создаём объект кеша
			$this->cache = Cache::createCache($this->arrayConfigCache['cache_name']);
		}

		// Возвращаем объект кеша
		return $this->cache;
	}

	/**
	 * App constructor.
	 * @param string $stringName Имя приложения
	 */
	protected function __construct(string $stringName){
		$this->stringName = $stringName;
	}

	/**
	 * Возвращает приложение по его имени
	 * @param string $stringAppName
	 * @return App
	 */
	public static function getApp(string $stringAppName):App{

		// Приложение ещё не инициировано
		if(!isset(static::$arrayAppList[$stringAppName])){

			// Создаём приложение
			static::$arrayAppList[$stringAppName] = new static($stringAppName);
		}

		// Отдаём приложение
		return static::$arrayAppList[$stringAppName];
	}

	/**
	 * Проверяет существует ли приложение с переданным именем
	 *
	 * @param string $stringAppName
	 * @return bool
	 */
	public static function issetApp(string $stringAppName):bool{
		return isset(static::$arrayAppList[$stringAppName]);
	}


}