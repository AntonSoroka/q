<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.09.2017
 * Time: 9:28
 */

namespace q\text;

/**
 * Возвращает код символа из указанной позиции в строке
 *
 * @param string $stringChar Строка
 * @param int $intOffset Позиция символа
 * @return int Код символа
 */
function ordUtf8(string $stringChar, int &$intOffset = 0):int{

	// Стандартная функция
	//$intOrdCode = \ord(\substr($stringChar, $intOffset, 1));
	$intOrdCode = \ord($stringChar[$intOffset]);

	// Номер символа
	$intOffset = 0;

	// Если больше или равно 128 то это символ имеет другое значение в utf8
	if ($intOrdCode >= 128) {

		// Количество байт в символе
		$intBytes = 2;

		if ($intOrdCode < 224) {
			$intBytes = 2; //110xxxxx
		}
		else if ($intOrdCode < 240){
			$intBytes = 3; //1110xxxx
		}
		else if ($intOrdCode < 248) {
			$intBytes = 4; //11110xxx
		}

		// Начальное значение кода
		$intOrdUtf8Code = $intOrdCode - 192 - ($intBytes > 2 ? 32 : 0) - ($intBytes > 3 ? 16 : 0);

		// Прибавляем следующие байты к коду
		for ($intIndex = 2; $intIndex <= $intBytes; $intIndex++) {

			// Передвигаем указатель на следующий байт
			$intOffset++;

			// Значение следующего байта
			$intOrdUtf8CodeNext = \ord(\substr($stringChar, $intOffset, 1)) - 128;

			// Добавляем новый байт
			$intOrdUtf8Code = ($intOrdUtf8Code * 64) + $intOrdUtf8CodeNext;
		}

		// Это наш правильный код
		$intOrdCode = $intOrdUtf8Code;
	}

	// Передвигаем указатель
	$intOffset++;

	// Указываем что строка кончилась
	if($intOffset >= strlen($stringChar)){
		$intOffset = -1;
	}

	// Возвращаем код символа
	return $intOrdCode;
}