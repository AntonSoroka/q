<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.03.2017
 * Time: 16:48
 */

namespace q\text {

	/**
	 * Транслитерация строки
	 *
	 * @param string $stringText
	 * @return string
	 */
	function translit(string $stringText):string
	{
		// Заменяяем все символы которые нам известны
		$stringText = str_replace(
			array_keys(\q\text\tarnslit\CHAR_TABLE),
			array_values(\q\text\tarnslit\CHAR_TABLE),
			$stringText
		);

		// Убирвем не ANSI 0-128 символы
		$stringText = preg_replace("#[^\x01-\x7f]#usi", '', $stringText);

		// Отдаём строку
		return $stringText;

	}
}

namespace q\text\tarnslit {

	// Таблица преобразования символов
	const CHAR_TABLE = [

		// RU
		'а' => 'a',
		'А' => 'A',
		'б' => 'b',
		'Б' => 'B',
		'в' => 'v',
		'В' => 'V',
		'г' => 'g',
		'Г' => 'G',
		'д' => 'd',
		'Д' => 'D',
		'е' => 'e',
		'Е' => 'E',
		'ё' => 'e',
		'Ё' => 'E',
		'ж' => 'j',
		'Ж' => 'J',
		'з' => 'z',
		'З' => 'Z',
		'и' => 'i',
		'И' => 'I',
		'й' => 'y',
		'Й' => 'Y',
		'к' => 'k',
		'К' => 'K',
		'л' => 'l',
		'Л' => 'L',
		'м' => 'm',
		'М' => 'M',
		'н' => 'n',
		'Н' => 'N',
		'о' => 'o',
		'О' => 'O',
		'п' => 'p',
		'П' => 'P',
		'р' => 'r',
		'Р' => 'R',
		'с' => 's',
		'С' => 'S',
		'т' => 't',
		'Т' => 'T',
		'у' => 'y',
		'У' => 'Y',
		'ф' => 'f',
		'Ф' => 'F',
		'х' => 'h',
		'Х' => 'H',
		'ц' => 'c',
		'Ц' => 'C',
		'ч' => 'ch',
		'Ч' => 'Ch',
		'ш' => 'sh',
		'Ш' => 'Sh',
		'щ' => 'sh',
		'Щ' => 'Sh',
		'ъ' => "'",
		'Ъ' => "'",
		'ы' => 'y',
		'Ы' => 'Y',
		'ь' => "'",
		'Ь' => "'",
		'э' => 'e',
		'Э' => 'E',
		'ю' => 'u',
		'Ю' => 'U',
		'я' => 'ia',
		'Я' => 'Ia',

		// KG, ru + 3 chars
		'ө' => 'o',
		'Ө' => 'O',
		'ң' => 'n',
		'Ң' => 'N',
		'ү' => 'y',
		'Ү' => 'Y',

		// KZ ru + kg + 6 chars
		'ә' => 'a',
		'Ә' => 'A',
		'ғ' => 'f',
		'Ғ' => 'F',
		'қ' => 'k',
		'Қ' => 'K',
		'ұ' => 'y',
		'Ұ' => 'Y',
		'І' => 'Y',
		'і' => 'y',
		'Һ' => 'H',
		'һ' => 'h',
	];

}