<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.2017
 * Time: 11:01
 */

namespace q\text;

/**
 * Подбор слова для числа. Для любого числа достаточно 3 вариантов слова для (0,1,2)
 *
 * @param int $intCount число для подбора
 * @param string $stringOfZero То как пишется когда рядом число 0 (0 - коментариев)
 * @param string $stringOfOne То как пишется когда рядом число 1 (1 - коментарий)
 * @param string $stringOfTwo То как пишется когда рядом число 2 (2 - коментария)
 * @return string подобраная строка
 */
function declension(
	int $intCount,
	string $stringOfZero /*0*/,
	string $stringOfOne /*1*/,
	string $stringOfTwo /*2*/
):string{
	// Если число отрицательное то делаем его положительным
	if($intCount < 0){
		$intCount = -$intCount;
	}

	// Остаток от деления на 10
	$intRemainder10 = $intCount % 10;

	// Остаток от деления на 100
	$intRemainder100 = $intCount % 100;

	if(
		// Если остаток от деления от 5 до 9
		($intRemainder10 >= 5 && $intRemainder10 <= 9) ||

		// Или если остаток равен 0
		($intRemainder10 === 0) ||

		// Или если остаток от деления на 100 от 5 до 20
		($intRemainder100 >= 5 && $intRemainder100 <= 20)
	){
		// Отправляем вариант для нуля
		return $stringOfZero;
	}

	// Если остаток от деления на 10 равен единице
	else if($intRemainder10 === 1){
		return $stringOfOne;
	}

	// Иначе
	else{
		return $stringOfTwo;
	}

}