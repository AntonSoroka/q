<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.11.2017
 * Time: 12:39
 */

namespace q\utils;

const INT_LONG_MINUTE = 60;
const INT_LONG_HOURS = 3600;
const INT_LONG_DAY = 86400;

function floatToTime(float $floatTime, bool $boolMilliseconds = false){

	// Переменные для хранения времени
	$intDay = 0;
	$intHours = 0;
	$intMinute = 0;
	$intSecond = 0;

	// Остаток времени. Сразу убираем миллисекунды
	$intTimeRemainder = (int)$floatTime;

	// Высчитываем данные при необходимости
	switch (true){

		// Больше дня
		case $floatTime >= INT_LONG_DAY:
			$intDay = (int)($intTimeRemainder / INT_LONG_DAY);
			$intTimeRemainder %= INT_LONG_DAY;

		// Больше часа
		case $floatTime >= INT_LONG_HOURS:
			$intHours = (int)($intTimeRemainder / INT_LONG_HOURS);
			$intTimeRemainder %= INT_LONG_HOURS;

		// Больше минуты
		case $floatTime >= INT_LONG_MINUTE:
			$intMinute = (int)($intTimeRemainder / INT_LONG_MINUTE);
			$intTimeRemainder %= INT_LONG_MINUTE;

	}

	// Всё что осталось это секунды
	$intSecond = $intTimeRemainder;

	// Данные для вставки по формату
	$arrayData = [];

	// Части отформатированной строки
	$stringFormat = [];

	// Добавляем сегменты данных и формата в обраьтном порядке
	switch(true){

		// Дни если нужно
		case $floatTime >= INT_LONG_DAY:
			$arrayData[] = $intDay;
			$stringFormat[] = '%d ';

		// Часы если надо
		case $floatTime >= INT_LONG_HOURS:
			$arrayData[] = $intHours;
			$stringFormat[] = '%\'.02d:';

		// Минуты если надо
		case $floatTime >= INT_LONG_MINUTE:
			$arrayData[] = $intMinute;
			$stringFormat[] = '%\'.02d:';

		// Секунды обязательно
		case true:
			$arrayData[] = $intSecond;
			$stringFormat[] = '%\'.02d';

			// Если не указаны дни, то в максимальном приближении цифра должна быть без ведущего нуля
			switch(count($stringFormat)){
				case 3:
				case 2: $stringFormat[0] = '%d:'; break;
				case 1: $stringFormat[0] = '%d'; break;
			}

			// Миллисекунды если надо
			if($boolMilliseconds){
				$arrayData[] = (int)(($floatTime * 1000) % 1000);
				$stringFormat[] = '.%\'.03d';
			}
	}

	// Собираем строку и возвращаем её отформатированной
	return sprintf(\implode('', $stringFormat), ...$arrayData);




}