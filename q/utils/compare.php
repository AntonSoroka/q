<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 09.03.2017
 * Time: 11:15
 */
namespace q\utils;

/**
 * Сравнение двух переменных. Массивы и объекты перебираются рекурсивно
 *
 * @param $mixedFirst
 * @param $mixedSecond
 * @param bool $boolStrict строгое сравнение с типами переменных
 * @return bool
 */
function compare($mixedFirst, $mixedSecond, bool $boolStrict = false):bool{

	// Если одно из значений равно null
	if($boolStrict && (null === $mixedFirst || null === $mixedSecond)){
		if(null === $mixedFirst && null === $mixedSecond){
			return true;
		}else{
			return false;
		}
	}

	// Типы переменных
	$stringTypeFirst = gettype($mixedFirst);
	$stringTypeSecond = gettype($mixedSecond);

	// Если это не массив и не объект то всё просто
	if(!(
		in_array($stringTypeFirst,  ['array', 'object']) ||
		in_array($stringTypeSecond, ['array', 'object'])
	)){
		return $boolStrict
			? $mixedFirst === $mixedSecond
			: $mixedFirst == $mixedSecond
		;
	}

	// Если сравнивать нужно структуру с массивом и сравнение строгое
	// то возвращаем false
	if($boolStrict && $stringTypeFirst !== $stringTypeSecond){
		return false;
	}

	// Приводим объекты к массивам
	if($stringTypeFirst === 'object'){
		$mixedFirst = (array)$mixedFirst;
	}
	if($stringTypeSecond === 'object'){
		$mixedSecond = (array)$mixedSecond;
	}

	// Количество элементов в каждом массиве
	$intCountFirst = count($mixedFirst);
	$intCountSecond = count($mixedSecond);

	if($intCountFirst != $intCountSecond) {
		return false;
	}

	// Проверка с типами
	if($boolStrict){

		// Ключи массивов
		$arrayKeyFirst = array_keys($mixedFirst);
		$arrayKeySecond = array_keys($mixedSecond);

		// Схождение массивов ключей
		$arrayKeyIntersect = array_intersect($arrayKeyFirst, $arrayKeySecond);

		// Количество элементов
		$intCountIntersect = count($arrayKeyIntersect);

		// Количество ключей не совпадает
		if($intCountIntersect != $intCountFirst) {
			return false;
		}

		// Сравниваем каждый элемент массива
		foreach($arrayKeyIntersect as $intIntersectKey => $stringIntersectValue){

			// Проверяем ключи если нужно строгое сравнение
			if($boolStrict && $arrayKeyFirst[$intIntersectKey] != $arrayKeySecond[$intIntersectKey]){
				return false;
			}

			// Значения
			$mixedValueFirst = $mixedFirst[$stringIntersectValue];
			$mixedValueSecond = $mixedSecond[$stringIntersectValue];

			// Вызываем рекурсивно текущую функцию
			if(!call_user_func(__FUNCTION__, $mixedValueFirst, $mixedValueSecond, true)){
				return false;
			}
		}
	}

	// Проверка без типов
	else {

		// Сбрасываем ключи
		$mixedFirst = array_values($mixedFirst);
		$mixedSecond = array_values($mixedSecond);

		// Перебираем значения первого массива
		for ($intIndexFirst = 0; $intIndexFirst < $intCountFirst; $intIndexFirst++) {

			// Значения
			$mixedValueFirst = $mixedFirst[$intIndexFirst];

			// Ключ во втором массиве
			$intIndexSecond = false;
			foreach ($mixedSecond as $intIndexSecondTemp => $mixedValueSecond) {
				if (call_user_func(__FUNCTION__, $mixedValueFirst, $mixedValueSecond, false)) {
					$intIndexSecond = $intIndexSecondTemp;
					break;
				}
			}

			// Если элемент не найден во втором массиве то прекращаем сравнение
			if ($intIndexSecond === false) {
				return false;
			} // Если нашли то удаляем элемент из массива
			else {
				unset($mixedSecond[$intIndexSecond]);
			}

		}

	}

	// Массивы и объекты равны
	return true;
}