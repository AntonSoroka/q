<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.2017
 * Time: 13:57
 */

namespace q\utils\argv;

function setValue(array &$arrayArguments, string $stringKey, $mixedValue){

	// Параметр с таким именем пришёл в первый раз
	if(!isset($arrayArguments[$stringKey])){

		// Записываем его
		$arrayArguments[$stringKey] = $mixedValue;
	}

	// Параметр с таким именем пришёл второй раз
	else if(!is_array($arrayArguments[$stringKey])){

		// Переделываем значение этого парамтра в массив, добавляя в него предыдущий и текущий
		$arrayArguments[$stringKey] = [$arrayArguments[$stringKey], $mixedValue];
	}

	// Парамтры с этим именем приходили больше двух раз
	else{

		// добавляем значение в конец массива
		$arrayArguments[$stringKey][] = $mixedValue;
	}
}