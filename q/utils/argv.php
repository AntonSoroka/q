<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.2017
 * Time: 13:34
 */

namespace q\utils;

/**
 * Возвращает список переданных в программу аргументов
 *
 * @return array
 */
function argv():array{

	// Переданные данные
	static $arrayArguments = null;

	// Уже распознаны
	if($arrayArguments !== null){

		// Возвращаем данные
		return $arrayArguments;
	}

	// Объявляем массив
	$arrayArguments = [];

	// Имя следующего параметра
	$stringValueName = null;

	// Берём PHP-шную глобальную переменную в которую записываются параметры
	/** @var array $argv */
	global $argv;

	// Перебираем аргументы
	foreach($argv ?? [] as $stringArgument){

		// Это имя следующего параметра
		if(strpos($stringArgument, '-') === 0){

			// Предыдущий параметр пришол пустым
			if($stringValueName !== null){

				// Нужно его заполнить этой пустотой
				argv\setValue($arrayArguments, $stringValueName, null);
			}

			// Запоминаем имя следующего параметра
			$stringValueName = substr($stringArgument, 1);
		}

		// Это именованый параметр
		else if($stringValueName){

			// Запоминаем его
			argv\setValue($arrayArguments, $stringValueName, $stringArgument);

			// Обнуляем имя
			$stringValueName = null;
		}

		// Это неименованый параметра
		else{

			// Добавляем его в конец массива
			$arrayArguments[] = $stringArgument;
		}
	}

	// Последний параметр пришол пустым
	if($stringValueName !== null){

		// Нужно его заполнить этой пустотой
		argv\setValue($arrayArguments, $stringValueName, null);
	}

	// Возвращаем параметры
	return $arrayArguments;
}