<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.2017
 * Time: 10:02
 */
namespace q\utils {

	/**
	 * Выполнение комманды через консоль с вызовом калбек функции для каждой новой строки.
	 * Работает с программами который перенаправляют свой вывод в консоль вместо потока записи
	 *
	 * @param string $stringCommand
	 * @param callable|null $functionCallback
	 * @param bool $bollReturnResult возвратить все строки результата после выполнения
	 * @return array|bool
	 */
	function cmd(string $stringCommand, callable $functionCallback = null, bool $bollReturnResult = true)
	{

		// Создаём файл в который будет записан пезультат выполнения команды
		$stringTempFilePath = tempnam('', 'cmd');

		// Настройки потоков ввода и вывода
		$arrayStreamConfig = array(
			0 => array('pipe', 'r'),
			1 => array('pipe', 'w'),
			2 => array('file', $stringTempFilePath, 'a')
		);

		// Создаём комманду в новом потоке
		$resourceCommand = proc_open($stringCommand, $arrayStreamConfig, $arrayResult);

		// В этот массив будем записывать строки результата выполнения
		if($bollReturnResult) {
			$arrayResultLines = [];
		}

		if (is_resource($resourceCommand)) {

			// Ожидаем поступления новых данных из потока вывода
			while (!feof($arrayResult[1])) {

				// Читаем новую строку и перекодируем её
				$stringLine = \q\utils\cmd\winString(fgets($arrayResult[1]));

				// Добавляем строку к результату
				if($bollReturnResult){
					$arrayResultLines[] = $stringLine;
				}

				// Вызываем калбэк функцию для строки
				if(null !== $functionCallback){
					$functionCallback($stringLine);
				}
			}
			// Закрываем потоки чтения и записи
			fclose($arrayResult[0]);
			fclose($arrayResult[1]);

			// Закрываем поток выполнения комманды
			proc_close($resourceCommand);
		}
		// Если не удалось создать новый поток то возвращаем false
		else {
			return false;
		}

		// Читаем данные из временного файла
		$arrayTempFileLines = file($stringTempFilePath);
		foreach ($arrayTempFileLines as $arrayTempFileLine) {
			// Читаем новую строку и перекодируем её
			$stringLine = \q\utils\cmd\winString($arrayTempFileLine);

			// Добавляем строку к результату
			if($bollReturnResult){
				$arrayResultLines[] = $stringLine;
			}

			// Вызываем калбэк функцию для строки
			if(null !== $functionCallback){
				$functionCallback($stringLine);
			}
		}
		unlink($stringTempFilePath);

		if($bollReturnResult) {
			/** @noinspection PhpUndefinedVariableInspection */
			return $arrayResultLines;
		}else{
			return true;
		}
	}
}

namespace q\utils\cmd{

	function winString(string $stringResult):string{
		if (PHP_OS === 'WINNT') {
			return iconv('CP866', 'utf-8', $stringResult);
		} else {
			return $stringResult;
		}
	}

}