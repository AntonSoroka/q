<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.2017
 * Time: 9:51
 */

namespace q\utils;

/**
 * Безопасный и менее содержтельный debug_backtrace
 */
function debug(){

	// Дерево вызовов
	$arrayDebugInfoParts = debug_backtrace();
	// Данные для вывода
	$arrayShortDebugInfoParts = array();

	// Регулярное выражение для поиска пути до директирии выполнения
	$regular_expression = '#^' . preg_quote(getcwd(), '#') . '#usi';

	// Перебираем массив с информацией о вызовах
	foreach($arrayDebugInfoParts as $arrayDebugInfoPart){

		$arrayShortDebugInfoParts[] =
			// Фенкция
			($arrayDebugInfoPart['function'] ?? '?') .
			'::' .
			// Файл, удаляем из пути путь до деректирии запуска
			($arrayDebugInfoPart['file'] ? preg_replace($regular_expression, '' , $arrayDebugInfoPart['file']) : '?') .
			':' .
			($arrayDebugInfoPart['line'] ?? '?') .
		'';
	}
	// Выводим информацию
	echo
		"\n" .
		str_repeat('=', 10) .
		"\n" .
		implode("\n", $arrayShortDebugInfoParts) .
		"\n\n"
	;
}