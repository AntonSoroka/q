<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 14.03.2017
 * Time: 12:15
 */

namespace q\utils;

/**
 * Отправка email сообщений
 * @package q\utils
 */
class Mail{

	/**
	 * @var string|array Получатель письма
	 */
	public $mixedTo;

	/**
	 * @var string Тема письма
	 */
	public $stringSubject;

	/**
	 * @var string Тело сообщения
	 */
	public $stringMessage;

	/**
	 * @var string|array Отправитель письма
	 */
	public $mixedFrom;

	/**
	 * @var string Кодировка письма в которой следует его отправить
	 */
	private $stringCharset = 'UTF-8';

	/**
	 * @var array Прикреплённые к письму файлы
	 */
	private $arrayFileList = [];

	/**
	 * @var string Уникальная строка для разделения частей письма
	 */
	private $boolBoundary;


	/**
	 * Mail constructor.
	 *
	 * @param string|array $mixedTo Кому
	 * @param string $stringSubject Тема
	 * @param string $stringMessage Сообщение
	 * @param string|array $mixedFrom От кого
	 */
	public function __construct(
		$mixedTo = '',
		string $stringSubject = '',
		string $stringMessage = '',
		$mixedFrom = ''
	){

		// Обязательные данные
		$this->mixedTo = $mixedTo;
		$this->stringSubject = $stringSubject;
		$this->stringMessage = $stringMessage;
		$this->mixedFrom = $mixedFrom;

		// Генерируем разднлитель
		$this->boolBoundary = '--' . md5(uniqid(self::class, true));

	}


	/**
	 * Прикрепление файла к письму
	 * 
	 * @param string $stringFilePath Полный путь до файла
	 * @param bool|string $stringNameForRecipient Замена имени, если указано то именно такое имя увидит получатель письма
	 */
	public function addFile(string $stringFilePath, string $stringNameForRecipient = null){

		// Расширение файла
		$stringExt = explode('.', $stringFilePath);
		$stringExt = array_pop($stringExt);

		// Если не указано имя то берём имя из самого файла
		if($stringNameForRecipient === null){
			$stringNameForRecipient = basename($stringFilePath);
		}

		// Если в имени для пользователя не указано расширение файла или оно
		// не совпадает с оригинальным то добавляем его
		if(!preg_match('#\.' . preg_quote($stringExt, '#') . '$#usi', $stringNameForRecipient)){
			$stringNameForRecipient = $stringNameForRecipient . '.' . $stringExt;
		}

		// Добавляем файл в список отправляемых
		$this->arrayFileList[] = array(
			'path' => $stringFilePath,
			'name' => $stringNameForRecipient,
		);
	}

	/**
	 * Перекодировка в строки с её base64 представление для письма
	 *
	 * @param string $stringData Строка для перекодировки
	 * @return string
	 */
	private function base64(string $stringData):string{
		return sprintf('=?%s?%s?%s?=', $this->stringCharset, 'B', base64_encode($stringData));
	}

	/**
	 * Преобразование email адресов в правильный вормат для заголовков письма
	 *
	 * @param string|array $mixedEmailList Адреса
	 * @return string|bool
	 */
	private function base64email($mixedEmailList){

		// Передан массив
		if(is_array($mixedEmailList)) {

			$arrayEmailList = $mixedEmailList;
		}

		// Если список адресов это не массив, то нужно слелать его массивом
		else{
			// Убираем переводы строк
			$stringEmailList = preg_replace('#[\r\n]#usi', ' ', $mixedEmailList);

			// Разбиваем получателей на элементы
			$arrayEmailList = explode(',', $stringEmailList);
		}


		// Перебираем получателей
		foreach($arrayEmailList as $intIndex => $stringEmail){

			// Убираем лишние символы по бокам
			$stringEmail = trim($stringEmail);

			// Если указано имя то его нужно закодировать
			if(preg_match('#^(.+?)<(.+?)>$#usi', $stringEmail, $matches)){

				// Кодируем email и имя в специальный формат, что-бы не произошла ошибка во время отправки
				$stringEmail = $this->base64(trim($matches[1])) . ' <' . trim($matches[2]) . '>';
			}

			// Записываем обработаный email обратно в массив
			$arrayEmailList[$intIndex] = $stringEmail;
		}

		// Если адресов нету то
		if(count($arrayEmailList) === 0){
			return false;
		}

		// Склеиваем адреса и отправляем их
		$stringEmailList = implode(', ', $arrayEmailList);
		return $stringEmailList;
	}

	/**
	 * Отправка письма
	 *
	 * @return bool
	 */
	public function send():bool{

		// Перекодируем адреса получателей для заголовков
		$stringTo = $this->base64email($this->mixedTo);

		// Если адресов нет, то отправлять письма некому
		// возвращаем false
		if($stringTo === false){
			return false;
		}

		// Перекодируем адреса получателей для заголовков
		$mixedFrom = $this->base64email($this->mixedFrom);



		// Проверяем и приводим в норму тему
		$stringSubject = $this->stringSubject;
		$stringSubject = preg_replace('#[\r\n]#usi', ' ', $stringSubject);
		$stringSubject = $this->base64($stringSubject);


		// Проверяем текст сообщения
		$stringHtml = $this->stringMessage;
		$stringHtml = chunk_split(base64_encode($stringHtml));


		//============================================
		// Заголовки
		$arrayHeader = [];

		// Версия протакола
		$arrayHeader[] = 'MIME-Version: 1.0';

		// Тип кодирования и разделитель частей
		$arrayHeader[] = 'Content-Type: multipart/mixed; boundary="' . $this->boolBoundary . '"';

		// Отправитель если указан
		if($mixedFrom !== false){
			$arrayHeader[] = 'From: ' . $this->base64email($this->mixedFrom);
		}

		// Соединяем заголовки
		$stringHeader = implode("\n", $arrayHeader) . "\n";


		//============================================
		// Тело сообщения
		$arrayMultipart = array();

		// Первая часть (само письмо)
		$arrayMultipart[] = '--' . $this->boolBoundary . "\n";
		$arrayMultipart[] = 'Content-Type: text/html; charset=' . $this->stringCharset . "\n";
		$arrayMultipart[] = 'Content-Transfer-Encoding: base64' . "\n\n";
		$arrayMultipart[] = $stringHtml;

		// Прикрепляем файлы
		foreach($this->arrayFileList as $stringFile){

			// Загружаем файл
			$content = file_get_contents($stringFile['path']);

			// Если невозможно загрузить, то значит что-то пощло не так
			if($content === false){
				return false;
			}

			// Добавляем данные о файле в письмо
			$arrayMultipart[] = '--' . $this->boolBoundary . "\n";
			$arrayMultipart[] = 'Content-Type: application/octet-stream' . "\n";
			$arrayMultipart[] = 'Content-Transfer-Encoding: base64' . "\n";
			$arrayMultipart[] = 'Content-Disposition: attachment; filename="' . $this->base64($stringFile['name']) . '"' . "\n\n";

			// Кодируем файл в base64 и делим на части для корректного распознания mail серверами
			$arrayMultipart[] = chunk_split(base64_encode($content));
		}

		// Добавляем разделитель в конец
		$arrayMultipart[] = '--' . $this->boolBoundary . '--' . "\n";

		// Собераем письмо в одну строку
		$stringMultipart = implode('', $arrayMultipart);

		// Отправляем письмо
		$result = mail($stringTo, $stringSubject, $stringMultipart, $stringHeader);

		// Возвращаем результат
		return (bool)$result;
	}
}