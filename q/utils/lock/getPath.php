<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 11.05.2017
 * Time: 12:31
 */

namespace q\utils\lock;


/**
 * Путь до файла блокировки, на случай если их нужно будет собрать
 *
 * @param string $stringPrefix
 * @param string $stringKey
 * @return string
 */
function getPath(string $stringPrefix, string $stringKey):string{
	return sys_get_temp_dir() . '/q-utils-lock-' . $stringPrefix . '-' . $stringKey . '';
}