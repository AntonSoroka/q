<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 03.11.2017
 * Time: 14:03
 */

namespace q\utils;
use q\Cache;

/**
 * Отравка письма на указвный Email
 *
 * @param string $stringTo Адрес получателя
 * @param string $stringFrom Адрес отправителя
 * @param string $stringSubject Тема письма
 * @param string $stringMessage Текст письма
 * @param float $floatTimeNotResend Время которое письмо не отправляется заного
 * @param Mail $mail
 * @return bool
 */
function bugEmail(
	string $stringTo,
	string $stringFrom,
	string $stringSubject,
	string $stringMessage,
	float $floatTimeNotResend = 0.0,
	$mail = null
):bool{

	// Объяект для кеширования
	static $cache;

	// Отправку письма нужно выполнять не каждый раз
	if($floatTimeNotResend > 0){

		// Ключ
		$stringKey = md5(serialize([
			$stringTo,
			$stringFrom,
			$stringSubject,
			$stringMessage,
		]));

		// Объект для кеширования ещё не определён
		if(!($cache instanceof Cache)){

			// Инициируем кеш
			$cache = Cache::createCache('\q\utils\bugEmail');
		}

		// Статус данных в кеше
		$intStatus = 0;

		// Забираем данные из кеша
		$boolResult = $cache->get($stringKey, $floatTimeNotResend, $intStatus);

		// Переменная ещё не устарела и в прошлый раз отправка прошла успешно
		if($intStatus === Cache::STATUS_OK && $boolResult === true){

			// Завершаем выполнение
			return true;
		}

		// Временно записываем положительный результат отправки в кеш
		$cache->set($stringKey, true);
	}

	// Создаём письмо
	if($mail === null){
		$mail = new Mail(
			$stringTo,
			$stringSubject,
			$stringMessage,
			$stringFrom
		);
	}else{
		$mail->mixedTo = $stringTo;
		$mail->stringSubject = $stringSubject;
		$mail->stringMessage = $stringMessage;
		$mail->mixedFrom = $stringFrom;
	}

	// Отправляем письмо и возвращаем результат
	$boolResult = $mail->send();

	// Нужно сохранить результат отправки
	if(!empty($stringKey)){

		// Записываем результат в кеш
		$cache->set($stringKey, $boolResult);
	}

	// Возвращаем результат отправки
	return $boolResult;
}