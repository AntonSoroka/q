<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.2017
 * Time: 13:48
 */

namespace q\utils;

/**
 * Выполнение калбек функции с блокировкой на файл.
 * Если скрипт выполнится с ошибкой то PHP сам позаботится о разблокировки файла.
 *
 * @param string $stringKeyLock Ключ блокировки
 * @param callable|null $functionCallback Функция для выполнения, если не передана то устанавливается глобальный префикс
 * @return bool Выполнилась функция или нет
 * @throws \Exception Ключч должен удовлетворять следующему регульрному выражению ^[ \w\d-_]+$
 */
function lock(string $stringKeyLock, callable $functionCallback = null):bool
{
	// Префикс для всех блокировок
	static $stringPrefix = '';

	// Проверяем ключ на наличие неверных символов
	if (!preg_match('#^[ \w\d_-]+$#usi', $stringKeyLock)) {
		throw new \Exception('Lock key contains invalid characters, preg ^[ \w\d_-]+$');
	}

	// Если калбек функция не передана, то переопределяем префикс
	// по умолчанию и выходим из функции
	if (null === $functionCallback) {
		$stringPrefix = $stringKeyLock;
		return true;
	}

	// Файл который будет использоваться для блокировки
	$stringLockFilePath = \q\utils\lock\getPath($stringPrefix, $stringKeyLock);

	// Если файл не существует
	if (!file_exists($stringLockFilePath)) {

		// Создаём файл
		@file_put_contents($stringLockFilePath, '');

		// Меняем права доступа к файлу, что-бы скрипт выполненый от любого
		// пользователя мог получить доступ к этому файлу
		chmod($stringLockFilePath, 0666);
	}

	// Открываем файл для записи
	$resourceFile = fopen($stringLockFilePath, 'wb+');

	// Пытаемся заблокировать доступ к файлу для других
	if (!flock($resourceFile, LOCK_EX | LOCK_NB)) {

		// Если заблокировать не удалось значит он уже был заблокирован
		// другим скриптом и нам не нужно выполнять калбек функцию

		// Функция не выполнилась
		return false;
	}

	// Выполняем калбекфункцию если это единственный процес
	// который хочет её выполнить
	$functionCallback();

	// Функция выполнилась
	return true;
}


