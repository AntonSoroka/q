<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.11.2017
 * Time: 11:39
 */

namespace q\utils;

function timer(string $stringName, bool $boolTimeLast = false):float{

	// Тут будем хранить таймеры
	static $arrayTimer = [];

	// Такой таймер сеществует
	if(array_key_exists($stringName, $arrayTimer)){

		// Время с которым нужно сравнить
		$floatReturn = $arrayTimer[$stringName][$boolTimeLast ? 1 : 0];

		// Обновляем время с последнего вызова
		$arrayTimer[$stringName][1] = microtime(true);

		// Считаем и возвращаем значение
		return $arrayTimer[$stringName][1] - $floatReturn;
	}

	// Инициируем значение
	$arrayTimer[$stringName] = array_fill(0, 2, microtime(true));

	// Возвращаем 0 т.к. время не прошло
	return 0;
}