<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.11.2017
 * Time: 12:34
 */

namespace q\utils;

class Server{

	/**
	 * @var string
	 */
	protected $stringHost;

	/**
	 * @var int
	 */
	protected $intPort;

	/**
	 * @var string
	 */
	protected $stringFilePath;

	/**
	 * @var resource
	 */
	protected $resource;

	/**
	 * @var resource[]
	 */
	protected $arrayPipes = [];

	public function __construct(string $stringPhpFilePath, int $intPort = 88, string $stringHost = 'localhost'){
		$this->setPath($stringPhpFilePath);
		$this->setHost($stringHost);
		$this->setPort($intPort);
	}

	protected function setPath(string $stringPath):void{
		if(!file_exists($stringPath)){
			throw new \LogicException("File {$stringPath} not exist");
		}
		$this->stringFilePath = $stringPath;
	}

	public function setPort(int $intPort):void{

		if($intPort <= 0 || $intPort > 65535){
			throw new \LogicException("Bad port {$intPort}");
		}

		$this->intPort = $intPort;
	}

	public function setHost(string $stringHost):void{
		if(preg_match('#[^0-9a-z._-]#usi', $stringHost)){
			throw new \LogicException("Bad host {$stringHost}");
		}
		$this->stringHost = $stringHost;
	}

	public function start():void{

		// Потоки вывода
		$arrayDescription = [
			0 => array('pipe', 'r'),
			1 => array('pipe', 'w'),
			2 => array('pipe', 'a'),
		];

		// Запускаем сервер
		$this->resource = proc_open(
			"php -S {$this->stringHost}:{$this->intPort} {$this->stringFilePath}",
			$arrayDescription,
			$this->arrayPipes
		);

		// Спим чуть чуть, чтоб сервер успел стартовать
		usleep(0.2 * 1000000);
	}

	public function __destruct(){


		// Сервер не запущен
		if(!is_resource($this->resource)){
			return;
		}

		// Перебираем дескрипторы
		foreach($this->arrayPipes as $resourcePipe){

			// Закрываем текущий
			fclose($resourcePipe);
		}

		$arrayStatus = proc_get_status($this->resource);

		// Выключаем сервер
		switch(strtoupper(substr(php_uname('s'), 0, 3))){

			// На винде
			case 'WIN':
				exec(sprintf('TASKKILL /F /T /PID %d 2>&1', $arrayStatus['pid']), $stringOutput , $intExitCode);
				break;

			// На всём остальном
			default:
				proc_terminate($this->resource);
				proc_close($this->resource);
				break;
		}



	}

}