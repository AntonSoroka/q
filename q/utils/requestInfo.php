<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.08.2017
 * Time: 9:09
 */

namespace q\utils;

/**
 * @return array $return
 * 		$return = [
 * 			'request_start'   => (float) Точное время запроса.
 * 			'path_script'  => (string) Файл который вызвал функцию (/www/site.com/index.php).
 * 			'path_root'    => (string) Путь до коневой директории сервера (/www/).
 * 			'path_site'    => (string) Путь до директории скрипта который запустили (/www/site.com/).
 * 			'path_cwd'     => (string) Путь до директории исполнения (/www/|/|~).
 * 			'request'      => (string) Запрос (/q/w/e/).
 * 			'request_part' => (string) Запрос без слеша вначале (q/w/e/).
 * 			'prefix'       => (string) Префикс сайта относительно корневой директории сервера (/site.com/).
 * 			'prefix_part'  => (string) Префикс без слеша в конце (/site.com).
 * 			'ip'           => (string) Ip того кто сделал запрос (127.0.0.1 при исполнение через консоль).
 * 			'ip_long'      => (int) IP в виде числа без знака (2130706433 при исполнение через консоль).
 * 			'scheme'       => (string) Протокол (http|https).
 * 			'host'         => (string) Хост (127.0.0.1 для выполнения из консоли).
 * 			'port'         => (int) Порт (80 для выполнения из консоли).
 * 			'port_default' => (bool) Указывает что текущий порт это порт по умолчанию для текущего протакола.
 * 			'port_string'  => (string) Если это порт по умолчанию, то пустая строка, иначе строка в формате ":{$return[port]}".
 * 			'url'          => (string) URL до корневой директории сайта "http://you.com/site.com/".
 * 			'url_request'  => (string) Полный URL запроса "http://you.com/site.com/q/w/e/".
 * 			'url_full'     => (string) Полный URL запроса с параметрами "http://you.com/site.com/q/w/e/?qwe=1".
 * 			'method'       => (array) Метод запроса (GET|POST|PUT|DELETE|OPTIONS).
 * 			'get'          => (array) GET параметры.
 * 			'post'         => (array) POST параметры (если запрос выполняется методом PUT, то тут будут присланные данные).
 * 			'files'        => (array) FILES данные переданных файлов.
 * 			'user_agent'   => (string) Useragent переданный браузером пользователя
 * 		]
 */
function requestInfo():array{

	// Тут будем кешировать результат
	static $arrayInfo = null;

	// Если резкльтат уже однажды просчитывался
	if(null !== $arrayInfo){

		// Отдаём его
		return $arrayInfo;
	}

	// Данные по запросу
	$arrayInfo = [];


	// Время начала выполнения запроса
	$arrayInfo['request_start'] = isset($_SERVER['REQUEST_TIME_FLOAT'])
		? (float)$_SERVER['REQUEST_TIME_FLOAT']
		: microtime(true)
	;



	// Стак вызовов
	$arrayStackBacktraceList = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
	$arrayStackBacktraceLast = end($arrayStackBacktraceList);

	// Файл который вызвал функцию
	$arrayInfo['path_script'] = \q\fs\realPath($arrayStackBacktraceLast['file']);

	// Рабочая директория сервера
	if(isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT'] != ''){
		$stringDocumentRoot = $_SERVER['DOCUMENT_ROOT'];
	}else{
		$stringDocumentRoot = dirname($arrayInfo['path_script']);
	}

	$stringDocumentRoot = \q\fs\realPath($stringDocumentRoot, '/');
	$stringDocumentRootNotSlash = rtrim($stringDocumentRoot, '/');

	// Директория сайта
	$stringDirectory = dirname($arrayInfo['path_script']);
	$stringDirectory = str_replace('\\', '/', $stringDirectory);
	$stringDirectory = rtrim($stringDirectory, '/');
	$stringDirectory .= '/';

	// Префикс по которому лежит сайт
	if(!preg_match('#^' . preg_quote($stringDocumentRootNotSlash, '#') . '(.*)$#usi', $stringDirectory, $arrayMatches)){
		$stringPrefix = '';
	}else{
		$stringPrefix = $arrayMatches[1];
	}


	// URL запроса
	$stringRequestUrl = $_SERVER['REQUEST_URI'] ?? '';
	$stringRequestUrl = preg_replace('#(.*?)(\?|\#).*$#usi', '$1', $stringRequestUrl);
	if($stringPrefix === $stringRequestUrl . '/'){
		$stringRequestUrl = '';
	}else{
		$stringRequestUrl = preg_replace('#^' . preg_quote($stringPrefix, '#') . '#usi', '', $stringRequestUrl);
		if(isset($stringRequestUrl[0]) && $stringRequestUrl[0] !== '/'){
			$stringRequestUrl = '/' . $stringRequestUrl;
		}
	}


	// Путь до коневой директории сервера
	$arrayInfo['path_root'] = $stringDocumentRoot;

	// Путь до директории скрипта который запустил выполнение
	$arrayInfo['path_site'] = $stringDirectory;

	// Путь до директории исполнения
	$arrayInfo['path_cwd'] = \q\fs\realPath(getcwd(), '/');

	// Запрос
	$arrayInfo['request'] = $stringRequestUrl;
	$arrayInfo['request_part'] = ltrim($stringRequestUrl, '/');

	// Префикс сайта относительно корневой директории сервера
	$arrayInfo['prefix'] = $stringPrefix;
	$arrayInfo['prefix_part'] = rtrim($stringPrefix, '/');


	// IP
	if(
		// Запрос прошол через прокси сервер
		isset($_SERVER['HTTP_X_REAL_IP']) &&
		// Этот прокси находится на этом же сервере
		$_SERVER['SERVER_ADDR'] == ($_SERVER['REMOTE_ADDR'] ?? '127.0.0.1')
	){
		$arrayInfo['ip'] = $_SERVER['HTTP_X_REAL_IP'];
	}
	else if(isset($_SERVER['REMOTE_ADDR'])){
		$arrayInfo['ip'] = $_SERVER['REMOTE_ADDR'];
	}else{
		$arrayInfo['ip'] = '127.0.0.1';
	}

	// IP в виде числа
	$arrayInfo['ip_long'] = ip2long($arrayInfo['ip']);

	// Система 32-х битная
	if($arrayInfo['ip_long'] < 0){

		// Преобразуем в строку как беззнаковое число
		$arrayInfo['ip_long'] = sprintf('%u', $arrayInfo['ip_long']);
	}

	// Протокол
	if(isset($_SERVER['HTTPS'])){
		if($_SERVER['HTTPS'] === 'on'){
			$arrayInfo['scheme'] = 'https';
		}else{
			$arrayInfo['scheme'] = 'http';
		}
	}else if(isset($_SERVER['REQUEST_SCHEME'])){
		$arrayInfo['scheme'] = $_SERVER['REQUEST_SCHEME'];
	}else{
		try{
			$arrayInfo['scheme'] = \q\net\portDefaultScheme($_SERVER['SERVER_PORT'] ?? 80);
		}catch(\Exception $e){
			$arrayInfo['scheme'] = 'http';
		}
	}

	// Хост
	if(isset($_SERVER['HTTP_HOST'])){
		$arrayInfo['host'] = parse_url($_SERVER['HTTP_HOST'])['host'];
	}else if(isset($_SERVER['SERVER_NAME'])){
		$arrayInfo['host'] = $_SERVER['SERVER_NAME'];
	}else{
		$arrayInfo['host'] = '127.0.0.1';
	}

	// Порт
	if(isset($_SERVER['SERVER_PORT'])){
		$arrayInfo['port'] = (int)$_SERVER['SERVER_PORT'];
	}else{
		$arrayInfo['port'] = \q\net\schemeDefaultPort($arrayInfo['scheme']);
	}

	// Указывает что это порт по умолчанию
	$arrayInfo['port_default'] =  \q\net\schemeDefaultPort($arrayInfo['scheme']) == $arrayInfo['port'];

	// Порт строкой
	$arrayInfo['port_string'] = $arrayInfo['port_default'] ? '' : ':' . $arrayInfo['port'];

	// URL
	$arrayInfo['url'] = sprintf('%s://%s%s%s',
		$arrayInfo['scheme'],
		$arrayInfo['host'],
		$arrayInfo['port_string'],
		$arrayInfo['prefix']
	);

	// URL запроса
	$arrayInfo['url_request'] = $arrayInfo['url'] . $arrayInfo['request_part'];



	// Метод запроса
	$arrayInfo['method'] = $_SERVER['REQUEST_METHOD'] ?? 'GET';

	// GET параметры
	$arrayInfo['get'] = $_GET ?? [];

	// POST параметры
	$arrayInfo['post'] = $_POST ?? [];

	// Если запрос выполнен методом PUT то данные потеряются, запишим их в post что-бы заместить пустой массив
	if($arrayInfo['method'] === 'PUT') {

		// Разбиваем данные
		mb_parse_str(file_get_contents('php://input'), $arrayPutData);

		// Записываем данные в переменную
		$arrayInfo['post'] = $arrayPutData;
	}

	// FILES переданные файлы
	$arrayInfo['files'] = $_FILES ?? [];


	// Польный URL запроса
	if(count($arrayInfo['get']) > 0){
		$arrayInfo['url_full'] = $arrayInfo['url_request'] . '?' . http_build_query($arrayInfo['get']);
	}else{
		$arrayInfo['url_full'] = $arrayInfo['url_request'];
	}

	// Юзерагент пользователя
	$arrayInfo['user_agent'] = $_SERVER['HTTP_USER_AGENT'] ?? '';

	// Отдаём информацию о запросе
	return $arrayInfo;
}