<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.11.2017
 * Time: 9:14
 */

namespace q\utils;


/**
 * Переводит количество байт в человеческий вид
 *
 * @param int $intByte
 * @param int $intDecimal
 * @return string
 */
function byteToSize(int $intByte, int $intDecimal = 2):string{

	static $arraySize = ['байт','Кб','Мб','Гб','Тб','Пб'];

	// Это ноль
	if($intByte === 0){

		// Возвращаем подготовленное значение
		return '0 ' . $arraySize[0];
	}

	// Индекс приближения
	$intIndex = (int)floor(log($intByte,1024));

	// Округдение
	$floatResult = round($intByte / (1024 ** $intIndex), $intDecimal);

	// Результат
	return $floatResult . ' ' . $arraySize[$intIndex];
}