<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.07.2017
 * Time: 10:22
 */

namespace q;


class Color{

	/**
	 * @var int
	 */
	protected $intRed;
	/**
	 * @var int
	 */
	protected $intGreen;
	/**
	 * @var int
	 */
	protected $intBlue;
	/**
	 * @var float Альфа канал, прозрачность
	 */
	protected $floatAlpha;
	/**
	 * @var null|Color Бэкграунд, для подсчёта прозрачности если её нельзя передать как число
	 */
	protected $colorBackground;

	/**
	 * Color constructor.
	 * @param int $intRed
	 * @param int $intGreen
	 * @param int $intBlue
	 * @param float $floatAlpha
	 * @param Color|null $colorBackground
	 */
	public function __construct(int $intRed, int $intGreen, int $intBlue, float $floatAlpha = 1, Color $colorBackground = null){

		if($intRed > 255 || $intRed < 0){
			throw new \RangeException('Color red channel range is 0-255');
		}

		if($intGreen > 255 || $intGreen < 0){
			throw new \RangeException('Color green channel range is 0-255');
		}

		if($intBlue > 255 || $intBlue < 0){
			throw new \RangeException('Color blue channel range is 0-255');
		}

		if($floatAlpha > 1 || $floatAlpha < 0){
			throw new \RangeException('Color alpha channel range is 0-1');
		}

		$this->intRed = $intRed;
		$this->intGreen = $intGreen;
		$this->intBlue = $intBlue;
		$this->floatAlpha = $floatAlpha;
		$this->colorBackground = $colorBackground;
	}

	/**
	 * @return int Вычисляет каким бы был красный цвет на белом фоне с заданой прозрачностью
	 */
	protected function alphaRed():int{
		return \round(
			$this->intRed - (
				(
					$this->intRed - (
						$this->colorBackground ? $this->colorBackground->alphaRed() : 255
					)
				) * (1 - $this->floatAlpha)
			)
		);
	}

	/**
	 * @return int Вычисляет каким бы был зелёный цвет на белом фоне с заданой прозрачностью
	 */
	protected function alphaGreen():int{
		return \round(
			$this->intGreen - (
				($this->intGreen -
					($this->colorBackground ? $this->colorBackground->alphaGreen() : 255)
				) * (1 - $this->floatAlpha)
			)
		);
	}
	/**
	 * @return int Вычисляет каким бы был голубой цвет на белом фоне с заданой прозрачностью
	 */
	protected function alphaBlue():int{
		return \round(
			$this->intBlue - (
				($this->intBlue - (
					$this->colorBackground ? $this->colorBackground->alphaBlue() : 255)
				) * (1 - $this->floatAlpha)
			)
		);
	}

	/**
	 * Отдать цвет в шеснадцатиричном виде
	 *
	 * @return string
	 */
	public function toHex():string{
		return \sprintf(
			"%'.02X%'.02X%'.02X",
			$this->alphaRed(),
			$this->alphaGreen(),
			$this->alphaBlue()
		);
	}

	/**
	 * Отдать цвет в шеснадцатиричном виде для css
	 *
	 * @return string
	 */
	public function toCssHex():string{
		return '#' . $this->toHex();
	}

	/**
	 * Отдать цвет в формате rgb для css
	 *
	 * @return string
	 */
	public function toCssRgb():string{
		return \sprintf(
			'rgb(%d,%d,%d)',
			$this->alphaRed(),
			$this->alphaGreen(),
			$this->alphaBlue()
		);
	}

	/**
	 * Отдать цвет в формате rgba для css
	 *
	 * @return string
	 */
	public function toCssRgba():string{
		return \sprintf(
			'rgba(%d,%d,%d,%g)',
			$this->intRed,
			$this->intGreen,
			$this->intBlue,
			$this->floatAlpha
		);
	}

	/**
	 * Смешивает 2 цвета в один
	 *
	 * @param Color $colorObject1 Первый цвет
	 * @param Color $colorObject2 Второй цвет
	 * @param float $floatProportion Пропорчия цветов <0.5 преобладает первый
	 * @return Color
	 */
	public static function average(Color $colorObject1, Color $colorObject2, float $floatProportion = 0.5):Color{
		if($colorObject1->colorBackground || $colorObject2->colorBackground){
			$colorBackground = static::average(
				$colorObject1->colorBackground ?? new static(255,255,255),
				$colorObject2->colorBackground ?? new static(255,255,255),
				$floatProportion
			);
		}else{
			$colorBackground = new static(255,255,255);
		}

		return new Color(
			\round($colorObject1->intRed - (($colorObject1->intRed - $colorObject2->intRed) * $floatProportion)),
			\round($colorObject1->intGreen - (($colorObject1->intGreen - $colorObject2->intGreen) * $floatProportion)),
			\round($colorObject1->intBlue - (($colorObject1->intBlue - $colorObject2->intBlue) * $floatProportion)),
			($colorObject1->floatAlpha + $colorObject2->floatAlpha) / 2,
			$colorBackground
		);
	}
}







