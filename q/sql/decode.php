<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.03.2017
 * Time: 11:03
 */

namespace q\sql;

/**
 * Экранирование спецсимволов в строке, для передачи её в запрос
 *
 * @param string $stringData
 * @return string
 */
function decode(string $stringData):string{
	return str_replace(
		['\\',   "\0",  "\n",  "\r",  "'",   '"',   "\x1a"],
		['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z' ],
		$stringData
	);
}