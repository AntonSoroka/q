<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.03.2017
 * Time: 15:40
 */

namespace q\sql;

/**
 * Убирать кавычки экранирующие имена если они есть
 *
 * @param string $stringName
 * @return string
 */
function unescapeName(string $stringName):string{

	// Убираем лишнее по бокам
	$stringName = trim($stringName, '`');

	// Имя может содержать только латинские символы, цифры 0-9, знак доллора и знак нижнего подчёркивания
	if(preg_match('#`#usi', $stringName)){
		throw new \DomainException(
			'SQL object name exist symbol "`".' .
			"Name '{$stringName}'"
		);
	}

	// Имя не должно быть длиньше 64-х символов
	if(strlen($stringName) > 64){
		throw new \LengthException('SQL object names maximum length identifier 64 characters');
	}

	// Возвращаем имя
	return $stringName;
}