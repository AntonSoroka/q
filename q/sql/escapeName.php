<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.03.2017
 * Time: 14:12
 */
namespace q\sql;


/**
 * Экранирование названий в SQL таблицах
 *
 * @param string $stringName
 * @return string
 */
function escapeName(string $stringName):string{

	// Убираем кавычки экранирующие имена если они есть
	$stringName = \q\sql\unescapeName($stringName);

	return '`' . $stringName . '`';
}