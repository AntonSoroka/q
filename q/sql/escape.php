<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.03.2017
 * Time: 10:47
 */

namespace q\sql;

/**
 * Экранирование данных передваемых в запросы
 *
 * @param mixed $mixedValue Значение которое нужно обработать
 * @return string
 */
function escape($mixedValue):string{
	switch (gettype($mixedValue)){
		case 'boolean': return $mixedValue ? 'TRUE' : 'FALSE';
		case 'integer': return (string)$mixedValue;
		case 'double':  return (string)$mixedValue;
		case 'NULL':    return 'NULL';
		case 'string':  return '"' . \q\sql\decode($mixedValue) . '"';

		default: throw new \InvalidArgumentException('Unsupported type');
	}
}