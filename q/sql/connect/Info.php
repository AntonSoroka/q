<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.11.2017
 * Time: 9:56
 */

namespace q\sql\connect;


class Info{

	const FROM_DB = 1;
	const FROM_CACHE = 2;

	/**
	 * @var int
	 */
	protected $intFrom;

	/**
	 * @var string
	 */
	protected $stringQuery;

	/**
	 * @var float
	 */
	protected $floatTime;

	/**
	 * @var int
	 */
	protected $intCountSubQuery = 0;

	/**
	 * Info constructor.
	 * @param int $intFrom
	 * @param string $stringQuery
	 * @param float $floatTimeStart
	 * @param int $intCountSubQuery
	 * @throws \LogicException
	 */
	public function __construct(
		int $intFrom,
		string $stringQuery,
		float $floatTimeStart,
		int $intCountSubQuery = 1
	){

		// Проверяем источник
		switch($intFrom){
			case static::FROM_DB: break;
			case static::FROM_CACHE: break;
			default: throw new \LogicException("From {$intFrom} not exist");
		}

		// Количество запросов не может быть меньше нуля
		if($intCountSubQuery < 0){
			throw new \LogicException('Count sub query less than zero');
		}

		$this->intFrom = $intFrom;
		$this->stringQuery = $stringQuery;
		$this->floatTime = microtime(true) - $floatTimeStart;
		$this->intCountSubQuery = $intCountSubQuery;
	}

	/**
	 * @return int Источник данных
	 */
	public function getFrom():int{
		return $this->intFrom;
	}

	/**
	 * @return string Запрос
	 */
	public function getQuery():string{
		return $this->stringQuery;
	}

	/**
	 * @return float Время выполнения
	 */
	public function getTime():float{
		return $this->floatTime;
	}

	/**
	 * @return int Количество подзапросов
	 */
	public function getCountSubQuery():int{
		return $this->intCountSubQuery;
	}

	/**
	 * Добавить один подзапрос
	 */
	public function addSubQuery(){
		$this->intCountSubQuery++;
	}

}