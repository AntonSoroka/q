<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 10.11.2017
 * Time: 10:14
 */

namespace q\sql\connect;


class InfoList{

	const MAX_QUERY_STORE = 10;

	/**
	 * @var Info[]
	 */
	protected $arrayInfo = [];

	/**
	 * @var array
	 */
	protected $arrayStat = [
		'count' => 0,
		'countFrom' => [
			Info::FROM_DB => 0,
			Info::FROM_CACHE => 0,
		],
		'countQuery' => 0,
		'countQueryFrom' => [
			Info::FROM_DB => 0,
			Info::FROM_CACHE => 0,
		],
		'countSubQuery' => 0,
		'countSubQueryFrom' => [
			Info::FROM_DB => 0,
			Info::FROM_CACHE => 0,
		],
		'time' => 0.0,
		'timeFrom' => [
			Info::FROM_DB => 0,
			Info::FROM_CACHE => 0,
		],
	];


	public function push(Info $info){

		// Добавляем новый элемент
		$this->arrayInfo[] = $info;

		// Количество элементов больше заданого лимита
		if(\count($this->arrayInfo) > static::MAX_QUERY_STORE){

			// Удаляем первый элемент
			array_shift($this->arrayInfo);
		}

		$intFrom = $info->getFrom();
		$intCountSubQuery = $info->getCountSubQuery();
		$intCountQuery = $intCountSubQuery > 0 ? 1 : 0;
		$floatTime = $info->getTime();

		$this->arrayStat['count']++;
		$this->arrayStat['countQuery'] += $intCountQuery;
		$this->arrayStat['countSubQuery'] += $intCountSubQuery;
		$this->arrayStat['time'] += $floatTime;

		if(in_array($intFrom, [Info::FROM_DB, Info::FROM_CACHE], true)){
			$this->arrayStat['countFrom'][$intFrom] += 1;
			$this->arrayStat['countQueryFrom'][$intFrom] += $intCountQuery;
			$this->arrayStat['countSubQueryFrom'][$intFrom] += $intCountSubQuery;
			$this->arrayStat['timeFrom'][$intFrom] += $floatTime;

		}
	}

	/**
	 * Посчитать количество запросов
	 * @param int $intFrom
	 * @return int
	 */
	public function count(int $intFrom = null):int{

		// Источник не важен
		if($intFrom === null){
			return $this->arrayStat['count'];
		}

		// Источник указан
		return $this->arrayStat['countFrom'][$intFrom] ?? 0;
	}

	/**
	 * Посчитать количество запросов
	 * @param int $intFrom
	 * @return int
	 */
	public function countQuery(int $intFrom = null):int{

		// Источник не важен
		if($intFrom === null){
			return $this->arrayStat['countQuery'];
		}

		// Источник указан
		return $this->arrayStat['countQueryFrom'][$intFrom] ?? 0;
	}

	/**
	 * Посчитать количество запросов
	 * @param int $intFrom
	 * @return int
	 */
	public function countSubQuery(int $intFrom = null):int{

		// Источник не важен
		if($intFrom === null){
			return $this->arrayStat['countSubQuery'];
		}

		// Источник указан
		return $this->arrayStat['countSubQueryFrom'][$intFrom] ?? 0;
	}

	/**
	 * Считает затраченое время на все запросы
	 *
	 * @param int|null $intFrom
	 * @return float
	 */
	public function time(int $intFrom = null):float{

		// Источник не важен
		if($intFrom === null){
			return $this->arrayStat['time'];
		}

		// Источник указан
		return $this->arrayStat['timeFrom'][$intFrom] ?? 0;
	}


	/**
	 * Вазвращает информацию о запросе, по умолчании о последнем (-1)
	 *
	 * @param int $intIndex Индекс запроса
	 * @return Info
	 * @throws \OutOfRangeException
	 */
	public function getInfo(int $intIndex = -1):Info{

		// Пересчитываем отрицательный индекс
		$intRealIndex = $intIndex < 0
			? \count($this->arrayInfo) + $intIndex
			: $intIndex
		;

		// Информации по индексу нет
		if(!isset($this->arrayInfo[$intRealIndex])){
			throw new \OutOfRangeException("Info index {$intIndex} not exist");
		}

		return $this->arrayInfo[$intRealIndex];
	}
}