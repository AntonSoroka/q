<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 16:14
 */

namespace q\sql;
use q\Cache;
use q\sql\connect\Info;
use q\sql\connect\InfoList;


/**
 * Class Connect
 * @package q\sql
 */
class Connect
{

	/**
	 * @var float Время затроченное на выполнение всех запросов
	 */
	protected static $floatQueryTime = 0.0;

	/**
	 * @var \Mysqli Объект подключения к базк данных
	 */
	protected $objectMysqli;

	/**
	 * @var string Ключ подключения к базе данных, нужен для создания клюяа кеша для этого подключения
	 */
	protected $stringConnectKey;

	/**
	 * @var \q\Cache Объект кеша в него будем сохранять данные которые можно кешировать
	 */
	protected $objectCache;

	/**
	 * @var string
	 */
	protected $stringBaseName;

	/**
	 * Конструктор подключений к базе данных
	 *
	 * @param string $stringUser
	 * @param string $stringPassword
	 * @param string $stringDataBase
	 * @param string $stringHost
	 * @param int $intPort
	 * @throws \Exception Не удалось подключиться к базе данных
	 */
	public function __construct(
		string $stringUser = 'root',
		string $stringPassword = '',
		string $stringDataBase = '',
		string $stringHost = '127.0.0.1',
		int $intPort = 3306
	){

		// Подключаемся к базе
		$this->objectMysqli = new \Mysqli($stringHost, $stringUser, $stringPassword, $stringDataBase, $intPort);

		// Если произошла ошибка, выкидываем исключение
		if(mysqli_connect_errno()){
			throw new \RuntimeException(mysqli_connect_error());
		}

		// Имя базы данных к которой нужно подключиться
		$this->stringBaseName = $stringDataBase;

		// Устанавливаем кодировку
		$this->objectMysqli->query('SET NAMES "utf8mb4"');

		// Максимальная длянна строки получаемой при помощи функции GROUP_CONCAT
		$this->objectMysqli->query('SET group_concat_max_len = 655350');

		// Хеш параметров текущего подключения
		$this->stringConnectKey = md5(
			$stringUser .
			$stringPassword .
			$stringDataBase .
			$stringHost .
			$intPort
		);

		// Создаём объякт для хранения кеша
		$this->objectCache = Cache::createCache($this->stringConnectKey);

		// Объект для записи информации о запросе
		$this->infoList = new InfoList();
	}

	public function getBaseName():string{
		return $this->stringBaseName;
	}

	public function setNames(string $stringCharset){
        $this->objectMysqli->query('SET NAMES ' . \q\sql\escape($stringCharset));
    }

	/**
	 * Выполнить запрос к базе данных.
	 *
	 * Если переданно несколько запросов через точку с запятой, то результат вернётся в виде массива результатов
	 * выполнения каждого запроса.
	 *
	 * Результатом выполнения запроса может быть массив, если запрос мозвращает набор строк. Если запрос вставляет
	 * новую запись в базу данных и в таблице есть поле с autoincrement значениеи то вернётся id последней вставленной
	 * записи. Если же вставляется несколько записей или выполнен запрос на удаление/обновление то будет возаращено
	 * количество затронутых запросом строк в базе данных. Если запрос не возвращает никаких результатов то в качестве
	 * результата будет возвращено значение NULL
	 *
	 * @param string $stringQuery Запрос или несколько запросов через запятую
	 * @param float $floatCacheTime Если больше 0 то результат запроса будет сохранён в кеш или возвращён из кеша
	 * @param float|null $floatLastUpdate Дата обновления связанных данных
	 * @param string $stringAlternativeKey Альтернативный ключ для хранения кеша
	 * @return array
	 */
	public function query(string $stringQuery, float $floatCacheTime = 0.0, float $floatLastUpdate = null, string $stringAlternativeKey = null):array{

        // Нужно кешировать или вернуть закешированное значение
		if($floatCacheTime > 0.0){

			// Статус выполнения запроса к кешу
			$intStatus = 0;

			// Строим ключ
			if($stringAlternativeKey === null){
				$stringCacheKey = \md5(\serialize([$stringQuery, $floatLastUpdate]));
			}else{
				$stringCacheKey = \md5(\serialize($stringAlternativeKey));
			}

			// Есть максимальная дата обновления связанных таблиц
			if($floatLastUpdate !== null){

				// Вычисляем время прошедшее с обновления таблиц с данными и выбираем его если они меньше времени кеша
				$floatCacheTime = min(
					$floatCacheTime,
					microtime(true) - $floatLastUpdate
				);
			}

			// Время начала запроса
			$floatTimeCacheStart = microtime(true);

			// Забираем данные из кеша
			$arrayResult = $this->objectCache->get($stringCacheKey, $floatCacheTime, $intStatus);


			if(
				// Есть максимальная дата обновления связанных таблиц
				$floatLastUpdate !== null &&
				// Даные обновились позже чем был записан кеш
				$floatLastUpdate > microtime(true) - $floatCacheTime
			){
				$intStatus = Cache::STATUS_OLD;
			}

			// Если в кеше найден результат этого запроса то отдаём его
			if($intStatus === Cache::STATUS_OK){

				// Записываем онформацию о запросе
				$this->infoList->push(new Info(Info::FROM_CACHE, $stringQuery, $floatTimeCacheStart));

				// Возвращаем закешированное значение
				return $arrayResult;
			}

			// Кеш устарел
			if($intStatus === Cache::STATUS_OLD){
				// Записываем старый результат, это обновит время
				$this->objectCache->set($stringCacheKey, $arrayResult);
			}
		}

		// Записываем время перед началом выполнения запроса
		$floatTimeQueryStart = microtime(true);

		// Выполняем запрос
		$mixedResult = $this->objectMysqli->multi_query($stringQuery);

		// Записываем информацию о выполнении
		$info = new Info(Info::FROM_DB, $stringQuery, $floatTimeQueryStart, 0);

		// Если запрос выполнен успешно
		if($mixedResult === true){

			// Массив результатов этих запросов
			$arrayResult = [];

			// Забираем результаты всех запросов
			do{

				// Указываем что есть ещё один подзапрос
				$info->addSubQuery();

				// Получить результат очередного запроса
				$mysqliResultObject = $this->objectMysqli->store_result();

				// Если есть ошибки, то генерируем исключение
				if(!$mysqliResultObject && $this->objectMysqli->errno !== 0){
					throw new \RuntimeException($this->objectMysqli->errno . ' ' . $this->objectMysqli->error);
				}


				// Это INSERT запрос возвращающий autoincrement_id последней вставленной записи
				if($mysqliResultObject === false && $this->objectMysqli->insert_id !== 0){

					// Возвращаем autoincrement_id вставленной записи
					$arrayResult[] = $this->objectMysqli->insert_id;
				}

				// Этот запрос что-то обновил или удалил
				else if($mysqliResultObject === false && $this->objectMysqli->affected_rows !== 0){

					// Возвращаем количество затронутых строк
					$arrayResult[] = $this->objectMysqli->affected_rows;
				}

				// Текущий запрос не возвращает результата
				else if($mysqliResultObject === false){

					// Указываем что не была затронута ни одна запись
					$arrayResult[] = 0;
                }

				// Это SELECT запрос или любой другой возвращающий нобор строк
				else{

                    // Добавляем к результату пустой массив и запоминаем индекс
                    $intIndex = array_push($arrayResult, []) - 1;

                    // Читаем результат из базы
                    while($arrayRow = $mysqliResultObject->fetch_array(MYSQLI_ASSOC)){

                        // Добавляем новую строку в массив по запомненному индексу
                        $arrayResult[$intIndex][] = $arrayRow;
                    }

					// Освобождаем память занятую результатом запроса
					$mysqliResultObject->free();
				}



			}while(
				// Если есть ещё результаты
				$this->objectMysqli->more_results() &&

				// То подготавливаем этот результат
				$this->objectMysqli->next_result()
			);

			// Если есть ошибки, то генерируем исключение
			if($this->objectMysqli->errno !== 0){
				throw new \RuntimeException($this->objectMysqli->errno . ' ' . $this->objectMysqli->error . "\n" . $stringQuery);
			}
		}

		// Во время запроса произошла ошибка
		else{
			throw new \RuntimeException($this->objectMysqli->errno . ' ' . $this->objectMysqli->error);
		}

		// Записываем информацию о запросе
		$this->infoList->push($info);

		// Нужно закешировать результат, если указано время жизни
		if(!empty($stringCacheKey)){
			/** @noinspection PhpUndefinedVariableInspection */
			$this->objectCache->set($stringCacheKey, $arrayResult);
		}

		// Возвращаем значение
		return $arrayResult;
	}

	/**
	 * @var InfoList
	 */
	protected $infoList;

	/**
	 * @return InfoList Информация о запросе
	 */
	public function getInfoList():InfoList{
		return $this->infoList;
	}

}