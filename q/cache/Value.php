<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.2017
 * Time: 16:22
 */

namespace q\cache;


/**
 * Класс для создания объектов в которых хранятся значения
 * @package q\cache
 */
class Value
{
	/**
	 * @var string Ключь значения
	 */
	public $stringKey;
	/**
	 * @var mixed Хранимая переменная
	 */
	public $mixedValue;
	/**
	 * @var float Время создания/обновления
	 */
	public $floatTime;

	/**
	 * Value constructor.
	 * @param string $stringKey Ключь значения
	 * @param mixed $mixedValue Хранимая переменная
	 * @param float $floatTime Время создания/обновления
	 */
	public function __construct(string $stringKey, $mixedValue, float $floatTime)
	{
		$this->stringKey = $stringKey;
		$this->mixedValue = $mixedValue;
		$this->floatTime = $floatTime;
	}

}