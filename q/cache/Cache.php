<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.2017
 * Time: 13:01
 */

namespace q;


use q\cache\Storage;
use q\cache\storage\Fs;
use q\cache\storage\Memcache;
use q\cache\storage\No;
use q\cache\Value;

/**
 * Class Cache
 */
class Cache
{
	const STATUS_ERROR = -1;
	const STATUS_EMPTY = 0;
	const STATUS_OK = 1;
	const STATUS_OLD = 2;


	/**
     * Массив с именами классов потомков от класса \q\cache\Storage, порядок в котором они перечисленны это приоритет
	 * @var array[string]
	 */
	public static $arrayStorageType = [
		Memcache::class,
		Fs::class,
		No::class,
	];

	/**
	 * @var string
	 */
	public static $stringMemcacheHost = '127.0.0.1';
	/**
	 * @var int
	 */
	public static $intMemcachePort = 11211;

	/**
	 * @var string
	 */
	public static $stringFsPath; // = sys_get_temp_dir();


	/**
	 * @var Storage
	 */
	private $storageObject;

	/**
	 * @var string
	 */
	protected $stringStorageType;

	/**
	 * @return string
	 */
	public function getStorageType():string
	{
		return $this->stringStorageType;
	}


	/**
	 * @param string $stringStorageName
	 * @return Cache
	 */
	public static function createCache(string $stringStorageName = ''):Cache{
		
		// Тут будем хранить объект для хранения данных
		/** @var Storage $storageObject */
		$storageObject = null;

		// Перебираем доступные методы хранения по очереди
		foreach(static::$arrayStorageType as $className){

			// Проверяем доступен ли данный метод хранения
			if(!$className::available()){
				continue;
			}

			// Если доступен создаём экземпляр его сласса
			switch($className){
				case Memcache::class:
					$storageObject = new Memcache(static::$stringMemcacheHost, static::$intMemcachePort);
					break;

				case Fs::class:
					$storageObject = new Fs(static::$stringFsPath);
					break;

				case No::class:
					$storageObject = new No();
					break;
			}

			// Проверяем удалось ли подключиться к хранилищу
			if($storageObject->isConnect()){
				// Если удалось то можно прекратить поиски
				break;
			}

			// Если подключиться не удалось то удаляем объект
			// и пытаемся подключиться к следующему в очереди объекту
			$storageObject = null;
		}

		// Создаём экземпляр класса Cache
		return new static($storageObject, $stringStorageName);
	}

	/**
	 * Инициируем как protected для защиты
	 *
	 * @param Storage $storageObject
	 * @param string $stringStorageName
	 */
	protected function __construct(Storage $storageObject, string $stringStorageName = ''){
		$this->storageObject = $storageObject;
		$this->storageObject->setStorageName($stringStorageName);
		$this->stringStorageType = get_class($this->storageObject);
	}

	/**
	 * Защита объекта от клонирования
	 */
	private function __clone(){}


	/**
	 * Получить значение из кеша по его ключу
	 *
	 * @param string $stringKey Ключ
	 * @param float $floatTimeCache Считать значение устаревщим если оно старше этого времени
	 * @param int $intStatus Статус завершения
	 * @param float $floatHowOldTime Переменная для записи времени на которое просрочен кеш
	 * @return mixed Результаты из хранилища
	 */
	public function get(
		string $stringKey,
		float $floatTimeCache = 0,
		int &$intStatus = self::STATUS_EMPTY,
		float &$floatHowOldTime = 0
	){

		// Получаем данные из хранилища
		$mixedResult = $this->storageObject->get($stringKey, $intStatus);

		// Вынимаем данные из объекта значения с проверкой на устаревание
		return $this->getValue(
			$mixedResult instanceof Value ? $mixedResult : null,
			$floatTimeCache,
			$intStatus,
			$floatHowOldTime
		);
	}


	/**
	 * Запись значения в кеш
	 *
	 * @param string $stringKey Ключ
	 * @param mixed $mixedValue Значение
	 * @return bool
	 */
	public function set(string $stringKey, $mixedValue):bool{
		return $this->storageObject->set($stringKey, $this->setValue($stringKey, $mixedValue));
	}


	/**
	 * Удаление переменной из зранилища по её ключу
	 *
	 * @param string $stringKey
	 */
	public function remove(string $stringKey){
		$this->storageObject->remove($stringKey);
	}


	/**
	 * Проверка объекта хранилища на устаревание и возврат значения если оно не устарело
	 *
	 * @param \q\cache\Value $valueObject
	 * @param float $floatTimeCache
	 * @param int $intFlag
	 * @param float $floatHowOldTime
	 * @return mixed
	 */
	protected function getValue(
		Value $valueObject = null,
		float $floatTimeCache,
		int &$intFlag,
		float &$floatHowOldTime
	){

		// В кеше ничего нет
		if(null === $valueObject){
			$intFlag = self::STATUS_EMPTY;
			return null;
		}

		// Время на которое устарел кеш
		$floatHowOldTime = microtime(true) - $valueObject->floatTime - $floatTimeCache;

		if(
			// Время проведённое в кеше вообще имеет значение
			$floatTimeCache > 0 and

			// И данные провели в кеше больше отведённого времени
			$floatHowOldTime > 0
		){
			// Меняем статус
			$intFlag = self::STATUS_OLD;
		}

		// Возвращаем результат
		return $valueObject->mixedValue;
	}

	/**
	 * Создание объекта хранилища
	 *
	 * @param string $stringKey
	 * @param $mixedValue
	 * @return \q\cache\Value
	 */
	protected function setValue(string $stringKey, $mixedValue):Value{
		return new Value($stringKey, $mixedValue, microtime(true));
	}
	

}

// Устанавливаем значение по умолчанию в статическую
// переменную класса сразу после его объявления
if(empty(Cache::$stringFsPath)) {
	Cache::$stringFsPath = sys_get_temp_dir();
}