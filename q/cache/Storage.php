<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.2017
 * Time: 13:04
 */

namespace q\cache;
use \q\Cache;

/**
 * Абстрактный класс описывающий интерфейс доступа
 * к разныи типам хрпнилищ.
 *
 * @package q\cache
 */
abstract class Storage
{
	/**
	 * Функция для сохранения значения
	 *
	 * @param string $stringKey
	 * @param $mixedValue
	 * @return bool
	 */
	abstract public function set(string $stringKey, $mixedValue):bool;

	/**
	 * Функция для извлечения значения по ключу или по массиву ключей
	 *
	 * @param string $stringKey Ключ или массив ключей
	 * @param int &$intStatus Код или массив кодов заверщения
	 * @return mixed
	 */
	abstract public function get(string $stringKey, int &$intStatus = Cache::STATUS_EMPTY);


	/**
	 * Удаление переменной из хранилища по её ключу
	 *
	 * @param string $stringKey
	 * @return void
	 */
	abstract public function remove(string $stringKey);


	/**
	 * @var string имя хранилища
	 */
	protected $stringStorageName = '';

	/**
	 * Устанавливает имя для хранилища данных
	 *
	 * @param string $stringStorageName
	 * @return void
	 */
	public function setStorageName(string $stringStorageName){
		$this->stringStorageName = $stringStorageName;
	}

	/**
     * Проверка установленны ли нужные расщирения PHP для рпботы данного хранилища
	 * @return bool
	 */
	public static function available():bool{
		return true;
	}


	/**
	 * Проверка работает ли подключение к хранилищу
	 * @return bool
	 */
	public function isConnect():bool{
		return true;
	}

	/**
	 * Генерация ключа с которым должна храниться переменная.
	 * Генерируется основываясь на названии хранилища и ключа переменной.
	 *
	 * @param string $stringKey Ключ переменной
	 * @return string
	 */
	public function getHashKey(string $stringKey):string{
		return 'q_cache_' . md5($this->stringStorageName) . '_' . md5($stringKey);
	}
}

