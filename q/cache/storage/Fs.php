<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.2017
 * Time: 15:11
 */

namespace q\cache\storage;
use q\cache\Storage;
use q\Cache;


/**
 * Class Fs
 * @package q\cache\storage
 */
class Fs extends Storage
{

    /**
     * @var string Путь до директории с файлами кеша
     */
    protected $stringPath;

    /**
     * @var bool Указывает удалось ли найти указынный каталог
     */
    protected $boolConnect = false;

    /**
     * Fs constructor.
     * @param string $stringPath Путь до директории в которой храниятся файлы кеша
     */
    public function __construct(string $stringPath)
	{
	    // Удаляем лишнее
		$stringPath = \q\fs\realPath($stringPath);

		// Проверяем сеществует ли каталог
		if(!is_dir($stringPath)){
			return;
		}

		// Все ок, записываем настройки
		$this->stringPath = $stringPath;
		$this->boolConnect = true;
	}

    /**
     * Проверка работает ли подключение к хранилищу
     * @return bool
     */
    public function isConnect():bool{
		return $this->boolConnect;
	}

    /**
     * Полный путь до файла в котором будет храниться конкретная переменная
     *
     * @param string $stringName
     * @return string
     */
    protected function hashName(string $stringName):string{
		return $this->stringPath . DIRECTORY_SEPARATOR . $this->getHashKey($stringName);
	}

	/**
	 * Функция для сохранения значения
	 *
	 * @param string $stringKey
	 * @param $mixedValue
	 * @return bool
	 */
	public function set(string $stringKey, $mixedValue):bool
	{
		// Если у нас нет доступа к директории для записи, то ничего и не запишется
		if(!$this->isConnect()){
			return false;
		}

		// Полный путь до файла с кешем
		$stringFilePath = $this->hashName($stringKey);

		// Записываем данные в файл
		$mixedResult = file_put_contents($stringFilePath, serialize($mixedValue));

		// Проверяем результат записи со сравнением типов для возврата корректного результата
		return $mixedResult !== false;
	}




	/**
	 * Функция для извлечения значения по ключу или по массиву ключей
	 *
	 * @param string $stringKey Ключ или массив ключей
	 * @param int &$intStatus Код или массив кодов заверщения
	 * @return mixed
	 */
	public function get(string $stringKey, int &$intStatus = Cache::STATUS_EMPTY)
	{

		// Если у нас нет доступа к директории для записи, то ничего и не запишется
		if(!$this->isConnect()){
			$intStatus = Cache::STATUS_ERROR;
			return false;
		}

		// Полный путь до файла с кешем
		$stringFilePath = $this->hashName($stringKey);

		// Файл не существует возвращаем ошибку
		if(!file_exists($stringFilePath)){
			return false;
		}

		// Достаём данные из файла
		$mixedResult = @unserialize(file_get_contents($stringFilePath), []);

		// Записываем статус удачного выполнения
		$intStatus = Cache::STATUS_OK;

		// Возвращаем данные
		return $mixedResult;
	}

	/**
	 * Удаление переменной из хранилища по её ключу
	 *
	 * @param string $stringKey
	 * @return void
	 */
	public function remove(string $stringKey)
	{
		@unlink($this->hashName($stringKey));
	}
}