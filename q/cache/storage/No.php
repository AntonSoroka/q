<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.2017
 * Time: 15:56
 */

namespace q\cache\storage;
use q\Cache;
use \q\cache\Storage;

/**
 * Класс пустышка если не удалось инициализировать ни один другой
 * 
 * @package q\cache\storage
 */
class No extends Storage
{

	/**
	 * Функция для сохранения значения
	 *
	 * @param string $stringKey
	 * @param $mixedValue
	 * @return bool
	 */
	public function set(string $stringKey, $mixedValue):bool
	{
		return false;
	}

	/**
	 * Функция для извлечения значения по ключу или по массиву ключей
	 *
	 * @param string $stringKey Ключ или массив ключей
	 * @param int &$intStatus Код или массив кодов заверщения
	 * @return mixed
	 */
	public function get(string $stringKey, int &$intStatus = Cache::STATUS_EMPTY)
	{
		return false;
	}

	/**
	 * Удаление переменной из хранилища по её ключу
	 *
	 * @param string $stringKey
	 * @return void
	 */
	public function remove(string $stringKey){}
}