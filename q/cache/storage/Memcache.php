<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.2017
 * Time: 13:10
 */

namespace q\cache\storage;

use \q\Cache;
use \q\cache\Storage;


/**
 * Class Memcache
 *
 * Install memcache https://commaster.net/content/installing-memcached-windows
 *
 * @package q\cache\storage
 */
class Memcache extends Storage
{

    /**
     * @var \Memcache
     */
    protected $memcache;
    /**
     * @var bool
     */
    protected $connect = false;

    /**
     * Memcache constructor.
     * @param string $stringHost
     * @param int $intPort
     */
    public function __construct(string $stringHost, int $intPort = 11211)
	{
		if(!static::available()){
			return;
		}
		$this->memcache = new \Memcache();
		$this->connect = @$this->memcache->connect($stringHost, $intPort);
	}


    /**
     * Функция для сохранения значения
     *
     * @param string $stringKey
     * @param $mixedValue
     * @return bool
     */
    public function set(string $stringKey, $mixedValue):bool
	{
		if(!$this->connect){
			return false;
		}
		//echo 'set: ' . $stringKey . ', ' . print_r($this->memcache->get($stringKey), true) . "\n";

		return $this->memcache->set($stringKey, $mixedValue);
	}

    /**
     * Функция для извлечения значения по ключу или по массиву ключей
     *
     * @param string $stringKey Ключ или массив ключей
     * @param int &$intStatus Код или массив кодов заверщения
     * @return mixed
     */
    public function get(string $stringKey, int &$intStatus = Cache::STATUS_EMPTY)
	{
		//echo 'get: ' . $mixedKey . ', ' . print_r($this->memcache->get($mixedKey), true) . "\n";

		if(!$this->isConnect()){
			$intStatus = Cache::STATUS_ERROR;
			return false;
		}

		// Статус хранения в кеше
		$boolStatus = null;

		// Получаем данные из хранилища
		$arrayResult = $this->memcache->get($stringKey, $boolStatus);

		if($boolStatus !== false){
			$intStatus = Cache::STATUS_OK;
		}

		return $arrayResult;
	}


    /**
     * Проверка установленны ли нужные расщирения PHP для рпботы данного хранилища
     * @return bool
     */
    public static function available():bool
	{
		return class_exists('\Memcache');
	}

    /**
     * Проверка работает ли подключение к хранилищу
     * @return bool
     */
    public function isConnect():bool
	{
		return null === $this->memcache || $this->connect === false ? false : true;
	}

	/**
	 * Удаление переменной из хранилища по её ключу
	 *
	 * @param string $stringKey
	 * @return void
	 */
	public function remove(string $stringKey)
	{
		$this->memcache->delete($stringKey);
	}
}