<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 22.03.2017
 * Time: 8:00
 */

namespace q\session;

function start():bool{
	
	// Если сессия уже запущена, выходим
	if(session_id()){
		return true;
	}

	// Запускаем сессию
	session_start();

	// Если сессия запустилась
	if(session_id()){

		// Читаем время жизни сессий
		$intCookieLifetime = (int)ini_get('session.cookie_lifetime');

		// Если время хранения не равно нулю то обновляем куку
		if($intCookieLifetime != 0){
			setcookie(session_name(), session_id(), time() + $intCookieLifetime, '/');
		}

		// Все удачно закончилось возвращаем true
		return true;
	}

	// Не удалось звпустить сессию
	else{
		return false;
	}
}