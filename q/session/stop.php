<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 15:52
 */
namespace q\session;

function stop(){

	// Если сессия не запущена то её нужно запустить
	// иначе нельзя будет отчистить данные текущей сессии
	if(!session_id()){
		\q\session\start();
	}

	// Если сессия запустилась
	if(session_id()){

		// Обновляем session_id
		session_regenerate_id();

		// Ставим дату истечения сессиии 0 т.е. 1970-01-01 00:00:00,
		// это заставит удалиться куку связанную с данной сессией
		setcookie(session_name(), session_id());

		// Удаляем данные из сессии
		session_unset();

		// Останавливаем сессию
		session_destroy();
	}
}