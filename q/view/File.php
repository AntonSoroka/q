<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.07.2017
 * Time: 16:08
 */

namespace q\view;


use q\View;

class File{
	/**
	 * @var int Приоритет
	 */
	public $intPriority = 0;

	/**
	 * @var View Объект связанный с файлом
	 */
	public $bemObject;

	/**
	 * @var string Расширение файла
	 */
	public $stringExtension;

	/**
	 * @var bool Указывает обязательно ли грузить файл или нет
	 */
	public $boolMatter;

	/**
	 * @var int Id текущего файла
	 */
	public $intId;

	/**
	 * @var int Id следующего файла
	 */
	private static $intCurrentId = 0;

	public function __construct(){
		$this->intId = static::$intCurrentId;
		static::$intCurrentId++;
	}
}