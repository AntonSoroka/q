<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.07.2017
 * Time: 12:02
 */

namespace q;



use q\view\Files;

class View{

	/**
	 * @var string Директория проекта
	 */
	public $__rootPath;

	/**
	 * @var string Ссылка
	 */
	public $__rootUrl;

	/**
	 * @var string Ссылка на текущий каталог
	 */
	public $__url;

	/**
	 * @var string Путь к текущему каталогу
	 */
	public $__path;

	/**
	 * @var View Родительский элемент
	 */
	public $__parent;

	/**
	 * @var View Корневой элемент
	 */
	public $__root;

	/**
	 * @var Files
	 */
	protected $__css;

	/**
	 * @var Files
	 */
	protected $__js;

	/**
	 * @var App Приложение
	 */
	private $__app ;

	/**
	 * View constructor.
	 * @param string $stringPath
	 * @param string $stringUrl
	 * @param View $viewParent
	 */
	private function __construct(string $stringPath, string $stringUrl, View $viewParent = null){

		if(null === $viewParent){
			$this->__rootPath = $stringPath;
			$this->__rootUrl = $stringUrl;
			$this->__parent = $this;
			$this->__root = $this;
		}else{
			$this->__rootPath = $viewParent->__rootPath;
			$this->__rootUrl = $viewParent->__rootUrl;
			$this->__parent = $viewParent;
			$this->__root = $viewParent->__root;
		}

		$this->__path = $stringPath;
		$this->__url = $stringUrl;
	}

	/**
	 * Создание корневого элемента
	 *
	 * @param string $stringPath
	 * @param string $stringUrl
	 * @param App $appObject
	 * @return View
	 * @throws \LogicException
	 */
	public static function __create(string $stringPath, string $stringUrl, App $appObject){

		// Путь существует
		if(!is_dir($stringPath)){
			throw new \LogicException("Directory {$stringPath} not exists.");
		}

		// Меняем слеши на юниксовые
		$stringPath = str_replace('\\', '/', $stringPath);

		// Добавляем слеш в конце пути
		if($stringPath[-1] !== '/'){
			$stringPath .= '/';
		}

		// Информация по ссылке
		$arrayUrlInfo = parse_url($stringUrl);

		// Строим ссылку
		$stringUrl = (
			(isset($arrayUrlInfo['scheme']) ? "{$arrayUrlInfo['scheme']}://" : '//') .
			$arrayUrlInfo['host'] .
			(isset($arrayUrlInfo['port']) ? ":{$arrayUrlInfo['port']}" : '') .
			($arrayUrlInfo['path'] ?? '/') .
		'');

		// Добавляем слеш в конце ссылки
		if($stringUrl[-1] !== '/'){
			$stringUrl .= '/';
		}

		// Создаём корневой элемент
		$viewObject = new View($stringPath, $stringUrl);

		// Инициируем хранилище CSS и JS файлов для корневого элемента
		$viewObject->__js = new Files();
		$viewObject->__css = new Files();
		$viewObject->__app = $appObject;

		// Возвращаем корневой элемент
		return $viewObject;
}

	/**
	 * Генерируем дочерний элемент
	 *
	 * @param $stringName
	 * @return View
	 */
	public function __get($stringName):View{
		$stringPath = $this->__path . $stringName . '/';
		$stringUrl = $this->__url . $stringName . '/';
		$objectView = new View($stringPath, $stringUrl, $this);
		$objectView->__app = $this->__root->__app;
		return $objectView;
	}

	/**
	 * Вызываем дочернюю функцию
	 *
	 * @param $stringName
	 * @param $arrayArguments
	 * @return mixed
	 */
	public function __call($stringName, $arrayArguments){
		/** @var View $viewObject */
		$viewObject = $this->{$stringName};
		return $viewObject->__callFunction($arrayArguments);
	}

	/**
	 * @var \Closure[] Закешированные функции
	 */
	protected static $__arrayCache = [];

	/**
	 * Выполнение функции привязанной к дереву с нужными аргументами
	 *
	 * @param array $arrayArguments
	 * @return mixed
	 * @throws \LogicException
	 */
	public function __callFunction(array $arrayArguments){

		// PHP файл
		$stringFilePhp = $this->__path . basename($this->__path) . '.php';

		// Если функция нет в кеше то загружаем её
		if(!isset(static::$__arrayCache[$stringFilePhp])){

			// Целевого каталога не существует
			if(!is_dir($this->__path)){
				throw new \LogicException("Folder {$this->__path} not exists");
			}

			// Если файл существует, то проверяем его
			if(file_exists($stringFilePhp)){

				// Загружаем файл
				$functionLoad = include $stringFilePhp;

				// Файл должен возвращать функцию, если это не так генерируем исключение
				if(!is_callable($functionLoad)){
					throw new \LogicException("File {$stringFilePhp} does not return a function");
				}

				// Записываем функцию в кеш
				static::$__arrayCache[$stringFilePhp] = $functionLoad->bindTo($this);

			}

			// Если файла не существует то записываем функцию пустышку
			else{
				static::$__arrayCache[$stringFilePhp] = function(){
				};
			}
		}

		// Подключаем JS и CSS файлы
		$this->__loadCss(0, false);
		$this->__loadJs(0, false);

		// Извлекаем указатель на функцию из кеша
		$functionPhp = &static::$__arrayCache[$stringFilePhp];

		// Выполняем функцию с переданными параметрами
		return $functionPhp(...$arrayArguments);
	}

	/**
	 * Загрузка связанных с деревом css файла
	 * @param int $intPriority
	 * @param bool $boolMatter
	 */
	public function __loadCss(int $intPriority = 0, bool $boolMatter = true){

		// Добавляем CSS файл
		$this->__root->__css->add($this, 'css', $intPriority, $boolMatter);
	}

	/**
	 * Загрузка связанных с деревом css файла
	 * @param int $intPriority
	 * @param bool $boolMatter
	 */
	public function __loadJs(int $intPriority = 0, bool $boolMatter = true){

		// Добавляем JS файл
		$this->__root->__js->add($this, 'js', $intPriority, $boolMatter);
	}

	/**
	 * Возвращаем массив ссылок на JS файлы
	 *
	 * @return array
	 */
	public function __getJs():array{
		return $this->__root->__js->get();
	}

	/**
	 * Возвращаем массив ссылок на Css файлы нуж
	 *
	 * @return array
	 */
	public function __getCss():array{
		return $this->__root->__js->get();
	}

	/**
	 * Загрузка шаблона
	 *
	 * @param string $stringTemplateName Имя шаблона
	 * @param array ...$arrayParameters Параметры для генерации шаблона
	 * @return mixed
	 * @throws \LogicException
	 */
	public function __load(string $stringTemplateName, ...$arrayParameters){

		// Приводим путь к нормальному виду
		$stringTemplateName = \q\fs\joinPath($stringTemplateName);

		// Вызываем функцию
		return static::__loadView($this, $stringTemplateName)->__callFunction($arrayParameters);
	}

	/**
	 * Загрузка стилей связанных с шаблоном
	 *
	 * @param string $stringTemplateName
	 * @param int $intPriority
	 * @param bool $boolMatter
	 * @throws \LogicException
	 */
	public function __loadViewCss(string $stringTemplateName, int $intPriority = 0, bool $boolMatter = true){
		static::__loadView($this, $stringTemplateName)->__loadCss($intPriority, $boolMatter);
	}

	/**
	 * Загрузка скриптов связанных с шаблоном
	 *
	 * @param string $stringTemplateName
	 * @param int $intPriority
	 * @param bool $boolMatter
	 * @throws \LogicException
	 */
	public function __loadViewJs(string $stringTemplateName, int $intPriority = 0, bool $boolMatter = true){
		static::__loadView($this, $stringTemplateName)->__loadJs($intPriority, $boolMatter);
	}

	/**
	 * Загрузка шаблона относительно переданного View объекта
	 *
	 * @param View $self
	 * @param string $stringTemplateName
	 * @return mixed
	 * @throws \LogicException
	 */
	protected static function __loadView(View $self, string $stringTemplateName):View{

		// Нужна родительская папка
		if(preg_match('#^\.\./(?<path>.*)$#usi', $stringTemplateName, $arrayMatches)){
			return static::__loadView($self->__parent, $arrayMatches['path']);
		}

		// Нужна текущая папка
		if(preg_match('#^\.\/(?<path>.*)$#usi', $stringTemplateName, $arrayMatches)){
			return static::__loadView($self, $arrayMatches['path']);
		}

		// Нужна родительская папка
		if(preg_match('#^\/(?<path>.*)$#usi', $stringTemplateName, $arrayMatches)){
			return static::__loadView($self->__root, $arrayMatches['path']);
		}

		// Нужно продвинуться но одну директорию внутрь
		if (preg_match('#^(?<part>[^\/]+)\/(?<path>.*)$#usi', $stringTemplateName, $arrayMatches)){
			return static::__loadView($self->{$arrayMatches['part']}, $arrayMatches['path']);
		}

		// Нужно выполнить искомую функцию
		if(preg_match('#^(?<name>[^\/]+)\/?$#usi', $stringTemplateName, $arrayMatches)){
			return $self->{$arrayMatches['name']};
		}

		// Что-то не так с этим именем
		throw new \LogicException("Bad template path: $stringTemplateName");

	}
}
