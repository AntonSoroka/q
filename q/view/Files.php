<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.07.2017
 * Time: 16:36
 */

namespace q\view;


use q\View;

class Files{

	/**
	 * @var File[]
	 */
	private $arrayFiles = [];

	/**
	 * @param View $viewObject
	 * @param string $stringExtension
	 * @param int $intPriority
	 * @param bool $boolMatter
	 */
	public function add(View $viewObject, string $stringExtension, int $intPriority = 0, bool $boolMatter = true){

		// Путь к файлу
		$stringPath = $viewObject->__path . basename($viewObject->__path) . '.' . $stringExtension;

		// Файл уже есть
		if(isset($this->arrayFiles[$stringPath])){
			$this->arrayFiles[$stringPath]->intPriority = $intPriority;

			if($this->arrayFiles[$stringPath]->boolMatter === false && $boolMatter){
				$this->arrayFiles[$stringPath]->boolMatter = true;
			}
		}

		// Файл ещё небыл загружен
		else{
			$file = new File();
			$this->arrayFiles[$stringPath] = $file;

			$file->boolMatter = $boolMatter;
			$file->bemObject = $viewObject;
			$file->intPriority = $intPriority;
			$file->stringExtension = $stringExtension;
		}
	}

	public function get():array{

		// Копируем массив чьл-юы отсортировать его не влияя на изначальный
		$arrayFiles = $this->arrayFiles;

		// Сортируем файлы согласно из приоритету и порядку создания
		usort(
			$arrayFiles, function($fileObject1, $fileObject2){
			if($fileObject1->intPriority === $fileObject2->intPriority){
				return $fileObject1->intId <=> $fileObject2->intId;
			}else{
				return $fileObject1->intPriority <=> $fileObject2->intPriority;
			}
		});

		// Все ссылки на файлы
		$arrayUrlList = [];

		// Перебираем файлы
		foreach($arrayFiles as $fileObject){

			// Имя файла
			$stringFileName = basename($fileObject->bemObject->__path) . '.' . $fileObject->stringExtension;

			// Ссылка на файл
			$stringUrl = $fileObject->bemObject->__url . $stringFileName;
			// Путь к файлу
			$stringPath = $fileObject->bemObject->__path . $stringFileName;

			// Существует ли файл
			$boolFileExists = file_exists($stringPath);

			// Если файл не существует и он обязательный, генерируем исключение
			if(!$boolFileExists && $fileObject->boolMatter){
				throw new \LogicException("File {$stringUrl} not exists.");
			}

			// Если файл не существует и он не обязательный, то пропускаем его
			else if(!$boolFileExists){
				continue;
			}

			// Добавляем ссылку к списку
			$arrayUrlList[$stringPath] = $stringUrl;
		}

		// Возвращаем ссылки на все файлы
		return  $arrayUrlList;
	}
}