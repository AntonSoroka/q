<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 05.07.2017
 * Time: 11:05
 */

use q\Color;
use PHPUnit\Framework\TestCase;



class ColorTest extends TestCase{
	public function test(){

		// Непрозрачный
		$color = new Color(128, 128, 128);

		$this->assertEquals('808080', $color->toHex());
		$this->assertEquals('#808080', $color->toCssHex());
		$this->assertEquals('rgb(128,128,128)', $color->toCssRgb());
		$this->assertEquals('rgba(128,128,128,1)', $color->toCssRgba());

		// Наполовину прозрачный на стандартном белом фоне
		$color = new Color(128, 128, 128, 0.5);

		$this->assertEquals('C0C0C0', $color->toHex());
		$this->assertEquals('#C0C0C0', $color->toCssHex());
		$this->assertEquals('rgb(192,192,192)', $color->toCssRgb());
		$this->assertEquals('rgba(128,128,128,0.5)', $color->toCssRgba());

		// Наполовину прозрачный на чёрном фоне
		$color = new Color(128, 128, 128, 0.5, new Color(0,0,0));

		$this->assertEquals('404040', $color->toHex());
		$this->assertEquals('#404040', $color->toCssHex());
		$this->assertEquals('rgb(64,64,64)', $color->toCssRgb());
		$this->assertEquals('rgba(128,128,128,0.5)', $color->toCssRgba());

		// Цвета для смешивания
		$colorWhite = new Color(255,255,255);
		$colorBlack = new Color(0,0,0);

		// Смешиваем напопалам
		$colorAverage = Color::average($colorWhite, $colorBlack, 0.5);
		$this->assertEquals('rgba(128,128,128,1)', $colorAverage->toCssRgba());

		// Чёрный на максимум
		$colorAverage = Color::average($colorWhite, $colorBlack, 1);
		$this->assertEquals('rgba(0,0,0,1)', $colorAverage->toCssRgba());

		// Белый на максимум
		$colorAverage = Color::average($colorWhite, $colorBlack, 0);
		$this->assertEquals('rgba(255,255,255,1)', $colorAverage->toCssRgba());
	}
}
