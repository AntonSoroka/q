<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 31.03.2017
 * Time: 15:43
 */

namespace q\tests;



$stringConfigLocalPath = __DIR__ . '/config.local.php';

if(!file_exists($stringConfigLocalPath)){
	
	echo 'EDIT YOU LOCAL CONFIG IN ' . $stringConfigLocalPath;
	
	$stringContent = <<<'PHP'
<?php
namespace q\tests;

const MEMCACHE_HOST = 'localhost';
const MEMCACHE_POST = 11211;

const SQL_USER = 'root';
const SQL_PASS = '';
const SQL_BASE = 'test';
const SQL_HOST = 'localhost';
const SQL_PORT = 3306;

PHP;
	
	file_put_contents($stringConfigLocalPath, $stringContent);
	
	exit();
}
require_once $stringConfigLocalPath;
require_once __DIR__ . '/./../autoload.php';




