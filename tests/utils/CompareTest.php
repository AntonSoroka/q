<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:43
 */

namespace q\tests\utils;


class CompareTest extends \PHPUnit\Framework\TestCase
{
	public function testCompare(){
		$arrayVariant1 = [
			'string' => 'str',
			'int' => 1,
			'double' => 1.5,
			'stringInt' => '1',
			'stringDouble' => '1.5',
			'array' => [1,2,3],
			'object' => (object)[
				'var1' => 1,
				'var2' => 1.0,
			],
		];
		$arrayVariant2 = [
			'string' => 'str',
			'int' => '1',
			'double' => '1.5',
			'stringInt' => 1,
			'stringDouble' => 1.5,
			'array' => (object)[1,2,3],
			'object' => [
				'var1' => '1',
				'var2' => '1.0',
			],
		];

		$this->assertTrue(\q\utils\compare($arrayVariant1, $arrayVariant2));
		$this->assertFalse(\q\utils\compare($arrayVariant1, $arrayVariant2, true));


		$arrayVariant1 =         ['var' => 1,'var2' => 2, 'var3' => 3];
		$arrayVariant2 = (object)['var' => 1,'var2' => 2, 'var3' => 3];

		$this->assertTrue(\q\utils\compare($arrayVariant1, $arrayVariant2));
		$this->assertFalse(\q\utils\compare($arrayVariant1, $arrayVariant2, true));

		$this->assertTrue(\q\utils\compare([1,2,3], [3,2,1]));
		$this->assertFalse(\q\utils\compare([1,2,3], [3,2,1], true));
		$this->assertFalse(\q\utils\compare([1,1,1,2], [2,2,2,1]));
		
	}
}
