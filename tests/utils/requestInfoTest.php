<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.08.2017
 * Time: 14:26
 */

namespace q\tests\utils;

use PHPUnit\Framework\TestCase;
use q\utils\Server;

class requestInfoTest extends TestCase{

	public function testRequestInfo(){

		$server = new Server(__DIR__ . '/requestInfo.php', 8888, '127.0.0.1');
		$server->start();

		$stringResult = file_get_contents('http://127.0.0.1:8888/tests/utils/request/url?q=1');
		$arrayInfo = json_decode($stringResult, true);

		$this->assertEquals(8888, $arrayInfo['port']);
		$this->assertEquals('127.0.0.1', $arrayInfo['ip']);
		$this->assertEquals('http', $arrayInfo['scheme']);
		$this->assertEquals('127.0.0.1', $arrayInfo['host']);
		$this->assertEquals('http://127.0.0.1:8888/tests/utils/', $arrayInfo['url']);
		$this->assertEquals('http://127.0.0.1:8888/tests/utils/request/url', $arrayInfo['url_request']);
		$this->assertEquals('http://127.0.0.1:8888/tests/utils/request/url?q=1', $arrayInfo['url_full']);

	}
}
