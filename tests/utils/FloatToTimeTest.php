<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 27.11.2017
 * Time: 13:25
 */

namespace q\tests\utils;

use PHPUnit\Framework\TestCase;

class FloatToTimeTest extends TestCase{

	public function testFloatToTime(){
		$arrayData = [
			'1:00:00' => [60*60],
			'1:00:00.001' => [60*60 + 0.001, true],
			'1:00:00.000' => [60*60, true],
			'1:00' => [60],
			'1:00.000' => [60, true],
			'1' => [1],
			'1.000' => [1, true],
			'12 23:59:59.999' => [0.999 + 59 + 59*60 + 60*60*23 + 60*60*24*12, true],
		];

		foreach($arrayData as $stringResult => $arrayParams){
			$this->assertEquals($stringResult, \q\utils\floatToTime(...$arrayParams));
		}
	}
}
