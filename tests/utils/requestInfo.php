<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.08.2017
 * Time: 14:27
 */

include(__DIR__ . './../../q/fs/realPath.php');
include(__DIR__ . './../../q/fs/joinPath.php');
include(__DIR__ . './../../q/net/portDefaultScheme.php');
include(__DIR__ . './../../q/net/schemeDefaultPort.php');
include(__DIR__ . './../../q/utils/requestInfo.php');

$arrayInfo = \q\utils\requestInfo();
$arrayInfo['_SERVER'] = $_SERVER;

echo json_encode($arrayInfo);