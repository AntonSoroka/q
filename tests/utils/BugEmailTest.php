<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 08.11.2017
 * Time: 8:37
 */

namespace q\tests\utils;

use PHPUnit\Framework\TestCase;
use q\utils\Mail;

class BugEmailTest extends TestCase{

	public function testSend(){

		$mock = $this->createMock(Mail::class);
		$mock->method('send')->will($this->onConsecutiveCalls(
			true,
			false,
			false
		));

		// Это письмо как будто отправилось
		$boolResult = \q\utils\bugEmail(
			'ugin_root@mail.ru',
			'uginroot@gmail.com',
			'Test',
			'Test',
			0.1,
			$mock
		);
		$this->assertTrue($boolResult);

		// Это письмо не должно отправиться, так так уже отправлялось
		$boolResult = \q\utils\bugEmail(
			'ugin_root@mail.ru',
			'uginroot@gmail.com',
			'Test',
			'Test',
			0.1,
			$mock
		);
		// Но результатом будет true если функция скажет что письмо было отправлено меньше одной десятой секунды назад
		$this->assertTrue($boolResult);


		// Письмо не отправилось
		$boolResult = \q\utils\bugEmail(
			'ugin_root@mail.ru',
			'uginroot@gmail.com',
			'Test2',
			'Test2',
			0.1,
			$mock
		);
		// Ожидаем неудачу
		$this->assertFalse($boolResult);

	}
}
