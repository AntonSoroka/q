<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 12:03
 */

namespace q\tests\utils;


class LockTest extends \PHPUnit\Framework\TestCase
{
	public function testLock(){
		// Префикс и ключ
		$stringPrefix = 'q-tests';
		$stringKey = 'lock-tests-key';

		// Путь до файла блокировки
		$stringPath = \q\utils\lock\getPath($stringPrefix, $stringKey);

		// Устанавливаем префикс
		\q\utils\lock($stringPrefix);

		$boolFirstLockIn = false;
		$boolFirstLock = \q\utils\lock($stringKey, function() use ($stringKey, &$boolFirstLockIn){

			$boolFirstLockIn = true;
			$boolSecondLockIn = false;

			$boolSecondLock = \q\utils\lock($stringKey, function() use (&$boolSecondLockIn){
				$boolSecondLockIn = true;
			});
			
			$this->assertFalse($boolSecondLock);
			$this->assertEquals($boolSecondLockIn, $boolSecondLock);
		});

		$this->assertTrue($boolFirstLock);
		$this->assertEquals($boolFirstLockIn, $boolFirstLock);

		// Удаляем файл блокировки
		@unlink($stringPath);
	}
}
