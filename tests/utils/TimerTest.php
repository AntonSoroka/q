<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.11.2017
 * Time: 11:47
 */

namespace q\tests\utils;

use PHPUnit\Framework\TestCase;

class TimerTest extends TestCase{

	public function testTimer(){

		$floatSleep = 0.01;
		$intPercent = 10;

		// Инициируем
		\q\utils\timer('test');

		// Спим
		usleep($floatSleep * 1000000);

		// Получаем прошедшее время
		$floatProcessTime = \q\utils\timer('test');


		// Проверяем
		$this->assertTrue(
			$floatProcessTime > $floatSleep - ($floatSleep / $intPercent) &&
			$floatProcessTime < $floatSleep + ($floatSleep / $intPercent)
		);

		// Спим ещё
		usleep($floatSleep * 1000000);

		// Получаем прошедшее время
		$floatProcessTime = \q\utils\timer('test');

		// Проверяем правильно ли считает от начала
		$this->assertTrue(
			$floatProcessTime > ($floatSleep * 2) - (($floatSleep / $intPercent) * 2) &&
			$floatProcessTime < ($floatSleep * 2) + (($floatSleep / $intPercent) * 2)
		);

		// Спим ещё
		usleep($floatSleep * 1000000);

		// Получаем прошедшее время
		$floatProcessTime = \q\utils\timer('test', true);

		// Проверяем правильно ли считает от последней проверки
		$this->assertTrue(
			$floatProcessTime > $floatSleep - ($floatSleep / $intPercent) &&
			$floatProcessTime < $floatSleep + ($floatSleep / $intPercent)
		);
	}
}
