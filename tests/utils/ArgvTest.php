<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.2017
 * Time: 14:00
 */

namespace q\tests\utils;

use Arvg;
use PHPUnit\Framework\TestCase;

class ArgvTest extends TestCase{

	public function testArgv(){

		$stringFilePath = __DIR__ . '/argv.php';

		$stringResult = `php -f {$stringFilePath} -- -v v 1 -d -n n -n n -n`;

		$arrayExpected = [
			$stringFilePath,
			'v' => 'v',
			'1',
			'd' => null,
			'n' => [
				'n',
				'n',
				null
			]
		];

		$this->assertEquals($arrayExpected, json_decode($stringResult, true));

	}
}
