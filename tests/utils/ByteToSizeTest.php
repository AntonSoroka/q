<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28.11.2017
 * Time: 9:40
 */

namespace q\tests\utils;

use ByteToSize;
use PHPUnit\Framework\TestCase;

class ByteToSizeTest extends TestCase{

	public function testByteToText(){
		$arrayTest = [
			'0 байт' => [0],
			'550 байт' => [550],
			'1 Кб' => [1024, 0],
			'2 Кб' => [1024 * 2, 2],
			'2.5 Кб' => [1024 * 2.5, 2],
		];

		foreach($arrayTest as $resultString => $arrayParams){
			$this->assertEquals($resultString, \q\utils\byteToSize(...$arrayParams));
		}
	}
}
