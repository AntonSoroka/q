<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 31.03.2017
 * Time: 14:12
 */

namespace q\tests;

use PHPUnit\Framework\TestCase;
use q\orm\db\table\column\where\Data;
use q\orm\db\table\type\Integer;
use q\Orm;
use q\orm\Db;
use q\orm\db\Table;
use q\orm\db\table\Clause;
use q\sql\Connect;

class OrmTest extends TestCase
{

	public function testOrm(){

		$this->assertTrue(true);
		//return;

		$stringOrmName = 'test';
		$stringInitDbFile = __DIR__ . '/initDb.php';

		// Подключаемся к базе
		$connectObject = new Connect(
			\q\tests\SQL_USER,
			\q\tests\SQL_PASS,
			\q\tests\SQL_BASE,
			\q\tests\SQL_HOST,
			\q\tests\SQL_PORT
		);

		// Создаём тестовые таблицы
		$connectObject->query(file_get_contents(__DIR__ . '/test.create.sql'));


		// ORM
		$ormObject = new Orm($connectObject, $stringOrmName);

		// Тестовая база
		$dbTest = $ormObject->createDb(\q\tests\SQL_BASE);

		// Генерируем файл с базой
		$dbTest->generateInitializationFile($stringInitDbFile, '_');


		// Добавляем дополнительную колонку
		$dbTest->getTable('_cont_cat')->createColumnExtend(
			'tag_count',
			new Integer(),
			function(Table $table, Db $db):string{

				// Колитество тегов в категории
				$tableTag = $db->getTable('_cont_tag');
				return (
					'SELECT COUNT(*) ' .
					"FROM {$tableTag->getFullName()} " .
					"WHERE {$tableTag->getColumnReturned('cat_id')->getFullName()} = " .
					"{$table->getColumnReturned('cat_id')->getFullName()}"
				);
			}
		);

		// Добавляем условие
		$dbTest->getTable('_cont_topic')->createColumnWhere(
			'test_where',
			function(Data $data):string{
				return 'cat_id=' . $data->getData();
			}
		);

		// Добавляем условие
		$dbTest->getTable('_cont_topic')->createColumnExtend(
			'test_extend',
			new Integer(),
			function():string{
				return '1';
			}
		);

		$arrayResult = $dbTest->getTable('_cont_topic')->select()
			// Условия
			//->where('topic_id', new Clause('{{value}} > 1'))
			//->where('page_id', [1,2])
			->wheres([
				'topic_id' => new Clause('{{value}} > 1'),
				'page_id' => [1,2],

				// Все условия ниже являются эквивалентными
				// 'topic_id' => 1,
				// 'topic_id' => Clause('{{value}} = 1'),
				// 'topic_id' => Clause('%s = 1', '%s'),
				// 'topic_id' => [1],
				// 'topic_id' => ['1'],
				//
				// // И эти тоже
				// 'page_id' => [1,2],
				// 'page_id' => Clause('{{value}} IN (1,2)'),
				//
				// // Можно использовать поля из таблицы выборки и функции
				// 'topic_date_publish' => Clause('{{value}} < NOW() AND topic_view = 1'),
			])

			// Условия выполняемые после группировки (можно использовать функции агрегации)
			->having('page_id', [1, 2])

			// Сортировка результата
			//->order('page_id', false)
			//->order('topic_name', false)
			->orders([
				'page_id' => false,
				'topic_name' => false,
			])

			// Глубина просмотра
			->depthDefault(1)
			->depth('topic_id___link_topic_tag__topic_id', 0)

			// Время на которое закешииииуется результат запроса
			->cache(0.0)

			// Ограничение количества записей, начало выборки
			->limit(100, 0)

			// Выполнить запрос
			->query()
		;

		/*
		$arrayResult = $dbTest->getTable('_cont_topic')->get([
			// Условия
			'where' => [
				// // use q\orm\db\table\Clause;
				//
				// // Все условия ниже являются эквивалентными
				// 'topic_id' => 1,
				// 'topic_id' => Clause('{{value}} = 1'),
				// 'topic_id' => Clause('%s = 1', '%s'),
				// 'topic_id' => [1],
				// 'topic_id' => ['1'],
				//
				// // И эти тоже
				// 'page_id' => [1,2],
				// 'page_id' => Clause('{{value}} IN (1,2)'),
				//
				// // Можно использовать поля из таблицы выборки и функции
				// 'topic_date_publish' => Clause('{{value}} < NOW() AND topic_view = 1'),

				'topic_id' => new Clause('{{value}} > 1'),
				'page_id' => [1,2],
			],
			// Сортировка результата
			'order' => ['-page_id', '-topic_name'],

			// Глубина просмотра по всем внешним ключам
			'depthDefault' => 1,
			// Глубина просмотра по внешнему ключу 'tag'
			'depth' => [
				'topic_id___link_topic_tag__topic_id' => 0
			],

			// Время на которое закешииииуется результат запроса (если есть подходящее зранилище q\Cache::createCache())
			'cache' => 0.0,

			// Ограничение количества записей
			'limit' => 100,
			// Начало выборки
			'offset' => 0,
		]);
		*/



		// Проверяем всё ли верно вытащилось из базы
		$this->assertEquals(4, count($arrayResult));
		$this->assertEquals('Мусорки шмусорки', $arrayResult[0]['topic_id___cont_topic_meta__topic_id']['topic_meta']);
		$this->assertEquals('zanoza.kg', $arrayResult[3]['page_id___cont_page__page_id']['page_name']);


		// Обновляем запись
		$arrayResult = $dbTest->getTable('_cont_topic')->set([
			'topic_id' => 3,
			'topic_name' => 'Что сделали президенты!',
		]);

		// Проверяем обновилось ли значение
		$this->assertEquals(
			'Что сделали президенты!',
			$dbTest->getTable('_cont_topic')
				   ->select()
				   ->where('topic_id', 3)
				   ->first()['topic_name']
		);

		// Обновляем запись
		$arrayResult = $dbTest->getTable('_cont_topic')->set([
			'topic_id' => 3,
			'topic_name' => 'Что сделают президенты!',
		]);

		// Проверяем обновилось ли значение
		$this->assertEquals(
			'Что сделают президенты!',
			$dbTest->getTable('_cont_topic')
				   ->select()
				   ->where('topic_id', 3)
				   ->first()['topic_name']
		);

		// Обновляем запись
		$arrayResult = $dbTest->getTable('_cont_topic')->set([
			'topic_id' => 8,
			'topic_name' => 'Что что',
			'page_id' => 1,
		]);

		// Проверяем обновилось ли значение
		$this->assertEquals(
			'Что что',
			$dbTest->getTable('_cont_topic')
				   ->select()
				   ->where('topic_id', 8)
				   ->first()['topic_name']
		);

		$dbTest->getTable('_cont_topic')->delete()
			->where('topic_id', 8)
			->query()
		;


		/*
		print_r($dbTest->getTable('_cont_topic')->get([
			'cell' => [
				'topic_name'
			]
		]));
		*/

		// Удаляем тестовые таблицы
		//$connectObject->query(file_get_contents(__DIR__ . '/test.remove.sql'));
	}

}
