/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-08-17 15:20:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for _cont_cat
-- ----------------------------
#DROP TABLE IF EXISTS `_cont_cat`;
CREATE TABLE IF NOT EXISTS `_cont_cat` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of _cont_cat
-- ----------------------------
INSERT IGNORE INTO `_cont_cat` VALUES
  ('1', 'Политика'),
  ('2', 'Общество'),
  ('3', 'Разное')
;

-- ----------------------------
-- Table structure for _cont_page
-- ----------------------------
#DROP TABLE IF EXISTS `_cont_page`;
CREATE TABLE IF NOT EXISTS `_cont_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of _cont_page
-- ----------------------------
INSERT IGNORE INTO `_cont_page` VALUES
  ('1', 'zanoza.kg'),
  ('2', '24.kg')
;

-- ----------------------------
-- Table structure for _cont_set
-- ----------------------------
#DROP TABLE IF EXISTS `_cont_set`;
CREATE TABLE IF NOT EXISTS `_cont_set` (
  `id` int(11) NOT NULL,
  `value` set('четыре','три','два','один','ноль') NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of _cont_set
-- ----------------------------
INSERT IGNORE INTO `_cont_set` VALUES
  ('0', 'четыре,один'),
  ('1', 'три,два,один')
;

-- ----------------------------
-- Table structure for _cont_tag
-- ----------------------------
#DROP TABLE IF EXISTS `_cont_tag`;
CREATE TABLE IF NOT EXISTS `_cont_tag` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `cat_id` (`cat_id`),
  CONSTRAINT `_cont_tag_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `_cont_cat` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of _cont_tag
-- ----------------------------
INSERT IGNORE INTO `_cont_tag` VALUES
  ('1', 'Атамбаев', '1'),
  ('2', 'Бакиев', '1'),
  ('3', 'Акаев', '1'),
  ('4', 'Клубы', '2'),
  ('5', 'Кафе', '2'),
  ('6', 'Кино', '2'),
  ('7', 'Мусор', '3'),
  ('8', 'Ремонт', '3'),
  ('9', 'Светофоры', '3')
;

-- ----------------------------
-- Table structure for _cont_topic
-- ----------------------------
#DROP TABLE IF EXISTS `_cont_topic`;
CREATE TABLE IF NOT EXISTS `_cont_topic` (
  `topic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`topic_id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `_cont_topic_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `_cont_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of _cont_topic
-- ----------------------------
INSERT IGNORE INTO `_cont_topic` VALUES
  ('1', 'Президенты кыргызстана', '1'),
  ('2', 'Куда можно сходить', '1'),
  ('3', 'Что сделали президенты!', '1'),
  ('4', 'Мусорки возле общественных мест', '2'),
  ('5', 'Какие места посещают президенты', '2')
;

-- ----------------------------
-- Table structure for _cont_topic_meta
-- ----------------------------
#DROP TABLE IF EXISTS `_cont_topic_meta`;
CREATE TABLE IF NOT EXISTS `_cont_topic_meta` (
  `topic_id` int(10) unsigned NOT NULL,
  `topic_meta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`topic_id`),
  CONSTRAINT `_cont_topic_meta_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `_cont_topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of _cont_topic_meta
-- ----------------------------
INSERT IGNORE INTO `_cont_topic_meta` VALUES
  ('1', 'Президенты мрезиденты'),
  ('2', 'Сходить не сходить'),
  ('3', 'Сделали не сделали'),
  ('4', 'Мусорки шмусорки'),
  ('5', 'Места шместа')
;

-- ----------------------------
-- Table structure for _link_topic_tag
-- ----------------------------
#DROP TABLE IF EXISTS `_link_topic_tag`;
CREATE TABLE IF NOT EXISTS `_link_topic_tag` (
  `topic_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `link_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`topic_id`,`tag_id`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `_link_topic_tag_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `_cont_topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_link_topic_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `_cont_tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of _link_topic_tag
-- ----------------------------
INSERT IGNORE INTO `_link_topic_tag` VALUES
  ('1', '1', 'Последний президент', '1'),
  ('1', '2', 'Бывший президент', '2'),
  ('1', '3', 'Первый президент', '3'),
  ('2', '4', 'Потусить в клубы', '1'),
  ('2', '5', 'Пожрать в кафешку', '2'),
  ('2', '6', 'Посмотреть фильмец', '3'),
  ('3', '1', 'Забрал вечёрку', '1'),
  ('3', '2', 'Своровал пол страны', '2'),
  ('3', '3', 'Получил докторскую', '3'),
  ('3', '4', 'При Акаеве начали строит клубы', '4'),
  ('3', '5', 'При Бакиеве было нечего жрать', '5'),
  ('3', '6', 'При Атамбаеве появились киношки', '6'),
  ('4', '5', 'Возле кафешек теперь чисто', '2'),
  ('4', '7', 'До этого всё было в говне', '1'),
  ('5', '1', 'Ходит по кабакам ', '3'),
  ('5', '2', 'Ездит на горнолыжки', '2'),
  ('5', '3', 'Ходит в универ', '1'),
  ('5', '5', 'Сюда ходит Атабмаев', '4')
;

SET FOREIGN_KEY_CHECKS=1;
