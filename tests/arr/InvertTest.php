<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:04
 */

namespace q\tests\arr;


class InvertTest extends \PHPUnit\Framework\TestCase
{
	public function testInvert(){
		$arrayStart = [

			1 => [
				'a' => 'value 1 a',
				'b' => 'value 1 b',
				'c' => 'value 1 c',
			],
			2 => [
				'a' => 'value 2 a',
				'b' => 'value 2 b',
				'c' => 'value 2 c',
			],
			3 => [
				'a' => 'value 3 a',
				'b' => 'value 3 b',
				'c' => 'value 3 c',
			],
		];
		$arrayEnd = [
			'a' => [
				1 => 'value 1 a',
				2 => 'value 2 a',
				3 => 'value 3 a',
			],
			'b' => [
				1 => 'value 1 b',
				2 => 'value 2 b',
				3 => 'value 3 b',
			],
			'c' => [
				1 => 'value 1 c',
				2 => 'value 2 c',
				3 => 'value 3 c',
			],
		];
		
		$this->assertEquals(\q\arr\invert($arrayStart), $arrayEnd);
	}
}
