<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:07
 */

namespace q\tests\arr;


class MapTest extends \PHPUnit\Framework\TestCase
{
	private $arrayParts = [
		'key1' => 'value1',
		'key2' => 'value2',
		'key3' => 'value3',
	];

	public function testMap(){

		$arrayPartsResult = [
			'key1' => [
				'value' => 'value1',
				'key' => 'key1',
				'index' => 0,
			],
			'key2' => [
				'value' => 'value2',
				'key' => 'key2',
				'index' => 1,
			],
			'key3' => [
				'value' => 'value3',
				'key' => 'key3',
				'index' => 2,
			],
		];


		$this->assertEquals($arrayPartsResult, \q\arr\map($this->arrayParts, function($stringValue, $stringKey, $intIndex){
			return[
				'value' => $stringValue,
				'key' => $stringKey,
				'index' => $intIndex,
			];
		}));
	}

	public function testMapNextPrev(){
		$arrayPartsResultNextAndPrev = [
			'key1' => [
				'value' => 'value1',
				'key' => 'key1',
				'index' => 0,
				'prev' => null,
				'next' => 'value2',
			],
			'key2' => [
				'value' => 'value2',
				'key' => 'key2',
				'index' => 1,
				'prev' => 'value1',
				'next' => 'value3',
			],
			'key3' => [
				'value' => 'value3',
				'key' => 'key3',
				'index' => 2,
				'prev' => 'value2',
				'next' => null,
			],
		];

		$this->assertEquals($arrayPartsResultNextAndPrev, \q\arr\map($this->arrayParts, function($stringValue, $stringKey, $intIndex, $mixedPrev, $mixedNext){
			return[
				'value' => $stringValue,
				'key' => $stringKey,
				'index' => $intIndex,
				'prev' => $mixedPrev,
				'next' => $mixedNext,
			];
		}, true));
	}
}
