<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:02
 */

namespace q\tests\arr;


class ImplodeTest extends \PHPUnit\Framework\TestCase
{
	public function testImplode(){
		$arrayParts = [
			'key1' => 'value1',
			'key2' => 'value2',
			'key3' => 'value3',
		];

		$stringTestResult1 =
			'key1=>' . '0|' . ''       . '|' . 'value1' . '|' . 'value2' .
			'&' .
			'key2=>' . '1|' . 'value1' . '|' . 'value2' . '|' . 'value3' .
			'&' .
			'key3=>' . '2|' . 'value2' . '|' . 'value3' . '|' . '' .
			'';

		$stringResult1 = \q\arr\implode('&', $arrayParts, function($stringValue, $stringKey, $intIndex, $stringPrev, $stringNext){
			return $stringKey . '=>' . $intIndex . '|' . $stringPrev . '|' . $stringValue . '|' . $stringNext;
		}, true);

		$this->assertEquals($stringTestResult1, $stringResult1);
	}
}
