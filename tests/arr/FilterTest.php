<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 9:55
 */

namespace q\tests\arr;



class FilterTest extends \PHPUnit\Framework\TestCase
{
	public function testFilter(){

		$arrayTest = [false, null, '', 1];
		$arrayResultOk = [3 => 1];
		$arrayResult = \q\arr\filter($arrayTest, function($value){return $value;});

		$this->assertEquals($arrayResult, $arrayResultOk);
	}
}
