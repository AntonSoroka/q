<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 12:08
 */

namespace q\tests\route;


use q\utils\Route;

class RouteTest extends \PHPUnit\Framework\TestCase
{
	public function testRoute(){
		$arrayPath = [
			'#^/user/(?<user_id>[\d]+)/$#usi' => function(int $user_id){
				return [0, $user_id];
			},
			'#^/user/(?<user_id>[\d]+)/message/$#usi' => function(int $user_id){
				return [1, $user_id];
			},
			'#^/user/(?<user_id>[\d]+)/message/(?<message_id>[\d]+)/$#usi' => function(int $user_id, int $message_id){
				return [2, $user_id, $message_id];
			},
			'#^/user/(?<user_id>[\d]+)/message/order/(?<order>(date|len|update|rating))/$#usi' => function(int $user_id, string $order){
				return [3, $user_id, $order];
			},
			'#^/user/(?<user_id>[\d]+)/data/(name/(?<user_name>[^/]+)/)?$#usi' => function(int $user_id, string $user_name = null){
				return [4, $user_id, $user_name];
			},
		];

		$route = new Route();

		foreach($arrayPath as $regularPath => $functionCallback){
			$route->add('GET', $regularPath, $functionCallback);
		}


		$arrayResultList = [
			'/user/15/' => [0, 15],
			'/user/15/message/' => [1, 15],
			'/user/15/message/20/' => [2, 15, 20],
			'/user/15/message/order/len/' => [3, 15, 'len'],
			'/user/15/data/name/ugin_root/' => [4, 15, 'ugin_root'],
			'/user/15/data/' => [4, 15, null],
		];

		foreach($arrayResultList as $stringPath => $arrayResult) {
			$arrayOriginalResult = $route->find('GET', $stringPath);
			$this->assertEquals($arrayOriginalResult, $arrayResult);
		}
	}
}
