<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:26
 */

namespace q\tests\html;


class DecodeTest extends \PHPUnit\Framework\TestCase
{
	public function testDecode(){
		$stringTestStart = implode('', \q\html\DECODE);
		$stringTestEnd   = implode('', \q\html\ENCODE);

		$this->assertEquals(\q\html\decode($stringTestEnd), $stringTestStart);
	}
}
