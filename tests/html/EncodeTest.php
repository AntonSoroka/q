<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:27
 */

namespace q\tests\html;


class EncodeTest extends \PHPUnit\Framework\TestCase
{
	public function testEncode(){
		$stringTestStart = '&'     . '<'    . '>'    . '"'      . "'"      . '/'     ;
		$stringTestEnd   = '&amp;' . '&lt;' . '&gt;' . '&quot;' . '&#x27;' . '&#x2F;';
		
		$this->assertEquals(\q\html\encode($stringTestStart), $stringTestEnd);
	}
}
