<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:40
 */

namespace q\tests\text;


class DeclensionTest extends \PHPUnit\Framework\TestCase
{
	public function testDeclension(){
		$arrayTestList = [
			0 => 'рублей',
			1 => 'рубль',
			2 => 'рубля',
			3 => 'рубля',
			4 => 'рубля',
			5 => 'рублей',
			6 => 'рублей',
			7 => 'рублей',
			8 => 'рублей',
			9 => 'рублей',
			10 => 'рублей',
			11 => 'рублей',
			12 => 'рублей',
			13 => 'рублей',
			14 => 'рублей',
			15 => 'рублей',
			16 => 'рублей',
			17 => 'рублей',
			18 => 'рублей',
			19 => 'рублей',
			20 => 'рублей',
			21 => 'рубль',
			22 => 'рубля',
			23 => 'рубля',
			24 => 'рубля',
			25 => 'рублей',
			26 => 'рублей',
			27 => 'рублей',
			28 => 'рублей',
			29 => 'рублей',
			30 => 'рублей',
			31 => 'рубль',
			100 => 'рублей',
			101 => 'рубль',
			1000 => 'рублей',
			1001 => 'рубль',
		];

		foreach($arrayTestList as $intCount => $stringResult){
			$this->assertEquals($stringResult, \q\text\declension($intCount, 'рублей', 'рубль', 'рубля'));
		}
	}
}
