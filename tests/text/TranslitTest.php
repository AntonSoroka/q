<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.2017
 * Time: 10:41
 */

namespace q\tests\text;


class TranslitTest extends \PHPUnit\Framework\TestCase
{
	public function testTarnslit(){
		// Все символы которые мы можем преобразовать
		$stringStart = \q\arr\implode(',', array_keys(\q\text\tarnslit\CHAR_TABLE));
		// Результат преобразования строки
		$stringEnd = \q\arr\implode(',', array_values(\q\text\tarnslit\CHAR_TABLE));

		// Добавляем символы которые должны быть убраны
		$stringStart = $stringStart . '✉ᵫᵬᵭᵮᵯᵰᵱᵲᵳᵴᵵᵶ';


		$this->assertEquals(\q\text\translit($stringStart), $stringEnd);
	}
}
