<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.03.2017
 * Time: 11:18
 */

namespace q\tests\event;

class EventTest extends \PHPUnit\Framework\TestCase
{
	public function testEvent(){


		/** @var \q\Event $eventObject */
		$eventObject = new class extends \q\Event{
			public function eventClick(int &$intX, int &$intY){}
			public function eventRemoveBlock(string $stringBlockName, int $intIndex, bool &$boolRemove = true){}
		};


		// Просто аргкменты
		$eventObject->on('click', function(int &$intX, int &$intY){
			return func_get_args();
		});
		// Аргументы наоборот
		$eventObject->on('click', function(int &$intY, int &$intX){
			return func_get_args();
		});

		// Аргументы получены по ссылке и умножены на 10
		$eventObject->on('click', function(int &$intX, int &$intY){
			$intX = $intX * 10;
			$intY = $intY * 10;
			return func_get_args();
		});

		// Обновлённые в предыдущем калбеке аргументы
		$eventObject->on('click', function(int &$intX, int &$intY){
			return func_get_args();
		});

		$intX = 1;
		$intY = 2;

		$intLinkX = $intX;
		$intLinkY = $intY;

		$arrayResult = $eventObject->trigger('click', ...[&$intLinkX, &$intLinkY]);

		$this->assertEquals($arrayResult, [
			[$intX, $intY],
			[$intY, $intX],
			[$intLinkX, $intLinkY],
			[$intX * 10, $intY * 10],
		]);


		// Обычный порядок
		$eventObject->on('remove.block', function(string $stringBlockName, int $intIndex, bool &$boolRemove){
			return func_get_args();
		});

		// Не все аргументы
		$eventObject->on('remove.block',function (bool &$boolRemove){
			return func_get_args();
		});

		// Аргументы наоборот
		$eventObject->on('remove.block',function (bool &$boolRemove, int $intIndex, string $stringBlockName){
			return func_get_args();
		});

		$stringBlock = 'my_block';
		$intIndex = 5;
		$boolRemove = false;

		$arrayResult = $eventObject->trigger('remove.block', $stringBlock, $intIndex, $boolRemove);

		$this->assertEquals($arrayResult, [
			[$stringBlock, $intIndex, $boolRemove],
			[$boolRemove],
			[$boolRemove, $intIndex, $stringBlock],
		]);
	}
}
