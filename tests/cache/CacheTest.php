<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 25.03.2017
 * Time: 9:51
 */

namespace q\tests;


use PHPUnit\Framework\TestCase;
use q\Cache;
use q\cache\storage\Fs;
use q\cache\storage\Memcache;

class CacheTest extends TestCase
{
	public function testCache(){

		$stringTestVariableName = 'test1';
		$stringTestStorageName = 'test';
		
		@mkdir(Cache::$stringFsPath);
		
		$intStatus = 0;

		// Создаём хранилище
		$cacheObject = Cache::createCache($stringTestStorageName);

		// Записываем тысячу
		$cacheObject->set($stringTestVariableName, 1000);

		// Проверяем запись и статус
		$this->assertEquals($cacheObject->get($stringTestVariableName, 0, $intStatus), 1000);
		$this->assertEquals(Cache::STATUS_OK, $intStatus);

		// Записываем false
		$cacheObject->set($stringTestVariableName, false);

		// Проверяем запись
		$this->assertFalse($cacheObject->get($stringTestVariableName, 0, $intStatus));
		$this->assertEquals(Cache::STATUS_OK, $intStatus);

		// Спим 3 миллисекунды
		usleep(3000);


		// Проверяем запись
		$this->assertFalse($cacheObject->get($stringTestVariableName, 0.001, $intStatus));
		// Должна устареть
		$this->assertEquals(Cache::STATUS_OLD, $intStatus);


		// Remove
		$cacheObject->remove($stringTestVariableName);

	}
	
	public function testMemcache(){

		$stringTestVariableName = 'test1';
		$stringTestStorageName = 'test';
		$intStatus = 0;

		$memcacheObject = new Memcache(\q\tests\MEMCACHE_HOST, \q\tests\MEMCACHE_POST);

		$this->assertNotFalse($memcacheObject->isConnect());
		if(!$memcacheObject->isConnect()){
			return;
		}

		$memcacheObject->setStorageName($stringTestStorageName);

		// Ставим значение
		$memcacheObject->set($stringTestVariableName, 1000);

		// Проверяем значение
		$this->assertEquals($memcacheObject->get($stringTestVariableName, $intStatus), 1000);
		$this->assertEquals(Cache::STATUS_OK, $intStatus);


		// Ставим значение
		$memcacheObject->set($stringTestVariableName, true);

		// Проверяем значение
		$this->assertTrue($memcacheObject->get($stringTestVariableName, $intStatus));
		$this->assertEquals(Cache::STATUS_OK, $intStatus);


		// Удаляем значение
		$memcacheObject->remove($stringTestVariableName);
	}

	public function testFs(){

		$stringTestVariableName = 'test1';
		$stringTestStorageName = 'test';
		$intStatus = false;

		$fsObject = new Fs(Cache::$stringFsPath);
		$fsObject->setStorageName($stringTestStorageName);

		// Ставим значение
		$fsObject->set($stringTestVariableName, 1000);

		// Проверяем значение
		$this->assertEquals($fsObject->get($stringTestVariableName, $intStatus), 1000);
		$this->assertEquals(Cache::STATUS_OK, $intStatus);

		// Ставим значение
		$fsObject->set($stringTestVariableName, true);

		// Проверяем значение
		$this->assertTrue($fsObject->get($stringTestVariableName, $intStatus));
		$this->assertEquals(Cache::STATUS_OK, $intStatus);

		
		// Удаляем значение
		$fsObject->remove($stringTestVariableName);


	}
}
