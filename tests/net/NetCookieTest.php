<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 08.08.2017
 * Time: 10:30
 */

namespace q\tests\net;

use PHPUnit\Framework\TestCase;
use q\net\Cookie;
use q\net\cookie\Item;
use q\net\Header;
use q\net\Query;


class NetCookieTest extends TestCase{



	public function testCookieItem(){

		$cookieItem = new Item('id', '1', 'http://test.org/path/to/index.php', 'test.org', '/path/');

		// Дата истечения в прошлом
		$cookieItem->setExpires(time() - 10);
		// Проверяем
		$this->assertEquals(true, $cookieItem->isOld());

		// Дата истечения в будующем
		$cookieItem->setExpires(time() + 1000);
		// Проверяем
		$this->assertEquals(false, $cookieItem->isOld());

		// Отрицательное время жизни
		$cookieItem->setMaxAge(-100);
		// Проверяем
		$this->assertEquals(true, $cookieItem->isOld());

		// Положительное время жизни, явно не устаревшее с момента создания
		$cookieItem->setMaxAge(100);
		// Проверяем
		$this->assertEquals(false, $cookieItem->isOld());

		// Не подходящие ссылки
		$this->assertEquals(false, $cookieItem->isAllowedUrl('http://test.org/other/path/index.php'));
		$this->assertEquals(false, $cookieItem->isAllowedUrl('http://other.host.org/path/to/index.php'));
		$this->assertEquals(false, $cookieItem->isAllowedUrl('http://other.host.org/path'));
		$this->assertEquals(false, $cookieItem->isAllowedUrl('http://other.host.org/other/path/index.php'));

		// Подходящие ссылки
		$this->assertEquals(true, $cookieItem->isAllowedUrl('http://test.org/path/to/index.php'));
		$this->assertEquals(true, $cookieItem->isAllowedUrl('http://test.org/path/'));
		$this->assertEquals(true, $cookieItem->isAllowedUrl('http://www.test.org/path/to/index.php'));

		// Проверяем JSON преобразования
		$arrayJsonCookieItem = json_encode($cookieItem);
		$cookieItemCopy = Item::jsonUnserialize(json_decode($arrayJsonCookieItem, true));

		$this->assertEquals($cookieItemCopy->getId(), Item::getNew($cookieItem, $cookieItemCopy)->getId());

		// Набор кукисов
		$cookie = new Cookie();

		// Говорим генерировать исключение в случае не соблюдения куками секюрных норм
		$cookie->boolCreateException = true;

		// Назначаем куку внутреннему пути, по стандарту так делать нельзя
		try{
			$cookie->setFromSetCookieHeader('http://test.com/', 'Set-Cookie: path=2; Path=/path/; Max-Age=10');
			$this->assertTrue(false);
		}catch(\Exception $e){
			$this->assertTrue(true);
		}

		// Назначаем куку домену "дедушке", по стандарту так нельзя
		try{
			$cookie->setFromSetCookieHeader('http://z.y.x.com/', 'Set-Cookie: path=2; Domain=x.com; Path=/path/; Max-Age=10');
			$this->assertTrue(false);
		}catch(\Exception $e){
			$this->assertTrue(true);
		}

		// Ставим reset=1 для /
		$cookie->setFromSetCookieHeader('http://test.com/', 'Set-Cookie: reset=1; Path=/; Max-Age=10');
		// Переопределяем reset=2 для /
		$cookie->setFromSetCookieHeader('http://test.com/', 'Set-Cookie: reset=2; Path=/; Max-Age=10');
		// Ставим path=1 для /, из поддомена (так делать можно)
		$cookie->setFromSetCookieHeader('http://www.test.com/', 'Set-Cookie: path=1; Domain=test.com; Path=/; Max-Age=10');
		// Ставим path=2 для /path/
		$cookie->setFromSetCookieHeader('http://test.com/path/', 'Set-Cookie: path=2; Path=/path/; Max-Age=10');
		// Переопределяем reset=3 для /
		$cookie->setFromSetCookieHeader('http://www.test.com/', 'Set-Cookie: reset=3; Path=/; Max-Age=10');

		// Путь /
		$this->assertEquals('reset=2; path=1', $cookie->get('http://test.com/'));
		// Путь /path/
		$this->assertEquals('reset=2; path=2', $cookie->get('http://test.com/path/'));
		// Поддомен
		$this->assertEquals('reset=3; path=1', $cookie->get('http://www.test.com/'));
		// Поддомен с /path/
		$this->assertEquals('reset=3; path=2', $cookie->get('http://www.test.com/path/'));
		// Путь / (переопределение)
		$this->assertEquals('reset=2; path=1', $cookie->get('http://test.com/'));
	}

	public function testHeaderItem(){

		// Неверное имя, должно выскочить исключение
		try{
			new \q\header\Item('bad:name', 'value');
			$this->assertTrue(false);
		}catch(\Exception $e){
			$this->assertTrue(true);
		}

		// Неверный формат, должно сработать исключение
		try{
			\q\header\Item::createFromString("\nbad-name:value");
			$this->assertTrue(false);
		}catch(\Exception $e){
			$this->assertTrue(true);
		}

		// Неверный формат, должно сработать исключение
		try{
			\q\header\Item::createFromString("bad-name:value\ntwo string");
			$this->assertTrue(false);
		}catch(\Exception $e){
			\q\header\Item::createFromString("bad-name:value\n two string");
			$this->assertTrue(true);
		}

		$header = new Header();

		// Добавляем a
		$header->add('a', '1');
		// Добавляем a не перезаписывая старое значение
		$header->add('a', '2', false);
		// Добавляем b
		$header->add('b', '3');
		// Перезаписываем b
		$header->add('b', '4');

		// Все заголовки
		$this->assertEquals("a: 1\r\na: 2\r\nb: 4", (string)$header);
		// Заголовки a
		$this->assertEquals("a: 1\r\na: 2", \implode("\r\n", $header->getHeaderString('a')));
		// Заголовки b
		$this->assertEquals('b: 4', \implode("\r\n", $header->getHeaderString('b')));

	}

	public function testQuery(){

		// Запускаем сервер
		$server = new \q\utils\Server(__DIR__ . '/route.php', 9998);
		$server->start();

		$query = new Query(new Header(), new Cookie());

		$stringResult = $query->get('http://localhost:9998/');

		// Результат после перенаправления
		$this->assertEquals('result', $stringResult);

		// Куки для разных путей, ноставленные на перенаправлении
		$this->assertEquals('loc=1; old=1', $query->getCookie()->get('http://localhost:9998/'));
		$this->assertEquals('loc=2; old=1; new=2', $query->getCookie()->get('http://localhost:9998/location'));
	}
}
