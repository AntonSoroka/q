<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.07.2017
 * Time: 10:39
 */

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase{
	public function testApp(){

		$this->assertTrue(is_float(\q\app\REQUEST_START));
		$this->assertEquals(\q\fs\realPath(dirname(dirname(__DIR__)), '/'), \q\app\PATH_CWD);
		$this->assertEquals('http', \q\app\SCHEME);
		$this->assertEquals(80, \q\app\PORT);
		$this->assertTrue(\q\app\PORT_DEFAULT);
		$this->assertEquals('', \q\app\PORT_STRING);
		$this->assertEquals('127.0.0.1', \q\app\HOST);
		$this->assertEquals('', \q\app\REQUEST);
		$this->assertEquals('127.0.0.1', \q\app\IP);
		$this->assertEquals('http://127.0.0.1/', \q\app\URL);

		$app = \q\App::getApp('test');
		$app->initView('test', \q\fs\joinPath(__DIR__, '../view/test'), 'http://localhost:9999/view/test/');
		$app->initConnect(
			\q\tests\SQL_BASE,
			\q\tests\SQL_USER,
			\q\tests\SQL_PASS,
			\q\tests\SQL_HOST
		);

		// Создаём тестовые таблицы
		$app->getConnect()->query(file_get_contents(__DIR__ . '/../orm/test.create.sql'));

		// Инициируем ORM
		$app->initOrm(
			'test_app',
			__DIR__ . '/initDb.php',
			'_'
		);

		$this->assertTrue($app->getConnect() instanceof \q\sql\Connect);
		$this->assertTrue($app->getDb() instanceof \q\orm\Db);
		$this->assertTrue($app->getView('test') instanceof \q\View);
		$this->assertTrue($app->getCache() instanceof \q\Cache);
		$this->assertTrue($app->getRoute() instanceof \q\utils\Route);
	}
}
