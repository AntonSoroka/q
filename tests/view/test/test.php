<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 10:25
 */

/**
 * @param int $intNum
 * @return int
 */
return function(int $intNum){
	static $intCountCall = 0;

	$intCountCall = $intCountCall + 1;

	return 'test/' . ($intNum + $intCountCall);
};