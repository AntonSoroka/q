<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 10:53
 */

/**
 * @param string[] $arrayResult
 * @return string
 */
return function(string ...$arrayResult):string{
	return implode(';', $arrayResult);
};