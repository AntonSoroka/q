<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.07.2017
 * Time: 10:27
 */

use q\View;
use PHPUnit\Framework\TestCase;

class ViewTest extends TestCase{

	public function testBem(){
		$bemObject = View::__create(__DIR__, 'http://127.0.0.1/', \q\App::getApp('test'));

		// Тестируем статичные переменные внутри функции
		$this->assertEquals('test/124', $bemObject->__load('test',123));
		$this->assertEquals('test/125', $bemObject->__load('test',123));

		// Тестируем вложенные вызовы
		$this->assertEquals('test/126;test/127', $bemObject->__load('test/tree',
			$bemObject->__load('test',123),
			$bemObject->__load('test',123)
		));

		// Тестируем пустую директорию
		$this->assertEquals($bemObject->__load('test/empty'), null);

		// Тестируем несуществующую директорию, должно выброситься исключение
		try{
			$bemObject->__load('test/empty/bad');
			$this->assertTrue(false);
		}catch(\LogicException $e){
			$this->assertTrue(true);
		}

		// Подключаем директорию в которой содержатся только CSS и JS файлы
		$bemObject->__load('test/files');

		// Тестируем возвращаемые JS файлы
		$stringPath = str_replace('\\', '/', __DIR__) . '/';
		$this->assertEquals(
			[
				$stringPath . 'test/test.js' => 'http://127.0.0.1/test/test.js',
				$stringPath . 'test/files/files.js' => 'http://127.0.0.1/test/files/files.js',
			],
			$bemObject->__getJs()
		);


		// Подключение обязательного файла
		try{

			// Подключаем обязательный JS файл который не существует
			$bemObject->__loadViewJs('test/empty');

			// Построение списка файлов должно вызвать исключение
			$bemObject->__getJs();

			// Исключение не сгенерировалось
			$this->assertTrue(false);

		}catch(\LogicException $e){

			// Исключение сгенерировалось
			$this->assertTrue(true);
		}

	}
}
