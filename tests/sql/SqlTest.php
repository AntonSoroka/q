<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.03.2017
 * Time: 22:59
 */

namespace q\tests\sql;

use PHPUnit\Framework\TestCase;
use q\sql\connect\Info;

class SqlTest extends TestCase
{
    public function testSql(){

        // Запрос
        $stringQuery = /** @lang MySQL */ <<<'SQL'
            SELECT 
              1+1 AS test1, 
              2*2 AS test2
            ;
            SELECT 3+3 AS test3;
SQL;

        // Результат
        $arrayTestResult = [
            0 => [
                0 => ['test1' => '2', 'test2' => '4'],
            ],
            1 => [
                0 => ['test3' => '6'],
            ],
        ];

        // Создаём пустое подключение без юзера и пароля для выполнения тестов
        $connectObject = new \q\sql\Connect('', '');

        // Запрос, должен сохраниться в кеше
        $arrayResult = $connectObject->query($stringQuery, 0.01);
        $this->assertEquals($arrayTestResult, $arrayResult);

        // Но запроситься из базы
        $this->assertEquals(Info::FROM_DB, $connectObject->getInfoList()->getInfo()->getFrom());

        // Ещё раз выполняем тот же запрос он должен совпасть с эталоном
        $arrayResult = $connectObject->query($stringQuery, 0.01);
        $this->assertEquals($arrayTestResult, $arrayResult);

        // Но запроситься из кеша не не из базы
        $this->assertEquals(Info::FROM_CACHE, $connectObject->getInfoList()->getInfo()->getFrom());

        // Спим 3 миллисекунды
        usleep(3000);

		// Ещё раз выполняем тот же запрос он должен совпасть с эталоном
		$arrayResult = $connectObject->query($stringQuery, 0.001);
		$this->assertEquals($arrayTestResult, $arrayResult);

		// Но запроситься из базы
		$this->assertEquals(Info::FROM_DB, $connectObject->getInfoList()->getInfo()->getFrom());

		$this->assertEquals(3, $connectObject->getInfoList()->countQuery());

		// Подзапросов 6 но один раз результат целиком возвращается из кеша и считается только один запрос
		$this->assertEquals(5, $connectObject->getInfoList()->countSubQuery());
    }

    public function testEscapeName(){
    	$stringStart = 'column';
    	$stringEnd = '`column`';

    	$this->assertEquals(\q\sql\escapeName($stringStart), $stringEnd);
	}

	public function testEscape(){

    	$arrayVariants = [
    		[ 1,   0.5,   false,   true,   null,   'string',  "\\ ' \" \n"],
    		['1', '0.5', 'FALSE', 'TRUE', 'NULL', '"string"', "\"\\\\ \\' \\\" \\n\""],
		];

		$arrayVariants[0] = array_map('\q\sql\escape', $arrayVariants[0]);

    	$this->assertEquals($arrayVariants[0], $arrayVariants[1]);

	}
}
