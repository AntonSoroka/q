<?php

/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.03.2017
 * Time: 12:43
 */
class FsTest extends \PHPUnit\Framework\TestCase
{

	public static $stringPath = __DIR__ . '/';
	public static $arrayElements = [
		'test/',
		'test/1/',
		'test/1/file1.1.txt',
		'test/1/file1.2.txt',
		'test/1/file1.3.txt',
		'test/2/',
		'test/2/file2.1.txt',
		'test/2/file2.2.txt',
		'test/2/file2.3.txt',
		'test/3/',
		'test/3/file3.1.txt',
		'test/3/file3.2.txt',
		'test/3/file3.3.txt',
	];
	public static $boolInit = false;

	public function __construct($name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);

		if(!static::$boolInit){
			foreach(static::$arrayElements as &$stringPathCurrent) {
				$stringPathCurrent = static::$stringPath . $stringPathCurrent;
			}
			static::$boolInit = true;
		}
	}

	public function createFiles(){

		if(file_exists(static::$arrayElements[0])){
			$this->removeFiles();
		}

		foreach(static::$arrayElements as $stringPathCurrent){

			if(file_exists($stringPathCurrent)){
				continue;
			}

			if(mb_substr($stringPathCurrent, -1) === '/'){
				mkdir($stringPathCurrent);
			}else{
				touch($stringPathCurrent);
			}
		}

	}

	public function removeFiles(){

		$arrayElements = array_reverse(static::$arrayElements);

		foreach($arrayElements as $stringPathCurrent){

			if(!file_exists($stringPathCurrent)){
				continue;
			}

			if(mb_substr($stringPathCurrent, -1) === '/'){
				@rmdir($stringPathCurrent);
			}else{
				unlink($stringPathCurrent);
			}
		}
	}

	public function testScan()
	{

		$this->createFiles();

		$arrayResult = [];
		\q\fs\scan(static::$stringPath . 'test/', function ($stringPath) use (&$arrayResult) {
			$arrayResult[] = $stringPath;
		});
		$this->assertEquals(static::$arrayElements, $arrayResult);

		$this->removeFiles();
	}


	public function testRemove(){
		$this->createFiles();
		\q\fs\remove(static::$stringPath . 'test/');
		$this->assertFalse(file_exists(static::$stringPath . 'test/'));
		$this->removeFiles();
	}

	public function testCopy(){
		$this->createFiles();

		\q\fs\copy(static::$stringPath . 'test/', static::$stringPath . 'test1/');

		$arrayResult = [];
		\q\fs\scan(static::$stringPath . 'test1/', function ($stringPath) use (&$arrayResult) {
			$arrayResult[] = $stringPath;
		});

		foreach($arrayResult as &$stringPath){
			$stringPath = str_replace('/test1/', '/test/', $stringPath);
		}

		$this->assertEquals(static::$arrayElements, $arrayResult);


		$this->removeFiles();
		\rename(static::$stringPath . 'test1/', static::$stringPath . 'test/');
		$this->removeFiles();
	}

	public function testJoinPath(){
		$this->assertEquals('q/w/e', \q\fs\joinPath('q', 'w', 'e'));
		$this->assertEquals('/q/w/e', \q\fs\joinPath('/q', 'w', 'e'));
		$this->assertEquals('/q/w/e/', \q\fs\joinPath('/q', 'w', 'e/'));
		$this->assertEquals('C:/p/h/p/', \q\fs\joinPath('C:\\p\\h\\p\\'));
		$this->assertEquals('/var/bin/', \q\fs\joinPath('/var/log/../bin/././'));
	}

	public function testRealPath(){

		$this->assertEquals(\q\fs\joinPath(__DIR__), \q\fs\realPath(__DIR__));

		// Тут должно быть исключение
		try{
			\q\fs\realPath(__DIR__, 'fake_folder');
			$this->assertTrue(false);
		}catch(\Exception $e){
			$this->assertTrue(true);
		}

	}

	public function testFileGetContent(){


		$server = new q\utils\Server(__DIR__ . '/route.php', 9999);
		$server->start();

		// Должно скачаться с сайта
		$stringResultLoad = \q\fs\fileGetContent('http://localhost:9999/', 0.1);



		// Завершаем проццесс сервера
		unset($server);

		// Должно взяться из кеша
		$stringResultCache = \q\fs\fileGetContent('http://localhost:9999/', 0.1);

		// Результаты должны быть равны
		$this->assertEquals($stringResultLoad, $stringResultCache);
	}

}
